@Pumpkin
Feature: Pumpkin App Demo

  @PumpkinApp
  Scenario: Pumpkin Verify Profile
    Given I start application by id "com.pumpkin.pumpkin.debug"
    And I start device logging
    And I click on "pumpkin.login"
    And I enter "+33610187796" to "pumpkin.phone"
    And I click on "pumpkin.submit"
    And I enter "Azerty123" to "pumpkin.pass"
    And I click on "pumpkin.validate"
    And I click on "pumpkin.later"
    And I enter PIN "1597"
    And I click on "pumpkin.profile"
    And I click on "pumpkin.verify"
    And I click on "pumpkin.passport"
    And I click on "pumpkin.photo"
    And I click on "droid.bttn.allowWhileUsingApp"
    And I wait for "10" seconds
    And I close application by id "com.pumpkin.pumpkin.debug"
    And I stop device logging
