@JuliusBaer
Feature: Julius Baer Demo Scenarios

  @BJBViewTalkingPoints
  Scenario: BJB Talking Points
    Given I try to close application by name "Insights" and ignore error
    And I start application by name "Insights"
    And I wait for "5" seconds
    And I click on "bjb.bttn.home"
    When I click on "bjb.list.art1"
    And I click on "bjb.bttn.next"
    And I click on "bjb.bttn.next"
    And I click on "bjb.bttn.next"
    Then I click on "bjb.bttn.back"

  @BJBViewMenu
  Scenario: BJB Open Menu
    Given I try to close application by name "Insights" and ignore error
    And I start application by name "Insights"
    And I wait for "5" seconds
    And I click on "bjb.bttn.home"
    When I click on "bjb.bttn.menu"
    Then I wait for "5" seconds

  @BJBSearch
  Scenario: BJB Search Suggestions
    Given I try to close application by name "Insights" and ignore error
    And I start application by name "Insights"
    And I wait for "5" seconds
    And I click on "bjb.bttn.home"
    When I click on "bjb.bttn.search"
    And I enter "Julius Baer" to "bjb.input.search"
    Then I wait for "5" seconds

  @BJBViewCalendar
  Scenario: BJB Event Calendar
    Given I try to close application by name "Insights" and ignore error
    And I start application by name "Insights"
    And I wait for "5" seconds
    When I click on "bjb.bttn.calendar"
    Then I click on "bjb.bttn.mon"
    And I click on "bjb.bttn.tue"
    And I click on "bjb.bttn.wed"
    And I click on "bjb.bttn.thu"
    And I click on "bjb.bttn.fri"
