@easyJet
Feature: easyJet Demo Scenarios

  @easyJetMobile
  Scenario: Book a flight on mobile app
    Given I set the device location to the address "London, UK"
    And I click on "ej.myTrips"
    And I click on "ej.home"
    And I click on "ej.flightTracker"
    And I click on "ej.home"
    And I click on "ej.boardingPasses"
    And I click on "ej.home"
    And I click on "ej.bookTrip"
    And I click on "ej.flyFrom"
    And I click on "ej.london"
    And I click on "ej.flyTo"
    And I click on "ej.amsterdam"
    And I click on "ej.departing"
    And I wait for "5" seconds
    And I click on "ej.departingDate"
    And I wait for "2" seconds
    And I click on "ej.returningDate"
    And I click on "ej.btn.done"
    And I click on "ej.showFlights"

  @easyJetMobileAndWeb
  Scenario: Book a flight on mobile app and carry on from web
    Given I switch to driver "perfectodevii"
    And I set the device location to the address "London, UK"
    And I click on "ej.bookTrip"
    And I click on "ej.flyFrom"
    And I click on "ej.london"
    And I click on "ej.flyTo"
    And I click on "ej.amsterdam"
    And I click on "ej.departing"
    And I wait for "5" seconds
    And I click on "ej.departingDate"
    And I wait for "2" seconds
    And I click on "ej.returningDate"
    And I click on "ej.btn.done"
    And I click on "ej.showFlights"
    And I switch to driver "perfectoRemote"
    And I maximize window
    And I am on "https://www.easyjet.com/en"
