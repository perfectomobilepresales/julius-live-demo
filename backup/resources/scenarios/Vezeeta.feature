@Vezeeta
Feature: Vezeeta Demo Scenarios

  @VezeetaMobile
  Scenario: Open website and verify logo present
    Given I open browser to webpage "https://www.vezeeta.com/en"
    And I wait for "3" seconds

  @VezeetaMobileApp
    Scenario: Start app and look for dermatologists
    Given I try to close application by name "Vezeeta" and ignore error
    And I start application by name "Vezeeta"
    And I wait for "2" seconds
    Then I click on "veezeta.bttn.specialty"
    And I click on "veezeta.bttn.derma"
    And I wait for "2" seconds
    When I set the device location to the address "Bahary, Alexandria, Egypt"
    And I click on "veezeta.bttn.location"
    And I wait for "1" seconds
    And I click on "veezeta.bttn.currentLoc"
    And I wait for "2" seconds
    Then I should see text "Randa Saad"
    And I wait for "2" seconds
    When I set the device location to the address "Qena, Qena City, Egypt"
    And I click on "veezeta.bttn.location"
    And I wait for "1" seconds
    And I click on "veezeta.bttn.currentLoc"
    And I wait for "2" seconds
    Then I should see text "George Gamil"
    And I wait for "2" seconds
    When I set the device location to the address "Menouf, Menoufia, Egypt"
    And I click on "veezeta.bttn.location"
    And I wait for "1" seconds
    And I click on "veezeta.bttn.currentLoc"
    And I wait for "2" seconds
    Then I should see text "Sally Salah"
    And I wait for "2" seconds
    When I set the device location to the address "Giza, El-Giza, Egypt"
    And I click on "veezeta.bttn.location"
    And I wait for "1" seconds
    And I click on "veezeta.bttn.currentLoc"
    And I wait for "2" seconds
    Then I should see text "Menna Bahgat"
    And I wait for "2" seconds
    When I set the device location to the address "Assiut, Dayrout, Egypt"
    And I click on "veezeta.bttn.location"
    And I wait for "1" seconds
    And I click on "veezeta.bttn.currentLoc"
    And I wait for "2" seconds
    Then I should see text "Ahmed AbdElsamei"
    And I try to close application by name "Vezeeta" and ignore error
