@Morrisons
Feature: Morrisons Demo Scenarios

  @MorrisonsMobile
  Scenario: Add things to basket
    Given I instrument app "GROUP:MorrisonsScanShop048.apk"
    And I set the device location to the address "Leeds, UK"
    And I wait for "2" seconds
    And I start application by ID "com.morrisons.customer.uat"
    And I switch to webview context
    And I click on "morrisons.continue"
    And I click on "morrisons.login"
    And I enter "sachin@more.com" to "morrisons.email"
    And I enter "password" to "morrisons.pass"
    And I click on "morrisons.logIn"
    And I click on "morrisons.next"
    And I click on "morrisons.next"
    And I click on "morrisons.next"
    And I click on "morrisons.ok"
    And I switch to native context
    And I click on "droid.bttn.allow"
    And I switch to webview context
    And I click on "morrisons.getStarted"
    And I click on "morrisons.scan"
    And I switch to native context
    And I click on "droid.bttn.allow"
    And I start inject "GROUP:Carrots.jpg" image to application id "com.morrisons.customer.uat"
#    And I uninstall application by ID "com.morrisons.customer.uat"

  @Morrisons2Mobiles
  Scenario: Approve basket with Colleagues app
    Given I switch to driver "perfecto"
    And I instrument app "GROUP:MorrisonsScanShop048.apk"
    And I set the device location to the address "Leeds, UK"
    And I start application by ID "com.morrisons.customer.uat"
    And I switch to webview context
    And I click on "morrisons.continue"
    And I click on "morrisons.login"
    And I enter "dmitriy@more.com" to "morrisons.email"
    And I enter "password" to "morrisons.pass"
    And I click on "morrisons.logIn"
    And I uninstall application by ID "com.morrisons.customer.uat"
    And I switch to driver "perfectodevii"
    And I instrument app "Group:MorrisonsColleague026.apk"
    And I set the device location to the address "Leeds, UK"
    And I start application by ID "com.morrisons.rescan.uat"
    And I switch to webview context
    And I enter "11223344" to "rescan.employeeID"
    And I enter "246810" to "rescan.password"
    And I uninstall application by ID "com.morrisons.rescan.uat"

  @MorrisonsWeb
  Scenario: Open website and login
    Given I open browser to webpage "https://groceries.morrisons.com/"
    And I maximize window
    And I click on "morrisons.login"