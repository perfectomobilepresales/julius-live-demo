@FiatMobileApp
Feature: Fiat Demo Feature

  @FiatMobileAppiOS
  Scenario: Register using email on iOS
    Given I try to close application by ID "com.fiat.uconnect" and ignore error
    And I start application by ID "com.fiat.uconnect"
    And I click on "fiat.bttn.register"
    And I enter "Perfecto" to "fiat.input.first"
    And I enter "Demo" to "fiat.input.last"
    And I enter "447789456123" to "fiat.input.mobile"
    And I enter "perfectodemouk@gmail.com" to "fiat.input.email"
    And I enter "A1B1c2d2e3f3" to "fiat.input.password"
    And I enter "A1B1c2d2e3f3" to "fiat.input.confirmPassword"
    And I swipe up a lot
    And I click on "fiat.chkbox.tnc"
    And I click on "fiat.radio.offers"
    And I click on "fiat.radio.improve"
    And I click on "fiat.radio.offersPartners"
    And I click on "fiat.radio.personalised"
    And I swipe up a lot
    When I click on "fiat.bttn.accept"
    And I wait for "2" seconds
    Then I click on "fiat.bttn.OK"

  @FiatMobileAppDroid
  Scenario: Register using email on Android
    Given I try to close application by ID "com.acn.uc" and ignore error
    And I start application by ID "com.acn.uc"
    And I click on "fiat.bttn.register"
    And I enter "Perfecto" to "fiat.input.first"
    And I enter "Demo" to "fiat.input.last"
    And I enter "447789456123" to "fiat.input.mobile"
    And I enter "perfectodemouk@gmail.com" to "fiat.input.email"
    And I enter "A1B1c2d2e3f3" to "fiat.input.password"
    And I enter "A1B1c2d2e3f3" to "fiat.input.confirmPassword"
    And I swipe up a lot
    And I click on "fiat.chkbox.tnc"
    And I click on "fiat.radio.offers"
    And I click on "fiat.radio.improve"
    And I click on "fiat.radio.offersPartners"
    And I click on "fiat.radio.personalised"
    And I swipe up a lot
    When I click on "fiat.bttn.accept"
    And I wait for "2" seconds
    Then I click on "fiat.bttn.OK"
