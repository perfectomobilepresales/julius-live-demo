@Aristocrat
Feature: Aristocrat Demo

  @AristocratApp
  Scenario: Aristocrat Play
    Given I try to close application by name "UnityTestLobby" and ignore error
    And I start DevTunnel on "mac"
    And I start application by name "UnityTestLobby"
    And I rotate the device to landscape
    And I wait for "10" seconds
    And I set up the Unity test config "192.168.0.100" and port "3030"
#    And I wait for "10" seconds
    And I stop DevTunnel
