@BA
Feature: BA Demo Scenarios

  @BADesktop
  Scenario: Open website and look for text
    Given I am on "http://www.perfecto.io"
    When I open browser to webpage "http://www.ba.com"
    And I maximize window
    And I accept BA cookies
    Then I login to BA
    And I wait for "3" seconds
    And I find "My Avios" on BA

  @BAMobile
  Scenario: Open app test
    Given I try to close application by name "British Airways" and ignore error
    And I start application by name "British Airways"
    And I click on "ba.bttn.book"
    And I click on "ba.bttn.from"
    And I enter "London" to "ba.text.airport"
    And I click on "ba.label.lhr"
    And I click on "ba.bttn.to"
    And I enter "Tokyo" to "ba.text.airport"
    And I click on "ba.label.nrt"
    When I click on "ba.bttn.find"
    Then I wait "10" seconds for "ba.label.title" to appear

