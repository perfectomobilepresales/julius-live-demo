@Aviva
Feature: My Aviva Demo Scenarios

  @MyAviva_Web
  Scenario Outline: Create car insurance quote on web
    Given I am on "https://www.direct.aviva.co.uk/MyAccount/login"
    And I get a car insurance quote for "<firstName>" "<lastName>" with "<dob>" at "<postcode>" and "<mobile>" for car registration number "<reg>"
    Examples:
      | recId | firstName | lastName  | dob         | postcode  | mobile      | reg       |
      | 1     | Jim	      | King      | 23/10/1977  | BS1 3AG   | 0777123456  | NU60 EJG  |

  @MyAviva_Mobile
  Scenario Outline: Retrieve car insurance quote on mobile
    Given I retrieve a car insurance quote for "<firstName>" "<lastName>" with "<dob>" at "<postcode>"
    Examples:
      | recId | firstName | lastName  | dob         | postcode  |
      | 1     | Jim	      | King      | 23/10/1977  | BS1 3AG   |

  @MyAvivaWebAndMobile
  Scenario Outline: New insurance quote on web and retrieve on mobile
    Given I switch to driver "perfectoRemote"
    And I am on "https://www.direct.aviva.co.uk/MyAccount/login"
    And I get a car insurance quote for "<firstName>" "<lastName>" with "<dob>" at "<postcode>" and "<mobile>" for car registration number "<reg>"
    And I switch to driver "perfectodevii"
    And I retrieve a car insurance quote for "<firstName>" "<lastName>" with "<dob>" at "<postcode>"
    Examples:
      | recId | firstName | lastName  | dob         | postcode  | mobile      | reg       |
      | 1     | Jim	      | King      | 23/10/1977  | BS1 3AG   | 0777123456  | NU60 EJG  |
