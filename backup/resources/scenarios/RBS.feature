@RBS
Feature: RBS Demo Scenarios

  @RBSMobile
    Scenario: Open app and login
    Given I try to close application by name "Royal Bank" and ignore error
    And I start application by name "Royal Bank"
    And I wait for "2" seconds
    And I click on "rbs.customer"
    And I click on "rbs.getStarted"
    And I click on "rbs.acceptTerms"
    And I enter "2312801234" to "rbs.customerNo"
    And I click on "rbs.next"
    And I enter "1" to "rbs.securityNoBox1"
    And I enter "2" to "rbs.securityNoBox2"
    And I enter "3" to "rbs.securityNoBox3"
    And I enter "a" to "rbs.passwordBox1"
    And I enter "b" to "rbs.passwordBox2"
    And I enter "c" to "rbs.passwordBox3"
    And I click on "rbs.next"
    And I should see text "Incorrect entry"
