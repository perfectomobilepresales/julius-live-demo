@Tide
Feature: Multi Device Demo Scenarios

  @TideOnTheWeb
  Scenario: Tide on the Web - VM and Mobile
    Given I switch to driver "perfecto"
    And I open browser to webpage "http://web.staging.tide.co"
    And I maximize window
    And I get QR code from Tide Web
    And I wait for "10" seconds
    And I switch to driver "perfectodevii"
    And I start inject "PRIVATE:Tide/TideQRCode.png" image to application name "Tide-staging"
    And I try to close application by name "Tide-staging" and ignore error
    When I start application by name "Tide-staging"
    And I wait for "2" seconds
    And I test fingerprint
    And I click on "tide.bttn.more"
    And I scroll down a lot
    And I click on "tide.bttn.loginOnComp"
    Then I click on "tide.bttn.gotit"
