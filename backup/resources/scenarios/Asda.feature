
### iOS Test Suite ###

@AsdaScanGoDemo
Feature: Asda Demo Scenarios

  @AsdaMobileAppiOS
  Scenario: To add an item to cart
    Given I try to close application by name "Scan & Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan & Go"
    And I wait for "1" seconds
    And I click on "asda.bttn.prod"
    And I click on "asda.bttn.done"
    And I login to Asda
    And I scan barcode "ASP1008799"
    And I wait for "1" seconds
    And I should see text "TABLE SALT"
#    And I scan barcode "Paracetamol"
#    And I should see text "PARACETAMOL"
#    And I scan barcode "Donuts"
#    And I should see text "DONUTS"
#    And I scan barcode "Scotch Whisky"
#    And I should see text "WHISKY"
    And I hit "OPEN_CHECKOUT"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I complete QC check
    Then I write comment "Awesome! This worked well at Didcot on iOS"
    And I hit "Send"
    And I finish shopping
    And I close application by name "Scan & Go"

  @AsdaMobileAppiOSSMS
  Scenario: SMS during adding an item to cart
    Given I try to close application by name "Scan & Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan & Go"
    And I wait for "1" seconds
    And I click on "asda.bttn.prod"
    And I click on "asda.bttn.done"
    And I login to Asda
    And I scan barcode "ASP1008799"
    And I wait for "1" seconds
    And I should see text "TABLE SALT"
    And I receive an SMS with the text: "Testing SMS during Scan & Go"
    And I wait for "12" seconds
    And I start application by name "Messages"
    And I hit "‪‭+44 7537 416555‬‬"
    And I swipe to go back
    And I wait for "1" seconds
    And I start application by name "Scan & Go"
    And I hit "OPEN_CHECKOUT"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I complete QC check
    Then I write comment "Awesome! This worked well at Didcot on iOS"
    And I hit "Send"
    And I finish shopping
    And I try to close application by name "Messages" and ignore error
    And I close application by name "Scan & Go"

  @AsdaMobileAppiOSCall
  Scenario: Call during adding an item to cart
    Given I try to close application by name "Scan & Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan & Go"
    And I wait for "1" seconds
    And I click on "asda.bttn.prod"
    And I click on "asda.bttn.done"
    And I login to Asda
    And I scan barcode "ASP1008799"
    And I wait for "1" seconds
    And I should see text "TABLE SALT"
    And I receive a phone call
    And I wait for "10" seconds
    And I answer phone call
    And I wait for "1" seconds
    And I end phone call
    And I start application by name "Scan & Go"
    And I hit "OPEN_CHECKOUT"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I complete QC check
    Then I write comment "Awesome! This worked well at Didcot on iOS"
    And I hit "Send"
    And I finish shopping
    And I close application by name "Scan & Go"

  @AsdaMobileAppiOSNetwork
  Scenario: To add an item to cart
    Given I try to close application by name "Scan & Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan & Go"
    And I wait for "1" seconds
    And I click on "asda.bttn.prod"
    And I click on "asda.bttn.done"
    And I login to Asda
    And I scan barcode "ASP1008799"
    And I wait for "1" seconds
    And I should see text "TABLE SALT"
    And I turn off WiFi
    And I hit "OPEN_CHECKOUT"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I turn on WiFi
    And I complete QC check
    Then I write comment "Awesome! This worked well at Didcot on iOS"
    And I hit "Send"
    And I finish shopping
    And I close application by name "Scan & Go"




### Android Test Suite ###

  @AsdaMobileAppDroid
  Scenario: To add an item to cart
    Given I try to close application by name "Scan And Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan And Go"
#    And I login to Asda
    And I scan barcode "ASP1008799"
    And I should see text "TABLE SALT"
#    And I scan barcode "Paracetamol"
#    And I should see text "PARACETAMOL"
#    And I scan barcode "Donuts"
#    And I should see text "DONUTS"
#    And I scan barcode "Scotch Whisky"
#    And I should see text "WHISKY"
    And I hit "Checkout"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I hit "Barcode Scanned"
    And I set the slider
    Then I write comment "Awesome! This worked well at Didcot on Android"
    And I hit "Send"
    And I hit "Finished"
    And I close application by name "Scan And Go"

  @AsdaMobileAppDroidSMS
  Scenario: SMS during adding an item to cart
    Given I try to close application by name "Scan And Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan And Go"
    And I login to Asda
    And I scan barcode "ASP1008799"
    And I should see text "TABLE SALT"
    And I receive an SMS with the text: "Testing SMS during Scan & Go"
    And I wait for "12" seconds
    And I start application by name "Messages"
    And I hit "CONVERSATIONS"
    And I hit "⁨+447537416555⁩"
    And I wait for "1" seconds
    And I start application by name "Scan And Go"
    And I hit "Checkout"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I hit "Barcode Scanned"
    And I set the slider
    Then I write comment "Awesome! This worked well at Didcot on Android"
    And I hit "Send"
    And I hit "Finished"
    And I try to close application by name "Messages" and ignore error
    And I close application by name "Scan And Go"

  @AsdaMobileAppDroidCall
  Scenario: Call during adding an item to cart
    Given I try to close application by name "Scan And Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan And Go"
    And I login to Asda
    And I scan barcode "ASP1008799"
    And I should see text "TABLE SALT"
    And I receive a phone call
    And I wait for "10" seconds
    And I answer phone call
    And I wait for "1" seconds
    And I end phone call
    And I start application by name "Scan And Go"
    And I hit "Checkout"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I hit "Barcode Scanned"
    And I set the slider
    Then I write comment "Awesome! This worked well at Didcot on Android"
    And I hit "Send"
    And I hit "Finished"
    And I close application by name "Scan And Go"

  @AsdaMobileAppDroidNetwork
  Scenario: To add an item to cart
    Given I try to close application by name "Scan And Go" and ignore error
    And I set the device location to the address "Asda Didcot Supermarket UK"
    And I wait for "1" seconds
    When I start application by name "Scan And Go"
    And I login to Asda
    And I scan barcode "ASP1008799"
    And I should see text "TABLE SALT"
    And I turn off WiFi
    And I hit "Checkout"
    And I hit "OK"
    And I scan barcode "QR Checkout"
    And I hit "Yes"
    And I turn on WiFi
    And I hit "Barcode Scanned"
    And I set the slider
    Then I write comment "Awesome! This worked well at Didcot on Android"
    And I hit "Send"
    And I hit "Finished"
    And I close application by name "Scan And Go"



### TC70X ###

  @AsdaScanScheduled
  Scenario: To scan scheduled stock
    Given I try to close application by name "Receiving" and ignore error
    And I start application by name "Receiving"
    And I wait "5" seconds to see the text "Depot Receiving"
    And I click on "asda.li.trailer1"
    And I click on "asda.chkbox.sealCheck"
    And I click on "asda.bttn.startReceiving"
    And I click on "droid.bttn.ok"
    And I click on "asda.bttn.doneScanning"
    And I get barcodes from received labels
    And I click on "asda.bttn.close"
    And I scan barcodes from received labels
    And I click on "asda.bttn.doneScanning"
    And I click on "asda.chkbox.noReturn"
    And I click on "asda.bttn.continue"
    And I wait "5" seconds to see the text "Receiving Summary"
    And I verify reverse logical type
    And I click on "asda.bttn.complete"
    And I wait "5" seconds to see the text "Receiving Complete"
    And I click on "asda.bttn.scheduled"

