@BMW
Feature: BMW Destinations Feature

  @BMWDestinations
  Scenario: Cycle Locations on iOS
    Given I try to close application by name "Connected" and ignore error
    And I set the device location to the address "Marienplatz, Munich"
    And I start application by name "Connected"
    When I click on "bmw.bttn.dest"
    And I click on "bmw.bttn.locate"
    And I zoom out on map
    And I go to the device home screen
    And I set the device location to the address "Swindon, UK"
    And I start application by name "Connected"
    And I click on "bmw.bttn.locate"
    And I zoom out on map
    And I go to the device home screen
    And I set the device location to the address "Paris, France"
    And I start application by name "Connected"
    And I click on "bmw.bttn.locate"
    And I zoom out on map
    And I go to the device home screen
    And I set the device location to the address "Shanghai, China"
    And I start application by name "Connected"
    And I click on "bmw.bttn.locate"
    And I zoom out on map
    And I go to the device home screen
    And I set the device location to the address "Tokyo, Japan"
    And I start application by name "Connected"
    And I click on "bmw.bttn.locate"
    And I zoom out on map
    Then I close application by name "Connected"
    And I reset the device location
