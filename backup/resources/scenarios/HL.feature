@HL
Feature: HL Demo Scenarios

  @HLMobileLogin
  Scenario: Open App and login
    Given I start HL mobile app
    And I start network virtualization with HAR capture
    And I wait for "2" seconds
    And I swipe left a lot
    And I wait for "1" seconds
    And I go to "Get Started"
    And I go to "Accept"
    And I go to "My accounts"
    And I go to "Log in"
    And I enter test login "clnt1704790" and dob "010263"
    And I wait for "30" seconds
    And I stop network virtualization
