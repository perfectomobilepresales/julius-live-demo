@Watts
Feature: Watts Demo Scenarios
#
#  @WattsRegisterXPath
#  Scenario: Register a new user by XPath
#    Given I try to close application by name "Watts Pro" and ignore error
#    And I start application by name "Watts Pro"
#    And I start network virtualization with HAR capture
#    And I swipe up a lot
#    And I click on "watts.bttn.login"
#    And I click on "ios.bttn.continue"
#    And I wait for "2" seconds
#    And I switch to webview context
#    And I login to Watts by XPath
#    And I wait for "10" seconds
#    And I stop network virtualization

  @WattsRegisterCSS
  Scenario: Register a new user by CSS
    Given I try to close application by name "Watts Pro" and ignore error
    And I start application by name "Watts Pro"
    And I swipe up a lot
    And I click on "watts.bttn.login"
    And I click on "ios.bttn.continue"
    And I wait for "2" seconds
    And I switch to webview context
    And I login to Watts by CSS
    And I wait for "10" seconds
#
#  @WattsRegister
#  Scenario: Register a new user by Quantum
#    Given I try to close application by name "Watts Pro" and ignore error
#    And I start application by name "Watts Pro"
#    And I swipe up a lot
#    And I click on "watts.bttn.login"
#    And I click on "ios.bttn.continue"
#    And I wait for "2" seconds
#    And I switch to webview context
#    And I click on "watts.bttn.signup"
#    And I wait for "2" seconds
#    And I enter "test@test.com" to "watts.input.email"
#    And I enter "test123456" to "watts.input.newPass"
#    And I enter "test123456" to "watts.input.confirmPass"
#    And I enter "Jim" to "watts.input.firstName"
#    And I enter "King" to "watts.input.lastName"
#    And I enter "JK Corp" to "watts.input.company"
#    And I enter "CEO" to "watts.input.jobTitle"
#    And I enter "1 Sunset Boulevard" to "watts.input.street"
#    And I enter "Santa Monica" to "watts.input.city"
#    And I enter "FL" to "watts.input.state"
#    And I enter "12345" to "watts.input.postcode"
#    And I enter "United States" to "watts.input.country"
#    And I swipe up a lot
#    And I click on "watts.chkbox.acceptTC"
#    And I click on "watts.bttn.create"
#    And I wait for "10" seconds
