@DKB
Feature: DKB Demo Scenarios

  @DKBRegisteriOS
  Scenario: Register mobile with OTP
    Given I try to close application by name "Nova Banking" and ignore error
    And I start application by name "Nova Banking"
    And I wait for "2" seconds
    And I enter "Testkunde_C" to "dkb.username"
    And I enter "test01$$" to "dkb.password"
    And I click on "dkb.login"
    And I click on "dkb.confirm"
    And I click on "dkb.start"
    And I wait "3" seconds for "dkb.pin" to appear
    And I send keys "12345"
    And I wait "3" seconds for "dkb.pin" to appear
    And I send keys "12345"
    And I click on "dkb.touchId"
    And I validate biometrics on "Nova Banking"
    And I wait "5" seconds for "dkb.otp" to appear
    And I wait "10" seconds for "dkb.useSMS" to appear
    And I click on "dkb.useSMS"
    And I click on "dkb.allow"
    When I click on "dkb.toBanking"
    Then I wait "10" seconds to see the text "Gesamter Kontostand"

  @DKBRegisterDroid
  Scenario: Register mobile with OTP
    Given I try to close application by name "Nova Banking" and ignore error
    And I start application by name "Nova Banking"
    And I wait for "2" seconds
    And I enter "Testkunde_G" to "dkb.username"
    And I enter "test01$$" to "dkb.password"
    And I click on "dkb.login"
    And I click on "dkb.confirm"
    And I click on "dkb.start"
    And I wait "3" seconds for "dkb.pin" to appear
    And I send keys "12345"
    And I wait "3" seconds for "dkb.pin" to appear
    And I send keys "12345"
    And I click on "dkb.touchId"
    And I validate biometrics on "Nova Banking"
    And I wait "5" seconds for "dkb.otp" to appear
    And I wait "20" seconds for "dkb.useSMS" to appear
    And I click on "dkb.useSMS"
    When I click on "dkb.toBanking"
    Then I wait "10" seconds to see the text "Gesamter Kontostand"
