@BnQDemo
Feature: B&Q Demo Scenarios

  @BnQMobileScan
  Scenario: To scan a product
    Given I try to close application by name "B&Q" and ignore error
    And I start device vital monitoring every "5" seconds
    And I start device logging
    And I start application by name "B&Q"
    And I click on "bnq.bttn.browse"
    And I start inject "GROUP:Julius/Kingfisher/dewalt.png" image to application name "B&Q"
    When I click on "bnq.bttn.scan"
    And I click on "bnq.bttn.allowCamera"
    And I click on "droid.bttn.allow"
    And I wait for "2" seconds
    And I click on "bnq.bttn.find"
    And I start inject "GROUP:Julius/Kingfisher/stove.png" image to application name "B&Q"
    And I click on "bnq.bttn.scan"
    And I wait for "2" seconds
    And I click on "bnq.bttn.find"
    And I start inject "GROUP:Julius/Kingfisher/sockets.png" image to application name "B&Q"
    And I click on "bnq.bttn.scan"
    Then I wait for "2" seconds
    And I stop image injection
    And I stop device logging
    And I stop device vitals

  @BnQMobileSearch
  Scenario: To find a product
    Given I try to close application by name "B&Q" and ignore error
    And I start application by name "B&Q"
    And I click on "bnq.bttn.find"
    And I click on "bnq.input.search"
    When I enter "Cordless grinder" to "bnq.input.search"
    And I press ENTER
    Then I wait "2" seconds to see the text "grinder"

  @BnQMobileStoreLocate
  Scenario: To find a store
    Given I try to close application by name "B&Q" and ignore error
    And I start application by name "B&Q"
    And I set the device location to the address "Croydon, UK"
    When I click on "bnq.bttn.stores"
    And I click on "bnq.bttn.useLocation"
    And I click on "droid.bttn.allow"
    And I wait for "2" seconds
    And I set the device location to the address "SN4 7PB"
    And I click on "bnq.bttn.locate"
    And I wait for "1" seconds
    And I set the device location to the address "BS1 3AG"
    And I click on "bnq.bttn.locate"
    Then I wait for "1" seconds