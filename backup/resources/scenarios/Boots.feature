@Boots
Feature: Boots Demo Scenarios

  @BootsMobile
  Scenario: Open menu and verify options using text analysis
    Given I open browser to webpage "https://boots.com"
    When I click on "boots.mobile.burger.menu.icon"
    And I wait for "3" seconds
    Then I must see text "services"
    And I must see text "Boots Advantage Card"

  @BootsMobile
  Scenario: Open website and verify Boots logo present
    Given I open browser to webpage "https://boots.com"
    And I wait for "3" seconds
    Then I want to see the Boots logo

  @BootsMobileApp
    Scenario: Start app and check for Boots logo present
    Given I start application by name "Boots"
    When I wait for "3" seconds
    Then I want to see the Boots logo
    And I try to close application by name "Boots" and ignore error

  @BootsMobileApp
    Scenario: Start app and check settings content
    Given I start application by name "Boots"
    And I wait for "3" seconds
    When I click on "boots.menu.btn"
    Then I must see text "Terms & conditions"
    And I try to close application by name "Boots" and ignore error

  @BootsMobileApp
  Scenario: Start app and check Advantage Card text
    Given I start application by name "Boots"
    And I wait for "3" seconds
    Then I must see text "My Advantage Card"
    And I try to close application by name "Boots" and ignore error

  @BootsMobileApp
    Scenario: Start app, set location to Nottingham and check for store
    Given I start application by name "Boots"
    And I wait for "3" seconds
    And I set the device location to the address "Nottingham, UK"
    And I scroll down
    When I click on "boots.store.locator.btn"
    Then I must see text "Nottingham"

  @BootsDesktop
  Scenario: Open website and search for hugo boss perfume
    Given I am on "https://boots.com"
    And I use JavaScript to click element "boots.shipping.location.btn"
    When I type "Boss" in input "boots.search.input"
    Then I must see text "Hugo Boss"

  @BootsDesktop
  Scenario: Open website and search for cold medicine
    Given I am on "https://boots.com"
    And I use JavaScript to click element "boots.shipping.location.btn"
    When I type "cold" in input "boots.search.input"
    Then I must see text "cold"

  @BootsPharmRegister
  Scenario: Register user on Pharmacy site
    Given I am on "https://www.boots.com/online/pharmacy/register"
    And I click on "boots.bttn.cookies"
    And I click on "boots.dropdown.title"
    And I enter "Jim" to "boots.input.first"
    And I enter "King" to "boots.input.last"
    And I swipe up a bit
    And I click on "boots.dropdown.day"
    And I click on "boots.dropdown.month"
    And I click on "boots.dropdown.year"
    And I enter "jimking@live.com" to "boots.input.email"
    And I enter "12345678abcde" to "boots.input.pass"
    And I enter "12345678abcde" to "boots.input.confirm"
    And I click on "boots.chk.terms"
    When I click on "boots.bttn.continue"
