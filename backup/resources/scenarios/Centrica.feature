@BritishGas
Feature: Centrica Demo Scenarios

  @BGMobileAppiOS
  Scenario: Authenticate with Face ID on iOS
    Given I try to close application by name "British Gas" and ignore error
    And I start application by name "British Gas"
    And I wait for "2" seconds
    When I set fingerprint with success result to application by name "British Gas"
    And I wait for "2" seconds
    And I should see text "Balance"
    And I click on "centrica.bttn.meter"
    And I wait for "2" seconds
    And I click on "centrica.bttn.usage"
    And I wait for "2" seconds
    And I click on "centrica.bttn.services"
    And I wait for "2" seconds
    And I click on "centrica.bttn.payments"
    And I wait for "2" seconds
    And I click on "centrica.bttn.account"
    And I wait for "2" seconds
    Then I should see text "Energy Account"
    And I wait for "3" seconds
    And I close application by name "British Gas"

    @BGMobileAppDroid
    Scenario: Initial customer sign in on Android
      Given I install application "PRIVATE:Centrica/app-release.apk"
      And I wait for "120" seconds
      And I start application by name "British Gas Smart"
      And I wait for "8" seconds
      And I click on "centrica.bttn.next"
      And I wait for "2" seconds
      And I click on "centrica.bttn.next"
      And I wait for "2" seconds
      And I click on "centrica.bttn.next"
      And I wait for "2" seconds
      And I click on "centrica.bttn.next"
      And I wait for "2" seconds
      And I click on "centrica.bttn.next"
      And I wait for "2" seconds
      And I click on "centrica.bttn.next"
      And I wait for "2" seconds
      And I click on "centrica.bttn.letsgo"
      And I wait for "2" seconds
      And I type "pay98@bgdigitaltest.co.uk" in input "centrica.bttn.login"
      And I click on "centrica.bttn.gonext"
      And I wait for "2" seconds
      And I click on "centrica.bttn.next"
      And I wait for "2" seconds
      And I click on "centrica.bttn.pass"
      And I wait for "2" seconds
      And I type "password12" in input "centrica.bttn.pass"
      And I click on "centrica.bttn.gonext"
      And I wait for "2" seconds
      When I click on "centrica.bttn.done"
      And I wait for "10" seconds
      Then I uninstall application by name "British Gas Smart"
