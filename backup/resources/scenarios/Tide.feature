@Tide
Feature: Tide Demo Scenarios

  @TideSetupiOS
  Scenario: Setup device on iOS
    Given I try to close application by name "Tide-Staging" and ignore error
    And I uninstall application by name "Tide-Staging"
    And I wait for "5" seconds
    And I install app "PRIVATE:Tide/Tide.ipa"

  @TideRecoveryiOS
  Scenario: Recovery on iOS
    Given I try to close application by name "Tide-Staging" and ignore error
    And I start application by name "Tide-Staging"
    And I hit "Allow"
    And I click on "tide.bttn.recovery"
    And I click on "tide.bttn.next"
    And I enter "perfectodemouk@gmail.com" to "tide.input.email"
    And I click on "tide.bttn.next"
    And I enter my phone number
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.yes"
    And I hit "OK"
    And I start inject "PRiVATE:Tide/uk-driving-licence-xs.jpg" image to application name "Tide-Staging"
    And I scan ID document
    And I stop image injection
    And I start inject "PRiVATE:Tide/selfie rotated.png" image to application name "Tide-Staging"
    And I click on "tide.bttn.scan"
    And I wait for "2" seconds
    And I stop image injection
    And I click on "tide.bttn.yes"
    And I input the security code "15973"
    And I input the security code "15973"
    And I wait for "10" seconds
    And I start application by name "Messages"
    And I click on "ios.bttn.tide"
    And I read the text message
    And I wait for "1" seconds
    And I swipe to go back
    And I click on "ios.bttn.edit"
    And I click on "ios.bttn.tide"
    And I click on "ios.bttn.del"
    And I start application by name "Tide-Staging"
    And I input the pass code
    When I click on "tide.bttn.next"
    And I close application by name "Messages"
    Then I close application by name "Tide-Staging"

  @TideLoginAppiOS
  Scenario: Login to App on iOS
    Given I try to close application by name "Tide-Staging" and ignore error
    When I start application by name "Tide-Staging"
    And I set fingerprint with error type "authFailed" result to application by name "Tide-Staging"
    And I input the security code "15973"
    Then I try to close application by name "Tide-Staging" and ignore error
    And I start application by name "Tide-Staging"
    And I set fingerprint with success result to application by name "Tide-Staging"

  @TideActivateCardiOS
  Scenario: Activate Card on iOS
    Given I try to close application by name "Tide-Staging" and ignore error
    When I start application by name "Tide-Staging"
    And I wait for "2" seconds
    And I set fingerprint with success result to application by name "Tide-Staging"
    And I start inject "PRiVATE:Tide/Tidecard_rotated.jpeg" image to application name "Tide-Staging"
    And I click on "tide.bttn.more"
    And I click on "tide.bttn.activate"
    And I click on "tide.bttn.activate"
    And I wait for "5" seconds
    When I click on "tide.bttn.activate"
    And I stop image injection
    Then I wait for "5" seconds

  @TideSignUpiOS
  Scenario: Sign up on iOS
    Given I try to close application by name "Tide-Staging" and ignore error
    And I start application by name "Tide-Staging"
    And I hit "Allow"
    And I click on "tide.bttn.signup"
    And I wait for "1" seconds
    And I click on "tide.bttn.next"
    And I enter a random email on Tide
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.OK"
    And I enter "Testing" to "tide.input.first"
    And I enter "Demo" to "tide.input.last"
    And I click on "tide.bttn.done"
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.next"
    And I enter my phone number
    And I click on "tide.bttn.next"
    And I enter "1" to "tide.input.flatno"
    And I enter "SN4 7PB" to "tide.input.postcode"
    And I click on "tide.bttn.done"
    And I click on "tide.bttn.search"
    And I click on "tide.bttn.address"
    And I click on "tide.bttn.morethan3"
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.confirm"
    And I click on "tide.bttn.soletrader"
    And I enter "Perfecto Demo" to "tide.input.soletrader"
    And I click on "tide.bttn.next"
    And I click on "tide.input.businesstype"
    And I enter "testing" to "tide.input.search"
    And I wait for "2" seconds
    And I click on "tide.bttn.results"
    And I click on "tide.bttn.next"
    And I click on "tide.chkbox.agree"
    And I click on "tide.bttn.confirm"
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.okgotit"
    And I start inject "PRiVATE:Tide/uk-driving-licence-xs.jpg" image to application name "Tide-Staging"
    And I click on "tide.bttn.driverlic"
    And I click on "tide.bttn.readable"
    And I stop image injection
    #And I start inject "PRiVATE:Tide/selfie rotated.png" image to application name "Tide-Staging"
    And I click on "tide.bttn.tap2cont"
    #And I stop image injection
    And I click on "tide.bttn.back"
    And I click on "tide.bttn.close"
    And I click on "tide.bttn.continue"
    And I start inject "PRiVATE:Tide/uk-driving-licence-xs.jpg" image to application name "Tide-Staging"
    And I scan ID document
    And I stop image injection
    And I start inject "PRiVATE:Tide/selfie rotated.png" image to application name "Tide-Staging"
    And I click on "tide.bttn.scan"
    And I wait for "2" seconds
    And I stop image injection
    And I click on "tide.bttn.yes"
    And I input the security code "15973"
    And I input the security code "15973"
    And I click on "tide.bttn.OK"
    And I wait for "12" seconds
    And I start application by name "Messages"
    And I click on "ios.bttn.tide"
    And I read the sign up SMS message
    And I wait for "1" seconds
    And I swipe to go back
    And I click on "ios.bttn.edit"
    And I click on "ios.bttn.tide"
    And I click on "ios.bttn.del"
    And I start application by name "Tide-Staging"
    And I input the confirmation code
    And I close application by name "Tide-Staging"

  @TideOnTheWebiOS
  Scenario: Tide on the Web - VM and Mobile
    Given I switch to driver "perfectoRemote"
    And I open browser to webpage "http://web.staging.tide.co"
    And I maximize window
    And I get QR code from Tide Web
    And I wait for "10" seconds
    And I switch to driver "perfectodevii"
    And I try to close application by name "Tide-Staging" and ignore error
    When I start application by name "Tide-Staging"
    And I wait for "2" seconds
    And I validate fingerprint on "Tide-staging"
    And I start inject "PRIVATE:Tide/TideQRCode.png" image to application name "Tide-Staging"
    And I click on "tide.bttn.more"
    And I scroll down a lot
    And I click on "tide.bttn.loginOnComp"
    And I wait for "3" seconds
    And I switch to driver "perfectoRemote"
    And I wait for "30" seconds


  @TideSetupDroid
  Scenario: Setup device on Android
    Given I try to close application by name "Tide-staging" and ignore error
    And I uninstall application by name "Tide-staging"
    And I wait for "5" seconds
    And I install app "PRIVATE:Tide/Tide.apk"

  @TideRecoveryDroid
  Scenario: Recovery on Android
    Given I try to close application by name "Tide-staging" and ignore error
    And I start application by name "Tide-staging"
    And I wait for "3" seconds
    And I click on "tide.bttn.recovery"
    And I click on "tide.bttn.next"
    And I enter "perfectodemouk@gmail.com" to "tide.input.email"
    And I click on "tide.bttn.next"
    And I enter my phone number
    And I click on "tide.bttn.next"
    And I hit "ALLOW"
    And I click on "tide.bttn.yes"
    And I hit "ALLOW"
    And I start inject "PRiVATE:Tide/uk-driving-licence.jpg" image to application name "Tide-staging"
    And I scan ID document
    And I stop image injection
    And I start inject "PRiVATE:Tide/selfie.png" image to application name "Tide-staging"
    And I click on "tide.bttn.scan"
    And I wait for "2" seconds
    And I stop image injection
    And I click on "tide.bttn.yes"
    And I input the security code "85973"
    And I input the security code "85973"
    And I wait for "10" seconds
    When I click on "droid.bttn.allow"
#    And I start application by name "Messages"
#    And I click on "droid.bttn.tide"
#    And I click on "droid.bttn.del"
#    And I click on "droid.bttn.all"
#    And I click on "droid.bttn.del"
#    And I click on "droid.bttn.del"
#    And I close application by name "Messages"
    Then I close application by name "Tide-staging"

  @TideLoginAppDroid
  Scenario: Login to App on Android
    Given I try to close application by name "Tide-staging" and ignore error
    When I start application by name "Tide-staging"
    And I wait for "2" seconds
    And I validate fingerprint on "Tide-staging"
    And I wait for "1" seconds

  @TideActivateCardDroid
  Scenario: Activate Card on Android
    Given I try to close application by name "Tide-staging" and ignore error
    When I start application by name "Tide-staging"
    And I wait for "2" seconds
    And I validate fingerprint on "Tide-staging"
    And I start inject "PRiVATE:imageID.png" image to application name "Tide-staging"
    And I click on "tide.bttn.more"
    And I click on "tide.bttn.cards"
    And I wait for "2" seconds
    And I swipe left a lot
    And I click on "tide.bttn.activate"
    And I wait for "3" seconds
    When I click on "tide.bttn.activate"
    And I stop image injection
    Then I wait for "5" seconds

  @TideSignUpDroid
  Scenario: Sign up on Android
    Given I try to close application by name "Tide-staging" and ignore error
    And I start application by name "Tide-staging"
    And I wait for "3" seconds
    And I click on "tide.bttn.signup"
    And I click on "tide.bttn.next"
    And I enter a random email on Tide
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.next"
    And I enter "Testing" to "tide.input.first"
    And I enter "Demo" to "tide.input.last"
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.next"
    And I click on "tide.input.dob"
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I scroll up a bit
    And I click on "tide.dial.year"
    And I click on "tide.bttn.OK"
    And I click on "tide.bttn.next"
    And I enter my phone number
    And I click on "tide.bttn.next"
    And I enter "1" to "tide.input.flatno"
    And I enter "SN4 7PB" to "tide.input.postcode"
    And I click on "tide.bttn.search"
    And I click on "tide.bttn.address"
    And I click on "tide.bttn.morethan3"
    And I click on "tide.bttn.next"
    And I click on "tide.bttn.confirm"
    And I click on "tide.bttn.soletrader"
    And I enter "Perfecto Demo" to "tide.input.soletrader"
    And I click on "tide.bttn.next"
    And I click on "tide.input.businesstype"
    And I enter "testing" to "tide.input.search"
    And I wait for "1" seconds
    And I click on "tide.bttn.results"
    And I click on "tide.bttn.next"
    And I click on "tide.chkbox.agree"
    And I click on "tide.bttn.confirm"
    And I click on "tide.bttn.next"
    And I click on "droid.bttn.allow"
    And I click on "tide.bttn.driverlic"
    And I start inject "PRiVATE:Tide/uk-driving-licence3.jpg" image to application name "Tide-staging"
    And I wait for "1" seconds
    And I click on "tide.bttn.close"
    And I stop image injection
    And I click on "tide.bttn.continue"
    And I start inject "PRiVATE:Tide/uk-driving-licence3.jpg" image to application name "Tide-staging"
    And I scan ID for "60" seconds
    And I stop image injection
    And I start inject "PRiVATE:Tide/selfie.png" image to application name "Tide-staging"
    And I click on "tide.bttn.capture"
    And I stop image injection
    When I click on "tide.bttn.continue"
    And I input the security code "85973"
    And I input the security code "85973"
    And I wait for "12" seconds
    Then I click on "droid.bttn.allow"
#    And I input the security code "85973"
    And I close application by name "Tide-staging"

  @TideOnTheWebDroid
  Scenario: Tide on the Web - VM and Mobile
    Given I switch to driver "perfectoRemote"
    And I open browser to webpage "http://web.staging.tide.co"
    And I maximize window
    And I get QR code from Tide Web
    And I wait for "10" seconds
    And I switch to driver "perfectodevii"
    And I try to close application by name "Tide-staging" and ignore error
    When I start application by name "Tide-staging"
    And I wait for "2" seconds
    And I validate fingerprint on "Tide-staging"
    And I start inject "PRIVATE:Tide/TideQRCode.png" image to application name "Tide-staging"
    And I click on "tide.bttn.more"
    And I scroll down a lot
    And I click on "tide.bttn.loginOnComp"
    And I click on "tide.bttn.gotit"
    And I wait for "3" seconds
    And I switch to driver "perfectoRemote"
    And I wait for "30" seconds
