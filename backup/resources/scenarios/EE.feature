@EE
Feature: EE Demo Scenarios

  @IVR1
  Scenario: Call 150 IVR
    Given I make audio call "150"
    And I start audio recording sequence "1"
    And I click on "//*[@text='Keypad']"
    And I wait for "50" seconds
    And I stop audio recording
    And I start audio recording sequence "2"
    And I press "1" on the keypad
    And I wait for "35" seconds
    And I stop audio recording
    And I start audio recording sequence "3"
    And I press "one" on the keypad
    And I wait for "10" seconds
    And I stop audio recording
    And I start audio recording sequence "4"
    And I press "1" on the keypad
    And I wait for "15" seconds
    And I stop audio recording
    When I hang up the phone
    Then I validate audio sequences:
      | seq | message                                   |
      |  1  | welcome to EE business support            |
      |  1  | doing everything we can                   |
      |  1  | live chat to our team                     |
      |  1  | calling about your mobile phone press one |
      |  1  | unlimited broadband press two             |
      |  1  | mobile broadband is three                 |
      |  2  | to make a payment, press one              |
      |  2  | stolen or damaged, press two              |
      |  2  | technical support, press three            |
      |  2  | leaving us and require a pack, its four   |
      |  2  | to hear those options again, press hash   |
      |  3  | allowance and usage, press one            |
      |  3  | to make a payment, press two              |
