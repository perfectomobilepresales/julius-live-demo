@CSG
Feature: CSG Demo Scenarios

  @TalkTalkMobileAppiOS
  Scenario: Sign in and sign out
    Given I try to close application by name "TalkTalk TV" and ignore error
    And I start application by name "TalkTalk TV"
    When I click on "signin.email.password.btn"
    And I type "tttvtestdesk2@gmail.com" in element "email.input"
    And I type "Test2_TalkTalk" in element "password.input"
    And I click on "forgot.password.label"
    And I click on "signin.btn"
    And I wait "15" seconds to see the text "Guide"
    Then I click on "account.btn"
    And I click on "account.settings.account"
    And I click on "signout.btn"
    And I click on "signout.alert.confirm"
    And I close application by name "TalkTalk TV"

  @TalkTalkMobileAppAndroid
  Scenario: Sign in and sign out
    Given I try to close application by name "TalkTalk TV" and ignore error
    And I start application by name "TalkTalk TV"
    When I click on "signin.email.password.btn" when visible
    And I type "tttvtestdesk2@gmail.com" in element "email.input"
    And I type "Test2_TalkTalk" in element "password.input"
    And I hide keyboard
    And I click on "signin.btn" when visible
    And I wait "30" seconds to see the text "Home"
    Then I click on "more.btn" when visible
    And I click on "more.settings.account" when visible
    And I scroll down
    And I click on "more.settings.account.account" when visible
    And I click on "signout.btn" when visible
    And I click on "signout.alert.confirm" when visible
    And I close application by name "TalkTalk TV"

  @TalkTalkMobileAppAndroidVideo
  Scenario: Verify video playback
    Given I try to close application by name "TalkTalk TV" and ignore error
    And I start application by name "TalkTalk TV"

    And I scroll a lot
    And I wait for "3" seconds
    And I click on "movie" when visible
    And I click on "trailer.btn" when visible
    And I wait for "3" seconds
    Then the video is playing
    #And I double click on "video.details.container"
    #And I click on "video.back" when visible
    And I press on back button
    And I wait for "3" seconds
    And I take a device screenshot
#    Then I click on "more.btn" when visible
#    And I click on "more.settings.account" when visible
#    And I scroll down
#    And I click on "more.settings.account.account" when visible
#    And I click on "signout.btn" when visible
#    And I click on "signout.alert.confirm" when visible
#    And I close application by name "TalkTalk TV"

  @TalkTalkDesktopWeb
  Scenario: Sign in and sign out
    Given I open browser to webpage "https://www.talktalktv.co.uk"
    And I maximize window
    Then I click on "navigation.signin.btn"
    And I type "tttvtestdesk2@gmail.com" in input "signin.email.input"
    And I type "Test2_TalkTalk" in input "signin.password.input"
    And I click on "signin.btn"
    And I click on "intro.guide.continue.btn"
    And I click on "intro.guide.continue.btn"
    And I click on "intro.guide.continue.btn"
    And I click on "intro.guide.continue.btn"
    And I click on "intro.guide.continue.btn"
    #And I click on "intro.guide.useful.btn"
    And I click "intro.guide.getstarted.btn" using JavaScript
    #And I click on "signout.btn"
    And I hover and then click on Sign Out
    And I wait for "3" seconds
    And I wait "3" seconds to see the text "On now"