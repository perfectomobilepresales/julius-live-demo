@BT
Feature: BT Demo Scenarios

  @BTDesktop
  Scenario: Open website and look for name
    Given I am on "https://www.bt.com/broadband"
    And I maximize window
    And I accept BT cookies
    When I login to BT Broadband
    Then I find "Julius" on BT Broadband

  @BTAmazonFirePlay
  Scenario: Launch YouTube and see program
    Given I try to close application by name "YouTube" and ignore error
    And I start application by name "YouTube"
    And I wait for "10" seconds
    And I press DOWN
    And I wait for "2" seconds
    When I press RIGHT
    And I wait for "2" seconds
    And I press RIGHT
    And I wait for "2" seconds
    And I press DOWN
    And I wait for "4" seconds
    And I press ENTER
    And I wait for "10" seconds
    And I press PLAYorPAUSE
    And I wait for "5" seconds
    And I press PLAYorPAUSE
    And I wait for "10" seconds
    Then I press BACK
    And I press BACK
    And I should see text "HOME"

  @BTAmazonFireMenu
  Scenario: Launch YouTube and navigate menu
    Given I try to close application by name "YouTube" and ignore error
    And I start application by name "YouTube"
    And I wait for "10" seconds
    When I press LEFT
    And I wait for "1" seconds
    And I press DOWN
    And I wait for "1" seconds
    And I press DOWN
    And I wait for "1" seconds
    When I press DOWN
    And I wait for "2" seconds
    Then I should see text "GAMING"
