@VirginMedia
Feature: Virgin Media Demo Scenarios

  @VirginMedia_Web
  Scenario: Open VM homepage and check coverage
    Given I am on "https://www.virginmedia.com"
    And I maximize window
    And I click on "vm.bttn.accept" and ignore errors
    And I click on "vm.bttn.broadband"
    And I enter "SN4 7PB" to "vm.input.postcode"
    And I click on "vm.bttn.search"
    And I click on "vm.li.item"
    And I click on "vm.bttn.next"
    And I enter "John" to "vm.input.firstName"
    And I enter "Doe" to "vm.input.lastName"
    And I enter "johndoe@email.com" to "vm.input.email"
    And I enter "07787123456" to "vm.input.mobile"
    And I click on "vm.bttn.logo"
    And I click on "vm.bttn.reset"