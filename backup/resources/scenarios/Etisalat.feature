@Etisalat
Feature: Etisalat Demo Scenarios

  @MyEtisalat
    Scenario: Navigate My Etisalat app and pick location
    Given I try to close application by name "My Etisalat" and ignore error
    And I set the device location to the address "Burj Khalifa, UAE"
    And I start application by name "My Etisalat"
    And I wait "5" seconds for "eti.guest" to appear
    And I click on "eti.guest"
    And I wait "5" seconds for "eti.home" to appear
    And I scroll up a lot
    And I swipe left a lot
    And I click on "eti.plan"
    And I wait "5" seconds for "eti.prepaid" to appear
    And I scroll up a lot
    And I click on "eti.prepaid"
    And I scroll up a lot
    And I wait for "2" seconds
    And I click on "eti.controlLine"
    And I scroll up a lot
    And I wait for "2" seconds
    And I click on "eti.devices"
    And I wait for "3" seconds
    And I click on "eti.eLife"
    And I wait "10" seconds for "eti.pickLocation" to appear
    And I click on "eti.pickLocation"
    And I wait "5" seconds for "eti.locate" to appear
    And I click on "eti.locate"
    And I wait "5" seconds for "eti.confirmLocation" to appear
    And I click on "eti.confirmLocation"
    And I wait "30" seconds for "eti.location" to appear
    And I wait "10" seconds to see the text "Burj Khalifa"
