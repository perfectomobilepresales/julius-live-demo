@EDF
Feature: EDF Demo Scenarios

  @EDF
  Scenario: Open website and get a quote
    Given I open browser to webpage "https://my.edfenergy.com"
    And I maximize window
    And I click on "edf.bttn.quote"
    And I enter "SN4 7PB" to "edf.input.postcode"
    And I click on "edf.bttn.find"
    And I wait for "2" seconds
    And I click on "edf.li.select"
    And I click on "edf.bttn.continue"
    And I click on "edf.bttn.electricity"
    And I click on "edf.bttn.IKnowInKWhs"
    And I enter "4857" to "edf.input.kWh"
    And I click on "edf.bttn.yesCompare"
    And I click on "edf.menu.paymentMethod"
    And I click on "edf.menu.monthly"
    And I click on "edf.menu.tariff"
    And I click on "edf.menu.tariffOption"
    And I enter "perfectodemo@email.com" to "edf.input.email"
    And I enter "Perfecto" to "edf.input.first"
    And I enter "Demo" to "edf.input.last"
    And I enter "0789654321" to "edf.input.phone"
    And I click on "edf.bttn.showMe"
