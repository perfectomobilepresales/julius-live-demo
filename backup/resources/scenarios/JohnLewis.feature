@JohnLewis
Feature: John Lewis Demo Scenarios

  @JLiOSMobileApp
  Scenario: Add product to basket in John Lewis app
    Given I try to close application by name "John Lewis" and ignore error
    And I start application by name "John Lewis"
    And I click on "jl.bttn.dept"
    And I swipe up a bit
    And I wait "2" seconds for "jl.bttn.babyChild" to appear
    And I click on "jl.bttn.babyChild"
    And I click on "jl.bttn.shopschoolwear"
    And I swipe up a bit
    And I click on "jl.bttn.shoes"
    And I click on "jl.bttn.shoe"
    And I swipe up a bit
    And I click on "jl.bttn.size"
    And I wait "2" seconds for "jl.bttn.addBasket" to appear
    When I click on "jl.bttn.addBasket"
    And I click on "jl.bttn.basket"
    And I wait for "1" seconds
    Then I should see text "Clarks Children's Apollo Step"
    And I wait for "2" seconds
    And I click on "jl.bttn.minus"
    And I try to close application by name "John Lewis" and ignore error

  @JLAndroidMobileApp
  Scenario: Find nearest store in John Lewis app
    Given I try to close application by name "John Lewis & Partners" and ignore error
    And I start application by name "John Lewis & Partners"
    And I wait for "2" seconds
    And I bring up the JL menu
    Then I set the device location to the address "Manchester Chinatown"
    And I click on "jl.bttn.shopFinder"
    And I click on "jl.bttn.viewShop"
    And I wait for "2" seconds
    And I press on back button
    And I press on back button
    And I bring up the JL menu
    Then I set the device location to the address "Edinburgh Castle"
    And I click on "jl.bttn.shopFinder"
    And I click on "jl.bttn.viewShop"
    And I wait for "2" seconds
    And I press on back button
    And I press on back button
    And I bring up the JL menu
    Then I set the device location to the address "Cabot Circus, Bristol"
    And I click on "jl.bttn.shopFinder"
    And I click on "jl.bttn.viewShop"
    And I wait for "2" seconds
    And I zoom in on map
    And I wait for "3" seconds
    And I try to close application by name "John Lewis & Partners" and ignore error

  @WaitroseiOSMobileApp
  Scenario: Add product to basket in Waitrose app
    Given I try to close application by name "Waitrose" and ignore error
    And I start application by name "Waitrose"
    And I click on "ws.bttn.search"
    And I swipe up a bit
    And I wait "2" seconds for "ws.bttn.organic" to appear
    And I click on "ws.bttn.organic"
    And I click on "ws.bttn.freshchilled"
    And I click on "ws.bttn.product1"
    And I click on "ws.bttn.product2"
    And I swipe up a bit
    And I click on "ws.bttn.product3"
    And I click on "ws.bttn.trolley"
    And I wait "2" seconds to see the text "TROLLEY"
    And I wait for "5" seconds
    And I click on "ws.bttn.empty"
    And I wait for "1" seconds
    When I click on "ws.bttn.yes"
    Then I wait "2" seconds to see the text "Start browsing"
    And I try to close application by name "Waitrose" and ignore error