@Engie
Feature: Engie Demo Scenarios

  @EngieMobile
    Scenario: Open app and verify BE prices
    Given I try to close Engie application
    And I start Engie application
    And I wait for "2" seconds
    And I click on "eng.powerBE"
    And I click on "eng.cal"
    And I view period "CAL-22"
    And I click on "eng.cal"
    And I view period "Q4-20"
    And I click on "eng.cal"
    And I view period "Q2-21"
