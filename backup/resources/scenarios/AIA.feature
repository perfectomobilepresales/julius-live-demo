@AIA
Feature: AIA Demo Scenarios

  @ValidateLogin
  Scenario Outline: AIA Validate Login
    Given I wait "5" seconds for "aia.login" to appear
    And I netw
    And I clear "aia.login"
    And I enter "<username>" to "aia.username"
    And I enter "<password>" to "aia.password"
    And I click on "aia.login"
    And I restart application by name "My AIA SG"
    Examples:
        | username          | password    |
        | test1@perfecto.io | Perfecto123 |
        | demo@perfecto.io  | 456Perfecto |
        | trial@perfecto.io | Test789ABC  |

  @CheckAboutVitality
  Scenario: AIA Check About Vitality
    Given I click on "aia.menu"
    And I click on "aia.vitality"
    And I click on "aia.about"
    And I swipe up a lot

  @CheckRewardsPartners
  Scenario: AIA Check Rewards and Partners
    Given I click on "aia.menu"
    And I click on "aia.rewards"
    And I swipe up a lot
