@Leeds
Feature: Leeds Demo Scenarios

  @LBSMobileWeb
  Scenario: Mortgage borrowing calculator
    Given I open browser to webpage "http://www.leedsbuildingsociety.co.uk/"
    And I wait for "1" seconds
    And I click on "leeds.bttn.menu"
    And I wait for "1" seconds
    And I click on "leeds.menu.mortgages"
    And I swipe up a lot
    And I swipe up a lot
    And I click on "leeds.bttn.borrowingcalc"
    And I click on "leeds.bttn.continue"
    And I click on "leeds.bttn.residential"
    And I click on "leeds.bttn.yes"
    And I click on "leeds.dd.england"
    And I scroll up
    And I click on "leeds.bttn.1"
    And I click on "leeds.dd.yes"
    And I enter "80000" to "leeds.input.borrowinginterest"
    And I scroll up
    And I enter "300000" to "leeds.input.purchaseprice"
    And I click on "leeds.bttn.continueincome"
    And I wait for "1" seconds
    When I click on "leeds.bttn.addincome"
    And I wait for "2" seconds
    Then I should see text "Applicant 1"

  @LBSMobileWeb
  Scenario: Find the branch for Wiltshire
    Given I open browser to webpage "http://www.leedsbuildingsociety.co.uk/"
    And I wait for "1" seconds
    And I click on "leeds.bttn.menu"
    And I wait for "1" seconds
    And I click on "leeds.menu.findbranch"
    And I wait for "1" seconds
    And I enter "Swindon" to "leeds.input.findbranch"
    When I click on "leeds.bttn.search"
    And I wait for "3" seconds
    Then I should see text "Cheltenham"

  @LBSMobileWeb
  Scenario: Find the branch for East Midlands
    Given I open browser to webpage "http://www.leedsbuildingsociety.co.uk/"
    And I wait for "1" seconds
    And I click on "leeds.bttn.menu"
    And I wait for "1" seconds
    And I click on "leeds.menu.findbranch"
    And I wait for "1" seconds
    And I enter "Kinouton" to "leeds.input.findbranch"
    When I click on "leeds.bttn.search"
    And I wait for "3" seconds
    Then I should see text "Nottingham"
