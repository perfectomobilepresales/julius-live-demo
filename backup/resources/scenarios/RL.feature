@RoyalLondon
Feature: Royal London Demo Scenarios

  @RL_614627_iOS
    Scenario: PRE_Successful Login_Consumer iOS
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I click on "rl.bttn.login"
    And I enter "uat33@rlg.com" to "rl.input.email"
    And I enter "Welcome01!" to "rl.input.password"
    And I click on "rl.bttn.next"
    And I click on "rl.bttn.login"
    And I wait for "10" seconds
    When I click on "rl.bttn.anotherTime"
    Then I wait "30" seconds to see the text "Your plan value"
    And I click on "rl.bttn.profile"
    And I click on "rl.bttn.logout"
    And I close application by name "Royal London"
    And I start application by name "Royal London"
    And I wait "5" seconds for "rl.input.password" to appear
    And I enter "Welcome01!" to "rl.input.password"
    And I click on "rl.bttn.next"
    And I click on "rl.bttn.login"
    And I click on "rl.bttn.noThanks"

  @RL_614627_iOS_FaceID
  Scenario: PRE_Successful Login_Consumer iOS Face ID
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I click on "rl.bttn.login"
    And I enter "uat33@rlg.com" to "rl.input.email"
    And I enter "Welcome01!" to "rl.input.password"
    And I click on "rl.bttn.next"
    And I click on "rl.bttn.login"
    And I wait for "10" seconds
    When I click on "rl.bttn.turnOnFaceID"
    And I click on "rl.bttn.ok"
    And I validate Face ID on "Royal London"
    Then I wait "30" seconds to see the text "Your plan value"
    And I click on "rl.bttn.profile"
    And I click on "rl.bttn.logout"
    And I close application by name "Royal London"
    And I start application by name "Royal London"
    And I wait "5" seconds for "rl.bttn.faceID" to appear
    And I click on "rl.bttn.faceID"
    And I validate Face ID on "Royal London"

  @RL_614671_iOS
  Scenario: Income Release iOS
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I click on "rl.bttn.login"
    And I enter "uat33@rlg.com" to "rl.input.email"
    And I enter "Welcome01!" to "rl.input.password"
    And I click on "rl.bttn.next"
    And I click on "rl.bttn.login"
    And I wait for "10" seconds
    And I click on "rl.bttn.anotherTime"
    And I wait "30" seconds to see the text "Your plan value"
    When I click on "rl.bttn.plan"
    And I wait "2" seconds for "rl.label.taxFree" to appear
    And I wait "2" seconds for "rl.label.entitled" to appear
    And I wait "2" seconds for "rl.label.yourRegIncome" to appear
    And I wait "2" seconds for "rl.label.taxFreePayments" to appear
    And I wait "2" seconds for "rl.label.taxablePayments" to appear
    And I wait "2" seconds for "rl.label.frequency" to appear
    And I wait "2" seconds for "rl.label.nextPayment" to appear
    And I wait "2" seconds for "rl.label.withFlexible" to appear
    Then I click on "rl.bttn.back"
    And I scroll down a lot
    And I click on "rl.bttn.exploreFuture"
    When I click on "rl.bttn.readAssumptions"
    Then I wait "5" seconds to see the text "Assumptions"
    And I click on "rl.bttn.close"
    And I click on "rl.bttn.useTargetAge"
    And I wait "30" seconds to see the text "Your future outlook"
    When I click on "rl.bttn.ourAssumptions"
    Then I wait "30" seconds to see the text "Assumptions"
    And I click on "rl.bttn.close"
    When I click on "rl.bttn.onlineServices"
    Then I wait "30" seconds to see the text "Log in - Planholder"
    And I click on "rl.bttn.done"
    And I click on "rl.bttn.low"
    And I click on "rl.bttn.mid"
    And I click on "rl.bttn.high"
    And I click on "rl.bttn.back"
    And I click on "rl.bttn.showWhenIncomeRunsOut"
    And I wait "30" seconds to see the text "Your future outlook"
    When I click on "rl.bttn.ourAssumptions"
    Then I wait "30" seconds to see the text "Assumptions"
    And I click on "rl.bttn.close"
    When I click on "rl.bttn.onlineServices"
    Then I wait "30" seconds to see the text "Log in - Planholder"
    And I click on "rl.bttn.done"
    And I click on "rl.bttn.low"
    And I click on "rl.bttn.mid"
    And I click on "rl.bttn.high"
    And I click on "rl.bttn.back"
    And I click on "rl.bttn.back"


  @RL_614629_iOS
  Scenario: Error_Code_Handling iOS
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I click on "rl.bttn.register"
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Please enter your plan number"
    And I click on "rl.bttn.ok"
    And I enter "2628944" to "rl.input.planNo"
    And I enter "DZJRHDXY" to "rl.input.surname"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    And I click on "rl.bttn.next"
    And I wait for "5" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Please enter your date of birth"
    And I click on "rl.bttn.ok"
    And I click on "rl.input.dob"
    And I click on "rl.bttn.done"
    And I wait for "2" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Please check your details"
    And I click on "rl.bttn.ok"
    And I click on "rl.input.dob"
    And I enter "28" to "rl.dial.day"
    And I enter "September" to "rl.dial.month"
    And I enter "1953" to "rl.dial.year"
    And I click on "rl.bttn.done"
    And I wait for "2" seconds
    And I click on "rl.bttn.next"
    And I wait for "5" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Please enter your email address"
    And I click on "rl.bttn.ok"
    When I enter a random email on RL
    And I wait for "2" seconds
    And I click on "rl.bttn.next"
    And I wait for "5" seconds
    And I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "You haven't given us a valid email"
    And I click on "rl.bttn.ok"
    And I enter "@rlg.com" to "rl.input.email"
    And I click on "rl.bttn.next"
    And I wait for "5" seconds
    And I click on "rl.bttn.next"
    And I enter "Welcome01" to "rl.input.password"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Please create a password that meets"
    And I click on "rl.bttn.ok"
    And I enter "Welcome01!" to "rl.input.password"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    And I click on "rl.bttn.next"
    And I wait for "5" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Please select a security question"
    And I click on "rl.bttn.ok"
    And I wait for "2" seconds
    And I click on "rl.input.secQuestion"
    And I wait for "2" seconds
    And I click on "rl.list.secQ1"
    And I wait for "2" seconds
    And I enter "P" to "rl.input.secAnswer"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Too short!"
    And I click on "rl.bttn.ok"
    And I enter "erfecto answer 1" to "rl.input.secAnswer"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    And I click on "rl.input.secQuestion"
    And I wait for "2" seconds
    And I click on "rl.list.secQ2"
    And I wait for "2" seconds
    And I enter "P" to "rl.input.secAnswer"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Too short!"
    And I click on "rl.bttn.ok"
    And I enter "erfecto answer 2" to "rl.input.secAnswer"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    And I click on "rl.input.secQuestion"
    And I wait for "2" seconds
    And I click on "rl.list.secQ3"
    And I wait for "2" seconds
    And I enter "P" to "rl.input.secAnswer"
    And I click on "rl.bttn.next"
    And I wait for "2" seconds
    When I click on "rl.bttn.next"
    Then I wait "30" seconds to see the text "Too short!"
    And I click on "rl.bttn.ok"
    And I enter "erfecto answer 3" to "rl.input.secAnswer"
    When I uninstall application by name "Royal London"
    And I install app "PUBLIC:App/UAT.ipa"
    And I start application by name "Royal London"
    And I click on "rl.bttn.register"
    And I enter "2628944" to "rl.input.planNo"
    And I enter "DZJRHDXY" to "rl.input.surname"
    Then I click on "rl.bttn.next"
    And I wait for "2" seconds
    And I click on "rl.bttn.next"


  @RL_614627_Droid
  Scenario: PRE_Successful Login_Consumer Android
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I audit accessibility on page "Register or Login"
    And I click on "rl.bttn.login"
    And I audit accessibility on page "Login credentials"
    And I enter "uat33@rlg.com" to "rl.input.email"
    And I enter "Welcome01!" to "rl.input.password"
    And I hide keyboard
    When I click on "rl.bttn.login"
    And I wait for "10" seconds
    And I audit accessibility on page "Plan overview"
    When I click on "rl.bttn.profile"
    And I audit accessibility on page "My Profile"
    Then I click on "rl.bttn.logout"
    And I close application by name "Royal London"
    And I start application by name "Royal London"
    And I wait "5" seconds for "rl.input.password" to appear
    And I enter "Welcome01!" to "rl.input.password"
    And I hide keyboard
    And I click on "rl.bttn.login"

  @RL_614627_Droid_Biometric
  Scenario: PRE_Successful Login_Consumer Android
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I audit accessibility on page "Register or Login"
    And I click on "rl.bttn.login"
    And I audit accessibility on page "Login credentials"
    And I enter "uat33@rlg.com" to "rl.input.email"
    And I enter "Welcome01!" to "rl.input.password"
    And I hide keyboard
    When I click on "rl.bttn.login"
    And I wait for "3" seconds
    And I click on "rl.bttn.turnOnFingerprint"
    And I wait for "10" seconds
    And I audit accessibility on page "Plan overview"
    When I click on "rl.bttn.profile"
    And I audit accessibility on page "My Profile"
    Then I click on "rl.bttn.logout"
    And I close application by name "Royal London"
    And I start application by name "Royal London"
    And I wait "5" seconds for "rl.icon.fingerprint" to appear
    And I validate fingerprint on "Royal London"

  @RL_614671_Droid
  Scenario: Income Release Android
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I audit accessibility on page "Register or Login"
    And I click on "rl.bttn.login"
    And I wait for "3" seconds
    And I audit accessibility on page "Login credentials"
    And I enter "uat33@rlg.com" to "rl.input.email"
    And I enter "Welcome01!" to "rl.input.password"
    And I hide keyboard
    When I click on "rl.bttn.login"
    And I wait for "10" seconds
    And I click firm on "rl.bttn.anotherTime"
    And I wait for "10" seconds
    And I audit accessibility on page "Plan overview"
    And I click on "rl.bttn.plan"
    And I scroll down a lot
    Then I audit accessibility on page "Plan details"
    And I wait "2" seconds for "rl.label.taxFree" to appear
    And I wait "2" seconds for "rl.label.entitled" to appear
    And I wait "2" seconds for "rl.label.yourRegIncome" to appear
    And I wait "2" seconds for "rl.label.taxFreePayments" to appear
    And I wait "2" seconds for "rl.label.taxablePayments" to appear
    And I wait "2" seconds for "rl.label.frequency" to appear
    And I wait "2" seconds for "rl.label.nextPayment" to appear
    And I wait "2" seconds for "rl.label.withFlexible" to appear
    And I click on "rl.bttn.back"
    And I scroll down a lot
    And I scroll down a lot
    And I wait for "3" seconds
    Then I audit accessibility on page "Plan more details"
    And I click on "rl.bttn.exploreFuture"
    And I wait for "3" seconds
    Then I audit accessibility on page "Explore Future"
    When I click on "rl.bttn.readAssumptions"
    And I wait for "3" seconds
    And I audit accessibility on page "Assumptions"
    And I click on "rl.bttn.back"
    And I click on "rl.bttn.useTargetAge"
    And I wait for "3" seconds
    And I audit accessibility on page "Your future outlook"
    When I click on "rl.bttn.ourAssumptions"
    And I wait for "3" seconds
    And I audit accessibility on page "Assumptions"
    And I click on "rl.bttn.back"
    When I click on "rl.bttn.onlineServices"
    And I wait for "3" seconds
    And I audit accessibility on page "Log in - Planholder"
    And I press BACK
    And I click on "rl.bttn.low"
    And I click on "rl.bttn.mid"
    And I click on "rl.bttn.high"
    And I click on "rl.bttn.back"
    And I click on "rl.bttn.showWhenIncomeRunsOut"
    And I wait for "2" seconds
    And I audit accessibility on page "Your future outlook"
    When I click on "rl.bttn.ourAssumptions"
    And I wait for "3" seconds
    And I audit accessibility on page "Assumptions"
    And I click on "rl.bttn.back"
    When I click on "rl.bttn.onlineServices"
    And I wait for "3" seconds
    And I audit accessibility on page "Log in - Planholder"
    And I press BACK
    And I click on "rl.bttn.low"
    And I click on "rl.bttn.mid"
    And I click on "rl.bttn.high"
    And I click on "rl.bttn.back"
    And I click on "rl.bttn.back"

  @RL_614629_Droid
  Scenario: Error_Code_Handling Android
    Given I start application by name "Royal London"
    And I wait for "3" seconds
    And I audit accessibility on page "Register or Login"
    And I click on "rl.bttn.register"
    And I wait for "3" seconds
    When I click on "rl.bttn.next"
    Then I wait "2" seconds for "rl.msg.planNo" to appear
    And I wait "2" seconds for "rl.msg.surname" to appear
    And I audit accessibility on page "Plan number and Surname input"
    And I enter "2628944" to "rl.input.planNo"
    And I enter "DZJRHDXY" to "rl.input.surname"
    And I hide keyboard
    When I click on "rl.bttn.next"
    And I wait for "3" seconds
    When I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.dob" to appear
    And I audit accessibility on page "DOB input"
    And I click on "rl.input.dob"
    And I click on "rl.bttn.ok"
    When I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.dob" to appear
    And I wait for "3" seconds
    And I click on "rl.input.dob"
    And I enter "28" to "rl.dial.day"
    And I click on "rl.dial.month"
    And I click on "rl.dial.month"
    And I click on "rl.dial.month"
    And I click on "rl.dial.month"
    And I wait for "2" seconds
    And I enter "1953" to "rl.dial.year"
    And I wait for "2" seconds
    And I click on "rl.dial.yearConf"
    And I click on "rl.bttn.ok"
    And I click on "rl.bttn.next"
    And I wait for "3" seconds
    When I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.email" to appear
    When I enter a random email on RL
    And I hide keyboard
    And I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.email" to appear
    And I enter "@rlg.com" to "rl.input.email"
    And I hide keyboard
    And I click on "rl.bttn.next"
    And I wait for "3" seconds
    And I click on "rl.bttn.next"
    And I enter "Welcome01" to "rl.input.password"
    And I hide keyboard
    When I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.password" to appear
    And I enter "!" to "rl.input.password"
    And I hide keyboard
    When I click on "rl.bttn.next"
    And I wait for "3" seconds
    When I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.selectQ" to appear
    And I wait "3" seconds for "rl.msg.tooShort" to appear
    And I click on "rl.input.secQuestion"
    And I click on "rl.list.secQ1"
    And I enter "P" to "rl.input.secAnswer"
    And I hide keyboard
    And I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.tooShort" to appear
    And I enter "erfecto answer 1" to "rl.input.secAnswer"
    And I hide keyboard
    And I click on "rl.bttn.next"
    And I click on "rl.input.secQuestion"
    And I click on "rl.list.secQ2"
    And I enter "P" to "rl.input.secAnswer"
    And I hide keyboard
    And I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.tooShort" to appear
    And I enter "erfecto answer 2" to "rl.input.secAnswer"
    And I hide keyboard
    And I click on "rl.bttn.next"
    And I click on "rl.input.secQuestion"
    And I click on "rl.list.secQ3"
    And I enter "P" to "rl.input.secAnswer"
    And I hide keyboard
    And I click on "rl.bttn.next"
    Then I wait "3" seconds for "rl.msg.tooShort" to appear
    And I enter "erfecto answer 3" to "rl.input.secAnswer"
    And I hide keyboard
    When I uninstall application by name "Royal London"
    And I install app "PUBLIC:App/app-uat-release.apk"
    And I start application by name "Royal London"
    And I click on "rl.bttn.register"
    And I enter "2628944" to "rl.input.planNo"
    And I enter "DZJRHDXY" to "rl.input.surname"
    And I hide keyboard
    When I click on "rl.bttn.next"
