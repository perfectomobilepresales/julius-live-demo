@Barclays
Feature: Barclays Demo Scenarios

  @BarclaysMobileApp
  Scenario: Find branch to activate account
    Given I try to close application by name "Barclays" and ignore error
    And I start application by name "Barclays"
    And I reset the device location
    And I wait for "3" seconds
    And I enter Barclays passcode
    And I wait for "2" seconds
    And I click on "barclays.bttn.no"
    And I wait for "2" seconds
    And I swipe up a lot
    When I click on "barclays.bttn.findbranch"
    And I zoom in on map
    Then I set the device location to the address "Manchester Chinatown"
    And I click on "barclays.bttn.locate"
    And I zoom in on map
    Then I set the device location to the address "Edinburgh Castle"
    And I click on "barclays.bttn.locate"
    And I zoom in on map
    Then I set the device location to the address "Cabot Circus, Bristol"
    And I click on "barclays.bttn.locate"
    And I zoom in on map
    Then I want to see the closest branch
    And I click on "barclays.icon.branch"
    And I try to close application by name "Barclays" and ignore error

  @PingItMobileApp
  Scenario: Find branch to activate account
    Given I try to close application by name "Pingit" and ignore error
    And I start application by name "Pingit"
    And I reset the device location
    And I wait for "3" seconds
    And I enter Barclays passcode
    And I wait for "2" seconds
    When I click on "barclays.bttn.findAtm"
    And I wait for "2" seconds
    And I click on "barclays.bttn.findNearestAtm"
    And I zoom in on map
    Then I set the device location to the address "Manchester Chinatown"
    And I click on "barclays.bttn.locate"
    And I zoom in on map
    Then I set the device location to the address "Edinburgh Castle"
    And I click on "barclays.bttn.locate"
    And I zoom in on map
    Then I set the device location to the address "Cabot Circus, Bristol"
    And I click on "barclays.bttn.locate"
    And I zoom in on map
    Then I want to see the closest branch
    And I try to close application by name "Pingit" and ignore error
