@CreditSuisse
Feature: Credit Suisse Demo Scenarios

  @CreditSuisseWeb
  Scenario: UK online account registration
    Given I am on "https://www.credit-suisse.com/uk/en/private-banking.html"
    And I register account as first name "<firstName>" and last name "<lastName>" with account "<accountNo>" plus mobile "<mobile>" and email "<email>"
    Examples:
      | recId | firstName | lastName  | mobile      | email             | accountNo   |
      | 1     | Joe	      | Bloggs    | 0777123456  | jbloggs@email.com | 1234567890  |

  @CreditSuisseWebAndMobile
  Scenario: UK online account registration
    Given I switch to driver "perfectoRemote"
    And I am on "https://www.credit-suisse.com/uk/en/private-banking.html"
    And I switch to driver "perfectodevii"
    And I am on "https://www.credit-suisse.com/uk/en/private-banking.html"

  @CreditSuisseMobileAndMobile
  Scenario: UK online account registration
    Given I switch to driver "perfecto"
    And I switch to driver "perfectodevii"
    And I switch to driver "perfecto"
    And I am on "https://www.credit-suisse.com/uk/en/private-banking.html"
    And I switch to driver "perfectodevii"
    And I am on "https://www.credit-suisse.com/uk/en/private-banking.html"
