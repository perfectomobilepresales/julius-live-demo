@Aircall
Feature: Aircall Demo Scenarios

  @Aircall_CleanUp
  Scenario: Aircall Cleanup
    Given I start application by name "Aircall"
    When I wait "5" seconds for "ac.bttn.signin" to appear
    Then I uninstall application by name "Aircall"

  @Aircall_Setup
  Scenario: Aircall Setup
    Given I start application by name "Aircall"
    And I wait "5" seconds for "ac.bttn.signin" to appear
#    And I enter "juliusm@perfectomobile.com" to "ac.input.email"
#    And I enter "J1mK1ng123" to "ac.input.pass"
    And I enter "jmong@perforce.com" to "ac.input.email"
    And I enter "e00f9941" to "ac.input.pass"
    And I click on "ac.bttn.signin"
    And I wait "5" seconds for "ac.bttn.english" to appear
    And I click on "ac.bttn.english"
    And I click on "ac.bttn.continue"
    And I wait "5" seconds for "ac.bttn.next" to appear
    And I click on "ac.toggle.allowMic"
    And I click on "ac.bttn.allow"
    And I click on "ac.toggle.allowNotifications"
    And I click on "ac.bttn.allowBattery"
    And I click on "ac.toggle.allowContacts"
    And I click on "ac.bttn.allow"
    When I click on "ac.bttn.next"
    Then I wait "5" seconds for "ac.bttn.to-do" to appear

  @Aircall_Login
    Scenario: Aircall Login and navigate
    Given I start application by name "Aircall"
    And I wait "5" seconds for "ac.bttn.signin" to appear
    And I enter "juliusm@perfectomobile.com" to "ac.input.email"
    And I enter "J1mK1ng123" to "ac.input.pass"
    And I click on "ac.bttn.signin"
    And I wait "5" seconds for "ac.bttn.english" to appear
    And I click on "ac.bttn.english"
    And I click on "ac.bttn.continue"
    And I wait "5" seconds for "ac.bttn.next" to appear
    And I click on "ac.toggle.allowMic"
    And I click on "ac.bttn.allow"
    And I click on "ac.toggle.allowNotifications"
    And I click on "ac.bttn.allowBattery"
    And I click on "ac.toggle.allowContacts"
    And I click on "ac.bttn.allow"
    And I click on "ac.bttn.next"
    And I wait "5" seconds for "ac.bttn.to-do" to appear
    And I click on "ac.bttn.to-do"
    And I wait "5" seconds to see the text "To-do"
    And I click on "ac.bttn.history"
    And I click on "ac.bttn.dial"
    And I wait "5" seconds for "ac.input.phoneNo" to appear
    And I enter "+447890123456" to "ac.input.phoneNo"
    And I wait for "2" seconds
    And I click on "ac.bttn.people"
    And I wait "5" seconds for "ac.bttn.contacts" to appear
    And I click on "ac.bttn.teammates"
    And I click on "ac.bttn.contacts"
    When I click on "ac.bttn.settings"
    Then I wait "5" seconds to see the text "Settings"

  @Aircall_Settings
  Scenario: Aircall Check Settings Contents
    Given I start application by name "Aircall"
    And I wait "5" seconds for "ac.bttn.signin" to appear
    And I enter "juliusm@perfectomobile.com" to "ac.input.email"
    And I enter "J1mK1ng123" to "ac.input.pass"
    And I click on "ac.bttn.signin"
    And I wait "5" seconds for "ac.bttn.english" to appear
    And I click on "ac.bttn.english"
    And I click on "ac.bttn.continue"
    And I wait "5" seconds for "ac.bttn.next" to appear
    And I click on "ac.toggle.allowMic"
    And I click on "ac.bttn.allow"
    And I click on "ac.toggle.allowNotifications"
    And I click on "ac.bttn.allowBattery"
    And I click on "ac.toggle.allowContacts"
    And I click on "ac.bttn.allow"
    And I click on "ac.bttn.next"
    And I click on "ac.bttn.settings"
    And I click on "ac.bttn.account"
    And I wait for "1" seconds
    And I wait "5" seconds to see the text "Credentials"
    And I click on "ac.bttn.close"
    And I click on "ac.bttn.settings"
    And I click on "ac.bttn.workingHours"
    And I wait "5" seconds to see the text "Time zone"
    And I click on "ac.bttn.close"
    And I click on "ac.bttn.settings"
    And I click on "ac.bttn.help"
    And I wait "5" seconds to see the text "Help"
    And I click on "ac.bttn.close"
    And I click on "ac.bttn.settings"
    And I click on "ac.bttn.feedback"
    And I wait "5" seconds to see the text "Feedback"
    And I enter "This is a test comment" to "ac.input.comment"
    And I click on "ac.bttn.close"
    When I click on "ac.bttn.settings"
    Then I wait "5" seconds for "ac.bttn.online" to appear
    And I click on "ac.bttn.online"
    And I click on "ac.bttn.auto"
    And I click on "ac.bttn.DND"

  @Aircall_Accessibility
  Scenario: Aircall Accessibility audit
    Given I start application by name "Aircall"
    And I wait "5" seconds for "ac.bttn.signin" to appear
    And I audit accessibility on page "Sign_In"
    And I enter "juliusm@perfectomobile.com" to "ac.input.email"
    And I enter "J1mK1ng123" to "ac.input.pass"
    And I click on "ac.bttn.signin"
    And I wait "5" seconds for "ac.bttn.english" to appear
    And I click on "ac.bttn.english"
    And I click on "ac.bttn.continue"
    And I wait "5" seconds for "ac.bttn.next" to appear
    And I audit accessibility on page "Permissions"
    And I click on "ac.toggle.allowMic"
    And I click on "ac.bttn.allow"
    And I click on "ac.toggle.allowNotifications"
    And I click on "ac.bttn.allowBattery"
    And I click on "ac.toggle.allowContacts"
    And I click on "ac.bttn.allow"
    And I click on "ac.bttn.next"
    And I wait "5" seconds for "ac.bttn.to-do" to appear
    And I audit accessibility on page "Landing_Page"
    And I click on "ac.bttn.to-do"
    And I wait "5" seconds to see the text "To-do"
    And I audit accessibility on page "To-Do"
    And I click on "ac.bttn.history"
    And I wait "5" seconds to see the text "Call history"
    And I audit accessibility on page "Call_History"
    And I click on "ac.bttn.dial"
    And I wait "5" seconds for "ac.input.phoneNo" to appear
    And I audit accessibility on page "Dial_Pad"
    And I enter "+447890123456" to "ac.input.phoneNo"
    And I wait for "2" seconds
    And I click on "ac.bttn.people"
    And I wait "5" seconds for "ac.bttn.contacts" to appear
    And I audit accessibility on page "People"
    And I click on "ac.bttn.teammates"
    And I click on "ac.bttn.contacts"
    When I click on "ac.bttn.settings"
    Then I wait "5" seconds to see the text "Settings"
    And I audit accessibility on page "Settings"

  @Aircall_Add_Contact
  Scenario: Aircall Login and Add Contact
    Given I try to close application by name "Contacts" and ignore error
    And I start application by name "Aircall"
    And I wait "5" seconds for "ac.bttn.signin" to appear
    And I enter "jmong@perforce.com" to "ac.input.email"
    And I enter "e00f9941" to "ac.input.pass"
    And I click on "ac.bttn.signin"
    And I wait "5" seconds for "ac.bttn.english" to appear
    And I click on "ac.bttn.english"
    And I click on "ac.bttn.continue"
    And I wait "5" seconds for "ac.bttn.next" to appear
    And I click on "ac.toggle.allowMic"
    And I click on "ac.bttn.allow"
    And I click on "ac.toggle.allowNotifications"
    And I click on "ac.bttn.allowBattery"
    And I click on "ac.toggle.allowContacts"
    And I click on "ac.bttn.allow"
    And I click on "ac.bttn.next"
    And I start application by name "Contacts"
    And I click on "droid.bttn.addContact"
    And I enter "John Doe" to "droid.input.contactName"
    And I click on "droid.input.contactPhone"
    And I enter "+441234567890" to "droid.input.contactPhone"
    When I click on "droid.bttn.contactSave"
    And I start application by name "Aircall"
    And I click on "ac.bttn.people"
    And I wait "5" seconds for "ac.bttn.contacts" to appear
    And I click on "ac.bttn.contacts"
    And I enter "Doe" to "ac.input.search"
    Then I validate contact "John Doe" is found
    And I click on "ac.bttn.add"
    And I enter "Jane" to "ac.input.first"
    And I enter "Doe" to "ac.input.last"
    And I enter "Perfecto" to "ac.input.company"
    And I enter "+33176360888" to "ac.input.mainNo"
    And I click on "ac.bttn.save"
    And I click on "ac.bttn.merge" and ignore errors
    When I click on "ac.bttn.back"
    Then I validate contact "Jane Doe" is found
    And I try to close application by name "Contacts" and ignore error
    And I start application by name "Contacts"
    And I click on "droid.bttn.contactOptions"
    And I click on "droid.bttn.delContact" and ignore errors
    And I click on "droid.bttn.selAllContacts" and ignore errors
    And I click on "droid.bttn.delAllContacts" and ignore errors
    And I click on "droid.bttn.delAllContacts" and ignore errors

  @Aircall_Call_Inbound
  Scenario: Aircall Inbound Call
    Given I switch to driver "perfectodeviiRemote"
    And I go to the device home screen
    And I switch to driver "perfectoRemote"
    And I try to close application by name "Aircall" and ignore error
    And I start application by name "Aircall"
    And I wait "5" seconds for "ac.bttn.to-do" to appear
    And I click on "ac.bttn.settings"
    And I wait "5" seconds for "ac.bttn.online" to appear
    And I click on "ac.bttn.online"
    And I click on "ac.bttn.close"
    And I switch to driver "perfectodeviiRemote"
    And I make audio call "+3238081013"
    And I wait for "5" seconds
    And I inject an audio from "GROUP:Julius/Audio/ENG_F.wav" into the device
    And I switch to driver "perfectoRemote"
    And I click on "ac.bttn.answer"
    And I turn volume up
    And I start audio recording
    And I wait for "15" seconds
    When I stop audio recording
    And I click on "ac.bttn.hangup"
    And I switch to driver "perfectodeviiRemote"
    And I hang up the phone
    And I switch to driver "perfectoRemote"
    Then I validate audio "GROUP:Julius/Audio/ENG_F.wav"
    And I click on "ac.bttn.settings"
    And I wait "5" seconds for "ac.bttn.online" to appear
    And I click on "ac.bttn.auto"
    And I click on "ac.bttn.close"

  @Aircall_Call_Outbound
  Scenario: Aircall Outbound Call
    Given I switch to driver "perfectodeviiRemote"
    And I go to the device home screen
    And I get my phone number
    And I switch to driver "perfectoRemote"
    And I try to close application by name "Aircall" and ignore error
    And I start application by name "Aircall"
    And I click on "ac.bttn.dial"
    And I wait "5" seconds for "ac.input.phoneNo" to appear
    And I call the other phone
    And I click on "ac.bttn.call"
    And I switch to driver "perfectodeviiRemote"
    And I wait "30" seconds for "phone.answer" to appear
    And I pick up the phone
    And I turn volume up
    And I switch to driver "perfectoRemote"
    And I inject an audio from "GROUP:Julius/Audio/ENG_F.wav" into the device
    And I switch to driver "perfectodeviiRemote"
    And I start audio recording
    And I wait for "15" seconds
    When I stop audio recording
    And I hang up the phone
    And I switch to driver "perfectoRemote"
    And I hang up the phone
    And I switch to driver "perfectodeviiRemote"
    Then I validate audio "GROUP:Julius/Audio/ENG_F.wav"
