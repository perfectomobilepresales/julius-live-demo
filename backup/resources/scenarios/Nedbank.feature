@Nedbank
Feature: Nedbank Demo Scenarios

  @NedbankMobileiOS
  Scenario: AVO app on iOS
    Given I start device logging
    And I start application by name "Avo by Nedbank"
    And I wait for "10" seconds
    When I click on "ios.bttn.allow" and ignore errors
    And I stop device logging
    Then I uninstall app "Avo by Nedbank"

  @NedbankMobileDroid
  Scenario: AVO app on Android
    Given I start device logging
    And I start application by name "Avo Africa"
    And I wait for "10" seconds
    When I click on "droid.bttn.allow" and ignore errors
    And I stop device logging
    Then I uninstall app "Avo Africa"