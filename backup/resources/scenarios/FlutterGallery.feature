@Flutter
  Feature: Flutter Gallery demo

    @FlutteriOS
    Scenario: Test Flutter Gallery app
      Given I try to close application by ID "julius.flutter.examples.gallery"
      And I start application by ID "julius.flutter.examples.gallery"
      And I visually press on "Cupertino"
      And I visually press on "Buttons"
      And I visually press on "With Background"
      And I visually press on "With Background"
      And I visually press on "With Background"
      And I visually press on button "PRIVATE:BMW/WhiteBack.png"
      And I visually press on button "PRIVATE:BMW/BlueBack.png"
      And I visually press on "Style"
      And I visually press on "Colors"
      And I swipe left a lot
      And I swipe left a lot
      And I swipe left a lot
      And I swipe left a lot
      And I visually press on button "PRIVATE:BMW/BlueBack.png"
      And I visually press on button "PRIVATE:BMW/BlueBack.png"
      And I visually press on "Studies"
      And I visually press on "Shrine"
      And I visually enter "test123" on "Password"
      And I visually enter "perfectodemouk" on "Username"
      And I hit "done"
      And I visually press on "NEXT"
      And I swipe left a lot
      And I swipe left a lot
      And I swipe left a lot
      And I swipe to go back
      And I try to close application by ID "julius.flutter.examples.gallery"

    @FlutterDroid
    Scenario: Test Flutter Gallery app
      Given I try to close application by ID "io.flutter.demo.gallery"
      And I start application by ID "io.flutter.demo.gallery"
      And I click on "flutter.cupertino"
      And I click on "flutter.buttons"
      And I click on "flutter.bttn.withBG"
      And I click on "flutter.bttn.withBG"
      And I click on "flutter.bttn.withBG"
      And I press on back button
      And I press on back button
      And I click on "flutter.style"
      And I click on "flutter.colors"
      And I swipe left a lot
      And I swipe left a lot
      And I swipe left a lot
      And I swipe left a lot
      And I press on back button
      And I press on back button
      And I click on "flutter.studies"
      And I click on "flutter.shrine"
      And I click on "flutter.bttn.user"
      And I type "perfectodemouk" in element "flutter.input.user"
      And I click on "flutter.bttn.pass"
      And I type "test123" in element "flutter.input.pass"
      And I hide keyboard
      And I click on "flutter.bttn.next"
      And I swipe left a lot
      And I swipe left a lot
      And I swipe left a lot
      And I press on back button
      And I try to close application by ID "io.flutter.demo.gallery"
