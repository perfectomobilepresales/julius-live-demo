@VHA
Feature: NHS Demo

  @Vodafone_Web
  Scenario Outline: Search for keywords on portal
    Given I am on "https://www.vodafone.com.au/"
    And I search for "<keyword>" on Vodafone portal
    Examples:
      | recId | keyword |
      | 1     | 5g      |
