@Santander
Feature: Santander Demo Scenarios

  @CyclesiOS
    Scenario: Start Cycles iOS app and cycle through locations
    Given I try to close application by name "Cycle Hire" and ignore error
    And I start device logging
    And I start device vitals
    And I set the device location to the address "Paddington, London, UK"
    And I start application by name "Cycle Hire"
    And I wait for "3" seconds
    And I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    And I close application by name "Cycle Hire"
    And I set the device location to the address "Baker Street, London, UK"
    And I start application by name "Cycle Hire"
    And I wait for "3" seconds
    And I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    And I close application by name "Cycle Hire"
    And I set the device location to the address "Covent Garden, London, UK"
    And I start application by name "Cycle Hire"
    And I wait for "3" seconds
    And I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    And I close application by name "Cycle Hire"
    And I set the device location to the address "Barbican, London, UK"
    And I start application by name "Cycle Hire"
    And I wait for "3" seconds
    When I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    Then I stop device logging
    And I stop device vital
    And I close application by name "Cycle Hire"

  @CyclesDroid
  Scenario: Start Cycles Android app and cycle through locations
    Given I try to close application by name "Cycle Hire" and ignore error
    And I start device logging
    And I start device vitals
    And I start application by name "Cycle Hire"
    And I set the device location to the address "Paddington, London, UK"
    And I click on "santander.bttn.locate"
    And I wait for "2" seconds
    And I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    And I set the device location to the address "Baker Street, London, UK"
    And I click on "santander.bttn.locate"
    And I wait for "2" seconds
    And I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    And I set the device location to the address "Covent Garden, London, UK"
    And I click on "santander.bttn.locate"
    And I wait for "2" seconds
    And I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    And I set the device location to the address "Barbican, London, UK"
    And I click on "santander.bttn.locate"
    And I wait for "2" seconds
    When I click on "santander.bttn.cycle"
    And I wait for "2" seconds
    Then I stop device logging
    And I stop device vital
    And I close application by name "Cycle Hire"
