@BCV
Feature: BCV Demo Feature

  @BCVScaniOS
  Scenario: Scan Payment
    Given I try to close application by name "BCV Mobile" and ignore error
    And I start application by name "BCV Mobile"
    And I click on "bcv.login"
    And I click on "bcv.password"
    And I enter "BCVPerfecto2020" to "bcv.password"
    And I click on "bcv.next"
    And I wait "5" seconds for "bcv.box" to appear
    And I click on "bcv.box"
    And I enter "0000" to "bcv.box"
    And I click on "bcv.okay"
    And I wait "10" seconds for "bcv.scan" to appear
    And I start inject "PRIVATE:QRCode2.jpg" image to application name "BCV Mobile"
    When I click on "bcv.scan"
    And I click on "bcv.continue"
    And I click on "bcv.confirm"
    And I click on "bcv.home"

  @BCVCheckBalanceiOS
  Scenario: Check Balance
    Given I try to close application by name "BCV Mobile" and ignore error
    And I start application by name "BCV Mobile"
    And I click on "bcv.login"
    And I enter "Perfecto2020" to "bcv.password"
    And I click on "bcv.next"
    And I enter "0000" to "bcv.box"
    And I click on "bcv.okay"
    And I wait "30" seconds for "bcv.scan" to appear
    When I click on "bcv.accounts"
    Then I wait "5" seconds to see the text "HA010610"
    And I click on "bcv.accountLI"
    And I wait "5" seconds to see the text "X-PERMANENT B.-N."
    And I click on "bcv.transactionLI"
    And I wait "5" seconds to see the text "CREDIT SUISSE"
    And I wait "5" seconds to see the text "70,000.00"


  @BCVScanDroid
  Scenario: Scan Payment
    Given I switch to driver "perfectoRemote"
    And I try to close application by name "BCV Mobile HA" and ignore error
    And I start application by name "BCV Mobile HA"
    And I switch to driver "perfectodeviiRemote"
    And I start application by name "Gallery"
    And I switch to driver "perfectoRemote"
    And I click on "bcv.login"
    And I enter "Perfecto2020" to "bcv.password"
    And I click on "bcv.next"
    And I enter "0000" to "bcv.box"
    And I click on "bcv.okay"
    And I wait "10" seconds for "bcv.scan" to appear
    When I click on "bcv.scan"

  @BCVCheckBalanceDroid
  Scenario: Check Balance
    Given I try to close application by name "BCV Mobile HA" and ignore error
    And I start application by name "BCV Mobile HA"
    And I click on "bcv.login"
    And I enter "Perfecto2020" to "bcv.password"
    And I click on "bcv.next"
    And I enter "0000" to "bcv.box"
    And I click on "bcv.okay"
    And I wait "30" seconds for "bcv.scan" to appear
    When I click on "bcv.accounts"
    Then I wait "5" seconds to see the text "HA010610"
    And I click on "bcv.accountLI"
    And I wait "5" seconds to see the text "X-PERMANENT B.-N."
    And I click on "bcv.transactionLI"
    And I wait "5" seconds to see the text "ORDRE PMT"
    And I wait "5" seconds to see the text "-284.22"
