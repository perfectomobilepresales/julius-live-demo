@Dynamics365
Feature: Dynamics365 Demo Scenarios

  @Dynamics365AddContactAndroid
    Scenario: Start CRM Dyanmics and check non-production apps
    Given I try to close application by name "Dynamics 365 for phones" and ignore error
    And I start application by name "Dynamics 365 for phones"
    And I press BACK
    And I click on "dyn.apps"
    And I click on "dyn.nonProdApps"
    And I click on "dyn.app1"
    And I click on "dyn.newRecord"
    And I click on "dyn.contact"
    And I enter "Julius" to "dyn.firstName"
    And I enter "Mong" to "dyn.lastName"
    And I enter "SE" to "dyn.jobTitle"
    And I enter "Perfecto" to "dyn.accountName"
    And I click on "dyn.cancel"
    And I enter "juliusm@perfectomobile.com" to "dyn.email"
    And I enter "07771054911" to "dyn.mobile"
    And I enter "Added Feb 2021" to "dyn.desc"
    And I swipe up a lot
    And I hide keyboard
    And I enter "123 Street" to "dyn.street1"
    And I hide keyboard
    And I enter "NYC" to "dyn.street2"
    And I close application by name "Dynamics 365 for phones"

  @Dynamics365AddContactiOS
  Scenario: Start CRM Dyanmics and check non-production apps
    Given I try to close application by name "Dynamics 365" and ignore error
    And I start application by name "Dynamics 365"
    And I click on "dyn.menu"
    And I click on "dyn.toApps"
    And I click on "dyn.apps"
    And I click on "dyn.nonProdApps"
    And I click on "dyn.app1"
    And I click on "dyn.newRecord"
    And I click on "dyn.contact"
    And I click on "dyn.firstName"
    And I enter "Julius" to "dyn.firstName"
    And I enter "Mong" to "dyn.lastName"
    And I enter "SE" to "dyn.jobTitle"
    And I click on "dyn.accountName"
    And I enter "Perfecto" to "dyn.accountName"
    And I click on "dyn.cancel"
    And I enter "juliusm@perfectomobile.com" to "dyn.email"
    And I enter "07771054911" to "dyn.mobile"
    And I enter "Added Feb 2021" to "dyn.desc"
    And I swipe up a bit
    And I enter "123 Street" to "dyn.street1"
    And I enter "NYC" to "dyn.street2"
    And I close application by name "Dynamics 365"
