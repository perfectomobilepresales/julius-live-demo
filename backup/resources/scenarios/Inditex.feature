@Inditex
Feature: Inditex Demo Scenarios

  @ZaraHome
    Scenario: Zara Home Product QR Scans
    Given I click on "zara.selectStore"
    And I enter "United" to "zara.market"
    And I click on "zara.uk"
    And I click on "zara.acceptCookies"
    And I click on "zara.optionAccept"
    And I click on "zara.search"
    And I click on "zara.scan"
    And I click on "droid.bttn.allowWhileUsingApp"
    And I inject "qr1.png" image to application name "Zara Home"
    And I inject "qr2.png" image to application name "Zara Home"
    And I inject "qr3.png" image to application name "Zara Home"
    And I inject "qr4.png" image to application name "Zara Home"
