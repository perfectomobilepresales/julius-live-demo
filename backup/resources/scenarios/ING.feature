@ING
Feature: ING Demo Scenarios

  @INGLogin
  Scenario: Open App and login
    Given I try to close application by name "ING Banking" and ignore error
    And I start application by name "ING Banking"
    And I click on "ing.login"
    Then I validate ING logins:
      | accountNo  | pin   |
      | 1234567890 | 12345 |
      | 1234567890 | 54321 |
      | 1234567890 | 55885 |
      | 1234567890 | 78955 |
      | 1234567890 | 44562 |
    When I click on "ing.cancel"
    And I click on "ing.yesCancel"
    Then I wait "5" seconds to see the text "Willkommen"

  @INGRegister
  Scenario: Open App and register
    Given I try to close application by name "ING Banking" and ignore error
    And I start application by name "ING Banking"
#    And I click on "ing.register" and ignore errors
#    And I click on "ing.next" and ignore errors
    And I click on "ing.2"
    And I click on "ing.5"
    And I click on "ing.8"
    And I click on "ing.0"
    And I click on "ing.2"
    And I click on "ing.male"
    And I click on "ing.dr"
    And I enter "Perfecto ING" to "ing.firstName"
    And I enter "Demo" to "ing.lastName"
    And I enter "Perfecto" to "ing.birthName"
    And I click on "ing.next"
    And I enter "demo@perfecto.io" to "ing.email"
    And I click on "ing.next"
    And I click on "ing.next"
    And I enter "21" to "ing.day"
    And I enter "12" to "ing.month"
    And I enter "1976" to "ing.year"
    When I click on "ing.next"
    And I click on "ing.next"
    Then I should see text "Deutschland"

  @INGUninstall
  Scenario: Uninstall app
    Given I uninstall application by name "ING Banking"