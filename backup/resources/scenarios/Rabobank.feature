@Rabobank
Feature: Rabobank Demo Scenarios

  @RabobankWeb
  Scenario: Open website and login
    Given I am on "https://www.rabobank.nl/particulieren/"
    And I maximize window
    And I click on "rb.akkoord"
    And I click on "rb.betalen"
    And I click on "rb.omzetten"
    And I click on "rb.mijn"
    And I enter "123456789" to "rb.accNo"
    And I enter "4321" to "rb.cardNo"
    When I enter "87654321" to "rb.loginCode"
    Then I click on "rb.login"
    And I start device vital monitoring every ".*" seconds
    And I start network virtualization with HAR capture

  @RabobankMobile
    Scenario: Open app and login
    Given I try to close application by name "Rabobank" and ignore error
    And I start application by name "Rabobank"
    And I click on "rb.register"
    When I click on "rb.yes"
    And I click on "rb.scan"
    Then I wait for "5" seconds
    And I should see text "Scan de QR-code"
    And I close application by name "Rabobank"
