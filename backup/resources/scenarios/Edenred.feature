@Edenred
Feature: Edenred Web Demo Scenarios

  @EdenredMobileiOS
  Scenario: Navigate Edenren app and try to register
    Given I try to close application by name "MyEdenred" and ignore error
    And I start application by name "MyEdenred"
    And I click on "eden.bttn.register"
    And I enter "Perfecto" to "eden.txt.first"
    And I enter "Demo" to "eden.txt.last"
    And I enter "perfectodemouk@gmail.com" to "eden.txt.email"
    And I enter "02225551234" to "eden.txt.phone"
    And I enter "PASSWORD123!" to "eden.txt.password"
    And I enter "PASSWORD123!" to "eden.txt.rpassword"
    And I scroll down a bit
    When I press on "eden.bttn.signup"
    And I wait for "2" seconds
    And I close application by name "MyEdenred"

  @EdenredMobileDroid
  Scenario: Navigate Edenren app and try to register
    Given I try to close application by name "Ticket" and ignore error
    And I start application by name "Ticket"
    And I click on "eden.bttn.register"
    And I enter "Perfecto" to "eden.txt.first"
    And I enter "Demo" to "eden.txt.last"
    And I enter "perfectodemouk@gmail.com" to "eden.txt.email"
    And I enter "02225551234" to "eden.txt.phone"
    And I enter "PASSWORD123!" to "eden.txt.password"
    And I enter "PASSWORD123!" to "eden.txt.rpassword"
    And I scroll down a bit
    When I press on "eden.bttn.signup"
    And I wait for "2" seconds
    And I close application by name "Ticket"

  @EdenredDesktop
  Scenario: Navigate Edenren website and search for phrase
    Given I am on "https://www.Edenred.com.tr"
    And I maximize window
    And I wait for "2" seconds
    And I accept Edenred cookies
    When I click thru on Edenred
    And I wait for "2" seconds
    Then I find "Ticket Restaurant" on Edenred
