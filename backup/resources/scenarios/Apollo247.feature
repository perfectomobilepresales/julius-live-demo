@Apollo247
Feature: Expense Tracker Demo Scenarios

  @AskApollo
    Scenario: Book appointment
    Given I start application by name "Ask Apollo"
    And I click on "apollo.book"
    And I click on "apollo.proceed"
    And I enter "Jim" to "apollo.firstName"
    And I enter "King" to "apollo.lastName"
    And I enter "test@perfecto.io" to "apollo.email"
    And I enter "12345678" to "apollo.mobile"
    And I click on "apollo.healthConcern"
    And I click on "apollo.done"
    And I click on "apollo.done"
    And I enter "Test message here" to "apollo.message"
    And I click on "apollo.submit"
    And I audit accessibility on page ".*"