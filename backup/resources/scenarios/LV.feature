@LV
Feature: LV Demo Scenarios

  @LV_UI_Check
  Scenario: LV UI Check
    Given I try to close application by name "Louis Vuitton" and ignore error
    And I start application by name "Louis Vuitton"
    And I wait for "2" seconds
    And I click on "lv.shop"
    And I click on "lv.men"
    And I click on "lv.mensShop"
    And I click on "lv.giftGuide"
    And I swipe up a lot
    And I click on "lv.magazine"
    And I swipe up a lot
    And I click on "lv.home"
    When I click on "lv.springSummer"
    Then I wait "10" seconds to see the text "PREVIEW"
