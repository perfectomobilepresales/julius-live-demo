@Nestle
Feature: PetFinder Demo Scenarios

  @PetFinderBurlington
  Scenario: Install app fresh
    Given I try to close application by name "Petfinder" and ignore error
    And I uninstall app "Petfinder"
    And I wait for "5" seconds
    And I install app "PRIVATE:Nestle/Petfinder_com.discovery.petfinder.apk"
    And I set the device location to the address "Burlington, Massachusetts"
    And I start application by name "Petfinder"
    And I click on "nestle.bttn.continue"
    And I click on "nestle.bttn.allowGPS"
    And I click on "droid.bttn.allow"
    And I click on "nestle.bttn.notnow"
    And I wait for "2" seconds
    And I scroll down a lot
    And I start inject "PRIVATE:Nestle/Dog.jpeg" image to application name "Petfinder"
    And I click on "nestle.link.dogsPhoto"
    And I click on "nestle.link.takePhoto"
    And I click on "droid.bttn.allow"
    And I click on "droid.bttn.allow"
    And I scroll down a bit
    And I wait for "1" seconds
    When I click on "nestle.bttn.snapPhoto"
    And I click on "nestle.bttn.ages"
    And I click on "nestle.bttn.adult"
    And I click on "nestle.bttn.apply"
    Then I should see text "Bichon Frise"
    And I close application by name "Petfinder"

  @PetFinderChicago
  Scenario: Set location to Chicago
    Given I try to close application by name "Petfinder" and ignore error
    And I uninstall app "Petfinder"
    And I wait for "5" seconds
    And I install app "PRIVATE:Nestle/Petfinder_com.discovery.petfinder.apk"
    And I set the device location to the address "Chicago, Illinois"
    And I start application by name "Petfinder"
    And I click on "nestle.bttn.continue"
    And I click on "nestle.bttn.allowGPS"
    And I click on "droid.bttn.allow"
    And I click on "nestle.bttn.notnow"
    And I wait for "2" seconds
    And I scroll down a lot
    And I start inject "PRIVATE:Nestle/Dog.jpeg" image to application name "Petfinder"
    And I click on "nestle.link.dogsPhoto"
    And I click on "nestle.link.takePhoto"
    And I click on "droid.bttn.allow"
    And I click on "droid.bttn.allow"
    And I scroll down a bit
    And I wait for "1" seconds
    When I click on "nestle.bttn.snapPhoto"
    And I click on "nestle.bttn.ages"
    And I click on "nestle.bttn.adult"
    And I click on "nestle.bttn.apply"
    Then I should see text "Bichon Frise"
    And I close application by name "Petfinder"

  @PetFinderSanJose
  Scenario: Set location to San Jose
    Given I try to close application by name "Petfinder" and ignore error
    And I uninstall app "Petfinder"
    And I wait for "5" seconds
    And I install app "PRIVATE:Nestle/Petfinder_com.discovery.petfinder.apk"
    And I set the device location to the address "San Jose, California"
    And I start application by name "Petfinder"
    And I click on "nestle.bttn.continue"
    And I click on "nestle.bttn.allowGPS"
    And I click on "droid.bttn.allow"
    And I click on "nestle.bttn.notnow"
    And I wait for "2" seconds
    And I scroll down a lot
    And I start inject "PRIVATE:Nestle/Dog.jpeg" image to application name "Petfinder"
    And I click on "nestle.link.dogsPhoto"
    And I click on "nestle.link.takePhoto"
    And I click on "droid.bttn.allow"
    And I click on "droid.bttn.allow"
    And I scroll down a bit
    And I wait for "1" seconds
    When I click on "nestle.bttn.snapPhoto"
    And I click on "nestle.bttn.ages"
    And I click on "nestle.bttn.adult"
    And I click on "nestle.bttn.apply"
    Then I should see text "Bichon Frise"
    And I close application by name "Petfinder"
