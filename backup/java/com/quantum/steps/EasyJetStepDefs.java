package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Then;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@QAFTestStepProvider
public class EasyJetStepDefs
{
    QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
    JavascriptExecutor js = DeviceUtils.getQAFDriver();

    @Then("^I log in to easyJet$")
    public void ILogInToEasyJet() {
        Capabilities caps = DeviceUtils.getQAFDriver().getCapabilities();
        Set<String> capNames = caps.getCapabilityNames();
        String url = "https://www.easyjet.com/en";

        if ((DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS))) {
            if (capNames.contains("bundleId") && caps.getCapability("bundleId").toString().toUpperCase().contains("CHROME")) {
                QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
//                Thread.sleep(20000);
                driver.waitForAllElementVisible();
                driver.findElementByXPath("//*[@name=\"NTPHomeFakeOmniboxAccessibilityID\"]").click();
                driver.getKeyboard().sendKeys(url);
                driver.findElementByXPath("//*[@label=\"go\"]").click();
            } else {
                Map<String, Object> params = new HashMap<>();
                params.put("url", url);
                Object result1 = DeviceUtils.getQAFDriver().executeScript("mobile:browser:goto", params);
            }
        } else if ((DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.ANDROID)))
        {
            if (capNames.contains("appPackage") && caps.getCapability("appPackage").toString().toUpperCase().contains("SBROWSER"))
            {
                QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
                try { driver.findElementByXPath("//*[@resource-id=\"android:id/button1\"]").click(); } catch (Exception ignored) {;}
                try { driver.findElementByXPath("//*[@resource-id=\"com.sec.android.app.sbrowser:id/help_intro_legal_agree_button\"]").click(); } catch (Exception ignored) {;}
                driver.findElementByXPath("//*[@resource-id=\"com.sec.android.app.sbrowser:id/url_bar_container\"]").click();
                driver.findElementByXPath("//*[@resource-id=\"com.sec.android.app.sbrowser:id/url_bar_container\"]").clear();
                driver.getKeyboard().sendKeys(url);

                Map<String, Object> EnterKeyEvent  = new HashMap<>();
                EnterKeyEvent.put("key", "66");
                driver.executeScript("mobile:key:event", EnterKeyEvent);
            }
            else DeviceUtils.getQAFDriver().get(url);
        } else {
            DeviceUtils.getQAFDriver().get(url);
            if (caps.getCapability("platformName").toString().toUpperCase().contains("WINDOWS") || caps.getCapability("platformName").toString().toUpperCase().contains("MAC") )
                DeviceUtils.getQAFDriver().manage().window().maximize();
        }

        // if iOS Chrome
        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS) && capNames.contains("bundleId") && caps.getCapability("bundleId").toString().toUpperCase().contains("CHROME"))
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { driver.findElementByXPath("//*[@label=\"Choose what you share with us, web dialog\"]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]").click(); } catch (Exception ignored) {;}
            try { Thread.sleep(2000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Go to log in page");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}

            // close tab
            driver.findElementByXPath("//*[@label=\"Show Tabs\"]").click();
            driver.findElementByXPath("//*[@name=\"GridCellCloseButtonIdentifier\"]").click();
            driver.findElementByXPath("//*[@label=\"Create new tab.\"]").click();
        }
        // if Samsung Internet
        else if (capNames.contains("appPackage") && caps.getCapability("appPackage").toString().toUpperCase().contains("SBROWSER"))
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { samsungClick("Accept and continue"); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Go to log in page");
            driver.waitForAllElementVisible();
            samsungClick("Menu");

            // close tab
            DeviceUtils.closeApp(caps.getCapability("appPackage").toString(),"identifier");
            DeviceUtils.startApp(caps.getCapability("appPackage").toString(),"identifier");
        }
        else
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            try { driver.findElementByCssSelector("#ensCloseBanner").click();} catch (Exception ignored) {;}

            ReportUtils.logStepStart("Go to log in page");
            driver.waitForAllElementVisible();
//            if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS) || DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.ANDROID) ) driver.findElementByCssSelector("body > section > div.m-masthead-nav__main-wrapper.js-masthead > div.m-masthead-nav__upper-wrapper > div > div > a").click();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            jsClickIfMac("#pageWrapper > header > div.main-header > div.inner-main-header > nav.main-navigation.additional-links > ul > li:nth-child(2) > span > a.navigation-sign-in-button");
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            driver.findElementByCssSelector("#signin-username").sendKeys("jccmong@gmail.com");
            driver.findElementByCssSelector("#signin-password").sendKeys("");
            jsClickIfMac("#RegisteredUserSignIn > form > div.ej-checkbox.ej-checkbox-multiline-text.ej-text.ej-text-dark.checked > label");
            jsClickIfMac("#signin-login");
        }
    }

    private void jsClickIfMac(String cssSelector)
    {
        String browserName =driver.getCapabilities().getCapability("browserName").toString();
//        if (browserName.toUpperCase().contains("SAFARI")) {
            js.executeScript("arguments[0].click();", driver.findElementByCssSelector(cssSelector));
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
//        } else {
//            driver.findElementByCssSelector(cssSelector).click();
//        }
    }

    private void samsungClick(String label)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungClick(String label, String direction, String offset)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("words", "words");
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        params.put("label.direction", direction);
        params.put("label.offset", offset);
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungFind(String label) {
        Map<String, Object> params = new HashMap<>();
        params.put("content", label);
        driver.executeScript("mobile:text:find", params);
    }
}
