package com.quantum.steps;

import com.perfecto.reportium.client.ReportiumClient;
import com.perfecto.reportium.testng.ReportiumTestNgListener;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.*;
import java.util.concurrent.TimeUnit;

@QAFTestStepProvider
public class CreditSuisseStepDefs
{
    QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
    JavascriptExecutor js = DeviceUtils.getQAFDriver();

    @Then("^I register account as first name \"([^\"]*)\" and last name \"([^\"]*)\" with account \"([^\"]*)\" plus mobile \"([^\"]*)\" and email \"([^\"]*)\"$")
    public void IRegisterCreditSuisseAccount(String firstName, String lastName, String accountNo, String mobile, String email) {
        Capabilities caps = DeviceUtils.getQAFDriver().getCapabilities();
        Set<String> capNames = caps.getCapabilityNames();

        // if iOS Chrome
        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS) && capNames.contains("bundleId") && caps.getCapability("bundleId").toString().toUpperCase().contains("CHROME"))
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { driver.findElementByXPath("//*[@label=\"Choose what you share with us, web dialog\"]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]").click(); } catch (Exception ignored) {;}
            try { Thread.sleep(2000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Go to log in page");
            driver.waitForAllElementVisible();
            driver.findElementByXPath("//*[@label=\"Private banking - Swiss private bank | Credit Suisse United Kingdom\"]/XCUIElementTypeOther[1]/XCUIElementTypeImage[1]").click();
            driver.findElementByXPath("//XCUIElementTypeLink[@label=\"Login\"]").click();
            driver.findElementByXPath("//*[@label=\"Private banking - Swiss private bank | Credit Suisse United Kingdom\"]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[3]/XCUIElementTypeLink[1]").click();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Click Register");
            driver.waitForAllElementVisible();
            driver.findElementByXPath("//*[@label=\"Register as a new user\"]").click();
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            CommonSteps.ISwipeUpALot();
            driver.findElementByXPath("//XCUIElementTypeOther[@label=\"Credit Suisse (UK) Limited and/or Credit Suisse AG, Guernsey Branch\"]").click();
            driver.findElementByXPath("//*[@label=\"Next\"]").click();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Enter Name");
            driver.waitForAllElementVisible();
            driver.findElementByXPath("//*[@label=\"2 User Information, tab panel\"]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]").sendKeys(firstName);
            driver.findElementByXPath("//*[@label=\"2 User Information, tab panel\"]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]").sendKeys(lastName);
            driver.findElementByXPath("//*[@value=\"Account number\"]").sendKeys(accountNo);
            driver.findElementByXPath("//*[@value=\"Yes\"]").click();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Enter Contact Info");
            driver.waitForAllElementVisible();

            // close tab
            driver.findElementByXPath("//*[@label=\"Show Tabs\"]").click();
            driver.findElementByXPath("//*[@name=\"GridCellCloseButtonIdentifier\"]").click();
            driver.findElementByXPath("//*[@label=\"Create new tab.\"]").click();
        }
        // if Samsung Internet
        else if (capNames.contains("appPackage") && caps.getCapability("appPackage").toString().toUpperCase().contains("SBROWSER"))
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { samsungClick("Accept and continue"); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Go to log in page");
            driver.waitForAllElementVisible();
            samsungClick("Menu");
            samsungClick("EN", "right", "80%");
            samsungClick("Digital");

            ReportUtils.logStepStart("Click Register");
            driver.waitForAllElementVisible();
            samsungClick("Register");
            CommonSteps.ISwipeUpALot();
            CommonSteps.ISwipeUpALot();
            CommonSteps.ISwipeUpALot();
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            samsungClick("Credit Suisse (UK)");
            samsungClick("Next");

            ReportUtils.logStepStart("Enter Name");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            samsungClick("First name*", "Above", "5%");
            CommonSteps.ISendKeys(firstName);
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            samsungClick("Surname*", "Above", "5%");
            CommonSteps.ISendKeys(lastName);
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            samsungClick("Indicate the 5 to 6 digit Bank Account", "Above", "8%");
            CommonSteps.ISendKeys(accountNo);
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            samsungClick("Yes");
            CommonSteps.ISwipeUpALot();

            ReportUtils.logStepStart("Enter Contact Info");
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            driver.waitForAllElementVisible();

            // close tab
            DeviceUtils.closeApp(caps.getCapability("appPackage").toString(),"identifier");
            DeviceUtils.startApp(caps.getCapability("appPackage").toString(),"identifier");
        }
        else
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            try { driver.findElementByCssSelector("#onetrust-accept-btn-handler").click();} catch (Exception ignored) {;}

            ReportUtils.logStepStart("Go to log in page");
            driver.waitForAllElementVisible();
            if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS) || DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.ANDROID) ) driver.findElementByCssSelector("body > section > div.m-masthead-nav__main-wrapper.js-masthead > div.m-masthead-nav__upper-wrapper > div > div > a").click();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            jsClickIfMac("body > section > div.m-masthead-nav__main-wrapper.js-masthead > div.m-masthead-nav__upper-wrapper > div > ul > li:nth-child(1) > a > span.m-functional-nav__item-title");
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            jsClickIfMac("body > section > div.m-masthead-nav__main-wrapper.js-masthead > div.m-masthead-nav__functional-flyout.js-functional-flyout.m-masthead-nav__functional-nav-mobile--on > div.m-masthead-nav__functional-flyout-wrapper.js-functional-flyout-wrapper > div.m-masthead-nav__functional-flyout-login > div.m-masthead-nav__functional-flyout-login-wrapper > div > div:nth-child(1) > li > a");

            ReportUtils.logStepStart("Click Register");
            driver.waitForAllElementVisible();
            jsClickIfMac("body > csd-root > div > csd-login-flow > div > div > csd-login > div > div > csd-nav-links > div > div:nth-child(1) > a");
            jsClickIfMac("#cdk-step-content-0-0 > div > div:nth-child(6) > label");
            jsClickIfMac("#cdk-step-content-0-0 > div > div.col-xs-12.col-sm-12.col-md-12.col-lg-12.registrationRowPad.ng-star-inserted > button");

            ReportUtils.logStepStart("Enter Name");
            driver.waitForAllElementVisible();
            driver.findElementByCssSelector("#fName").sendKeys(firstName);
            driver.findElementByCssSelector("#lName").sendKeys(lastName);
            driver.findElementByCssSelector("#accountNumber").sendKeys(accountNo);
            jsClickIfMac("#cdk-step-content-0-1 > div > fieldset:nth-child(3) > div:nth-child(2) > div.form-group.col-xs-6.col-sm-6.col-md-6.col-lg-6 > div > label");

            ReportUtils.logStepStart("Enter Contact Info");
            driver.waitForAllElementVisible();
            driver.findElementByCssSelector("#mobile").sendKeys(mobile);
            driver.findElementByCssSelector("#email").sendKeys(email);
//            ReportUtils.logAssert("Validating message", list.size() > 0 && list.get(0).getText().contains(message));
        }
    }

    private void jsClickIfMac(String cssSelector)
    {
        String browserName =driver.getCapabilities().getCapability("browserName").toString();
//        if (browserName.toUpperCase().contains("SAFARI")) {
            js.executeScript("arguments[0].click();", driver.findElementByCssSelector(cssSelector));
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
//        } else {
//            driver.findElementByCssSelector(cssSelector).click();
//        }
    }

    private void samsungClick(String label)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungClick(String label, String direction, String offset)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("words", "words");
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        params.put("label.direction", direction);
        params.put("label.offset", offset);
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungFind(String label) {
        Map<String, Object> params = new HashMap<>();
        params.put("content", label);
        driver.executeScript("mobile:text:find", params);
    }
}
