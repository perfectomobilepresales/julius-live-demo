package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@QAFTestStepProvider
public class AvivaStepDefs
{
    QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
    JavascriptExecutor js = DeviceUtils.getQAFDriver();

    @Then("^I get a car insurance quote for \"([^\"]*)\" \"([^\"]*)\" with \"([^\"]*)\" at \"([^\"]*)\" and \"([^\"]*)\" for car registration number \"([^\"]*)\"$")
    public void IGetCarQuote(String firstName, String lastName, String dob, String postcode, String mobile, String carReg) {
        String username = "perfectodemouk@gmail.com";
        String password = "A1b2c3d4e5";
        String day = dob.substring(0, dob.indexOf("/"));
        String month = dob.substring(dob.indexOf("/") + 1, dob.lastIndexOf("/"));
        String year = dob.substring(dob.lastIndexOf("/") + 1, dob.length());
        String message = "We couldn't match you to our records";

        Capabilities caps = DeviceUtils.getQAFDriver().getCapabilities();
        Set<String> capNames = caps.getCapabilityNames();

        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS) && capNames.contains("bundleId") && caps.getCapability("bundleId").toString().toUpperCase().contains("CHROME"))
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { driver.findElementByXPath("//*[@label=\"I'm OK with analytics cookies\"]").click(); } catch (Exception ignored) {;}
            driver.findElementByXPath("//*[@label=\"Start now\"]").click();
            try { Thread.sleep(3000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Enter date of birth");
            driver.waitForAllElementVisible();
            driver.findElementByXPath("//*[@label=\"main\"]//XCUIElementTypeOther[@label=\"What is your date of birth?\"]/XCUIElementTypeOther[4]/XCUIElementTypeOther[2]/XCUIElementTypeTextField[1]").sendKeys(day);
            driver.findElementByXPath("//*[@label=\"main\"]//XCUIElementTypeOther[@label=\"What is your date of birth?\"]/XCUIElementTypeOther[4]/XCUIElementTypeOther[4]/XCUIElementTypeTextField[1]").sendKeys(month);
            driver.findElementByXPath("//*[@label=\"main\"]//XCUIElementTypeOther[@label=\"What is your date of birth?\"]/XCUIElementTypeOther[4]/XCUIElementTypeOther[6]/XCUIElementTypeTextField[1]").sendKeys(year);
            driver.findElementByXPath("//*[@label=\"Next\"]").click();
            try { Thread.sleep(3000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Enter full name");
            driver.waitForAllElementVisible();
            driver.findElementByXPath("//XCUIElementTypeOther[@label=\"What is your name?\"]/XCUIElementTypeOther[4]/XCUIElementTypeTextField[1]").sendKeys(firstName);
            driver.findElementByXPath("//XCUIElementTypeOther[@label=\"What is your name?\"]/XCUIElementTypeOther[7]/XCUIElementTypeTextField[1]").sendKeys(lastName);
            driver.findElementByXPath("//*[@label=\"Next\"]").click();
            try { Thread.sleep(3000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Enter postcode");
            driver.waitForAllElementVisible();
            driver.findElementByXPath("//XCUIElementTypeTextField").sendKeys(postcode);
            driver.findElementByXPath("//*[@label=\"Next\"]").click();
            try { Thread.sleep(5000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Check for message");
            driver.waitForAllElementVisible();
            ReportUtils.logAssert("Validating message", driver.findElementByXPath("//*[@value=\"We couldn't match you to our records\"]").getText().contains(message));

            // close tab
            driver.findElementByXPath("//*[@label=\"Show Tabs\"]").click();
            driver.findElementByXPath("//*[@name=\"GridCellCloseButtonIdentifier\"]").click();
            driver.findElementByXPath("//*[@label=\"Create new tab.\"]").click();
        }
        else if (capNames.contains("appPackage") && caps.getCapability("appPackage").toString().toUpperCase().contains("SBROWSER"))
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { samsungClick("OK with analytics"); } catch (Exception ignored) {;}
            CommonSteps.ISwipeUpALot();
            CommonSteps.ISwipeUpALot();
            CommonSteps.ISwipeUpALot();
            samsungClick("Start now");

            ReportUtils.logStepStart("Enter date of birth");
            driver.waitForAllElementVisible();
            samsungClick("Day", "Above", "3%");
            CommonSteps.ISendKeys(day);
            samsungClick("Month", "Above", "3%");
            CommonSteps.ISendKeys(month);
            samsungClick("Year", "Above", "3%");
            CommonSteps.ISendKeys(year);
            samsungClick("Next");

            ReportUtils.logStepStart("Enter full name");
            driver.waitForAllElementVisible();
            samsungClick("First name", "Above", "3%");
            CommonSteps.ISendKeys(firstName);
            samsungClick("Last name", "Above", "3%");
            CommonSteps.ISendKeys(lastName);
            samsungClick("Next");

            ReportUtils.logStepStart("Enter postcode");
            driver.waitForAllElementVisible();
            samsungClick("What is", "Above", "5%");
            CommonSteps.ISendKeys(postcode);
            samsungClick("Next");

            ReportUtils.logStepStart("Check for message");
            try { Thread.sleep(5000); } catch (Exception ignored) {;}
            driver.waitForAllElementVisible();
            samsungFind(message);

            // close tab
            DeviceUtils.closeApp(caps.getCapability("appPackage").toString(),"identifier");
            DeviceUtils.startApp(caps.getCapability("appPackage").toString(),"identifier");
        }
        else
        {
            ReportUtils.logStepStart("Clear cookies");
            driver.waitForAllElementVisible();
            try { Thread.sleep(5000); } catch (Exception ignored) {;}
            try { driver.findElementByCssSelector("#onetrust-accept-btn-handler").click();} catch (Exception ignored) {;}

            ReportUtils.logStepStart("Enter login credentials");
            driver.waitForAllElementVisible();
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            driver.findElementByCssSelector("#username").sendKeys(username);
            driver.findElementByCssSelector("#password").sendKeys(password);
            jsClickIfMac("#loginButton");

            ReportUtils.logStepStart("Get a car quote");
            driver.waitForAllElementVisible();
            try { Thread.sleep(3000); } catch (Exception ignored) {;}
            jsClickIfMac("#main > div.l-section.l-section--guest-header > div:nth-child(2) > div > div.l-columns__column.qa-link-get-a-quote-header > a"); // get a quote
            jsClickIfMac("#main > div.contentsection.parsys > section > div > div > div > div:nth-child(1) > p:nth-child(2) > a"); // car insurance
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            driver.findElementByCssSelector("#car-registration").sendKeys(carReg);
            jsClickIfMac("#main > div.hero-wrapper.hero-wrapper--viewport-height.hero-wrapper--large-screen-middle-left.hero-wrapper--small-screen-bottom-left > div > div > div > div > form > button");

            ReportUtils.logStepStart("Enter personal details");
            driver.waitForAllElementVisible();
            try { Thread.sleep(1000); } catch (Exception ignored) {;}
            jsClickIfMac("fieldset.m-form-row:nth-child(4) > div:nth-child(2) > ul:nth-child(1) > li:nth-child(1) > label:nth-child(1) > span:nth-child(2) > span:nth-child(1)");
            driver.findElementByCssSelector("#PrincipalPolicyHolder_FirstName").sendKeys(firstName);
            driver.findElementByCssSelector("#PrincipalPolicyHolder_LastName").sendKeys(lastName);
            driver.findElementByCssSelector("#PrincipalPolicyHolder_DateOfBirthDay").sendKeys(day);
            driver.findElementByCssSelector("#PrincipalPolicyHolder_DateOfBirthMonth").sendKeys(month);
            driver.findElementByCssSelector("#PrincipalPolicyHolder_DateOfBirthYear").sendKeys(year);
            driver.findElementByCssSelector("#PrincipalPolicyHolder_PostCode").sendKeys(postcode);
            jsClickIfMac("#addressLookup");
            try { Thread.sleep(5000); } catch (Exception ignored) {;}
            jsClickIfMac("#PrincipalPolicyHolder_MultiplePostCode", 2);
            jsClickIfMac("#PrincipalPolicyHolder_MultiplePostCode > option:nth-child(2)");
            driver.findElementByCssSelector("#PrincipalPolicyHolder_EmailAddress").sendKeys(username);
            driver.findElementByCssSelector("#PrincipalPolicyHolder_TelephoneNumber").sendKeys(mobile);
            jsClickIfMac("#continueButton");

            ReportUtils.logStepStart("Enter vehicle details");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            jsClickIfMac("fieldset.m-form-row:nth-child(11) > div:nth-child(2) > ul:nth-child(1) > li:nth-child(2) > label:nth-child(1) > span:nth-child(2) > span:nth-child(1)");
            driver.findElementByCssSelector("#CoverStartDatePicker").sendKeys("20/11/2021");
            jsClickIfMac("#nextButton");

            ReportUtils.logStepStart("Enter driver details");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            jsClickIfMac("#LicenceType > option:nth-child(2)");
            jsClickIfMac("#LengthLicenceHeld > option:nth-child(11)");
            jsClickIfMac("#NumberOfConvictions > option:nth-child(2)");
            jsClickIfMac("#Vehicle_Cover_NoClaimsDiscount_NumberOfYears > option:nth-child(12)");
            jsClickIfMac("#main > form > div.m-form-row-no-border > ul > li:nth-child(2) > button");

            ReportUtils.logStepStart("Confirm details");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            jsClickIfMac("#assumptionsBox > ul > li:nth-child(3) > span > input");

            ReportUtils.logStepStart("View quote details");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
        }
    }


    @Then("^I retrieve a car insurance quote for \"([^\"]*)\" \"([^\"]*)\" with \"([^\"]*)\" at \"([^\"]*)\"$")
    public void IRetrieveCarQuote(String firstName, String lastName, String dob, String postcode) {
        String username = "perfectodemouk@gmail.com";
        String password = "A1b2c3d4e5";
        String day = dob.substring(0, dob.indexOf("/"));
        String month = dob.substring(dob.indexOf("/") + 1, dob.lastIndexOf("/"));
        String year = dob.substring(dob.lastIndexOf("/") + 1, dob.length());
        String message = "We couldn't match you to our records";

        Capabilities caps = DeviceUtils.getQAFDriver().getCapabilities();
        Set<String> capNames = caps.getCapabilityNames();

        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS))
        {
            try {
                ReportUtils.logStepStart("Log in to mobile app");
                CommonStep.sendKeys("A1b2c3d4e5", "aviva.password");
                CommonStep.click("ios.bttn.done");
                CommonStep.click("aviva.login");
                Thread.sleep(6000);

                ReportUtils.logStepStart("Retrieve insurance quote");
                CommonStep.click("aviva.offers");
                CommonStep.click("aviva.retrieve");
                CommonSteps.ISwipeUpALot();
                CommonSteps.ISwipeUpALot();
                Thread.sleep(3000);

                ReportUtils.logStepStart("Select car insurance quote");
                PerfectoSteps.IVisualPressOn("Your Car Insurance quote");
                Thread.sleep(2000);
                PerfectoSteps.IVisualPressOn("Retrieve a quote");

                ReportUtils.logStepStart("Provide personal info");
                PerfectoSteps.IVisualPressOnBy("First name", "12%", "above");
                driver.getKeyboard().sendKeys(firstName);
                Thread.sleep(1000);
                PerfectoSteps.IVisualPressOnBy("Last name", "6%", "above");
                driver.getKeyboard().sendKeys(lastName);
                Thread.sleep(1000);
                PerfectoSteps.IVisualPressOnBy("Postcode", "12%", "above");
                driver.getKeyboard().sendKeys(postcode);
                Thread.sleep(1000);
                PerfectoSteps.IVisualPressOn("DD");
                driver.getKeyboard().sendKeys(day);
                PerfectoSteps.IVisualPressOn("MM");
                driver.getKeyboard().sendKeys(month);
                PerfectoSteps.IVisualPressOn("YYYY");
                driver.getKeyboard().sendKeys(year);
                Thread.sleep(1000);

                ReportUtils.logStepStart("Retrieve quote and validate");
                PerfectoSteps.IVisualPressOn("Retrieve Quote");
            } catch (Exception ignored) {;}
        }
    }

    private void jsClickIfMac(String cssSelector)
    {
        String browserName =driver.getCapabilities().getCapability("browserName").toString();
        if (browserName.toUpperCase().contains("SAFARI")) {
            js.executeScript("arguments[0].click();", driver.findElementByCssSelector(cssSelector));
            try { Thread.sleep(3000); } catch (Exception ignored) {;}
        } else {
            driver.findElementByCssSelector(cssSelector).click();
        }
    }

    public void jsClickIfMac(String cssSelector, int index) {
        String browserName =driver.getCapabilities().getCapability("browserName").toString();
        if (browserName.toUpperCase().contains("SAFARI")) {
            js.executeScript("arguments[0].selectedIndex=" + index + ";", driver.findElementByCssSelector(cssSelector));
        } else {
            driver.findElementByCssSelector(cssSelector).click();
        }
    }

    private void samsungClick(String label)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungClick(String label, String direction, String offset)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        params.put("label.direction", direction);
        params.put("label.offset", offset);
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungFind(String label) {
        Map<String, Object> params = new HashMap<>();
        params.put("content", label);
        driver.executeScript("mobile:text:find", params);
    }
}
