package com.quantum.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

import com.perfecto.PerfectoLabUtils;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.CloudUtils;
import cucumber.api.java.en.Then;

// import com.perfecto.PerfectoLabUtils;

import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Graphics2D;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

@QAFTestStepProvider
public class TideStepDefs
{
    @Then("^I input the security code \"(.*?)\"$")
//    @QAFTestStep(description = "I input the security code {inCode}")
    public void IInputSecCode(String inCode) throws Exception {
        DeviceUtils.switchToContext("NATIVE_APP");
        String txtLabel = "text";
//        String lblElem = "hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.view.ViewGroup[1]/android.widget.TextView[3]";
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
        	txtLabel = "label";
//        	lblElem = "XCUIElementTypeStaticText";
        }
        int codeLength = inCode.length();
        for (int i = 0; i < codeLength; i++) {
            CommonStep.click("xpath=//*[@" + txtLabel + "=\"" + inCode.substring(i, i + 1) + "\"]");
        }
    }

    @Then("^I enter my phone number$")
    public void IEnterPhoneNo() throws Exception {
        DeviceUtils.switchToContext("NATIVE_APP");
        String telNo = DeviceUtils.getDeviceProperty("phoneNumber");
        telNo = telNo.substring(telNo.length() - 10);
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            int telNoLength = telNo.length();
            for (int i = 0; i < telNoLength; i++) {
                CommonStep.click("xpath=//*[@label=" + telNo.substring(i, i + 1) + "]");
            }
        } else {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            DeviceUtils.switchToContext("NATIVE_APP");
            driver.findElementByXPath("//*[@resource-id=\"com.tideplatform.banking.staging:id/et_mobile_number\"]|//*[@resource-id=\"com.tideplatform.banking.staging:id/editView\"]").sendKeys(telNo);
        }
    }

    @Then("^I scan ID document$")
    public void IScanID() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        try {
            for (int i=0; i<10; i++) {
                DeviceUtils.switchToContext("NATIVE_APP");
                if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
                    driver.findElementByXPath("//*[@label=\"button scan passport\"]").click();
                    Thread.currentThread().sleep(15000);
                    driver.findElementByXPath("//*[@label='OK']").click();
                } else {
                    driver.findElementByXPath("//*[@resource-id=\"com.tideplatform.banking.staging:id/button_shot\"]").click();
                    Thread.currentThread().sleep(15000);
                    driver.findElementByXPath("//*[@text=\"OK\"]").click();
                }
            }
        } catch (Exception e)
        {
            ;
        }
    }

    @Then("^I scan ID for \"(.*?)\" seconds$")
    public void IScanIDFor(String time) throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        try {
            for (int i=0; i<10; i++) {
                DeviceUtils.switchToContext("NATIVE_APP");
                if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
                    driver.findElementByXPath("//*[@label=\"button scan passport\"]").click();
                    Thread.currentThread().sleep(Integer.valueOf(time)*1000);
                    driver.findElementByXPath("//*[@label='OK']").click();
                } else {
                    driver.findElementByXPath("//*[@resource-id=\"com.tideplatform.banking.staging:id/button_shot\"]").click();
                    Thread.currentThread().sleep(Integer.valueOf(time)*1000);
                    driver.findElementByXPath("//*[@text=\"OK\"]").click();
                }
            }
        } catch (Exception e)
        {
            ;
        }
    }
    @Then("^I read the text message$")
    public void IReadTxtMsg() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        String path;
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            path = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[3]/XCUIElementTypeOther[1]";
            String txtMsg = driver.findElementByXPath(path).getAttribute("label");
            getBundle().setProperty("securityCode",txtMsg.substring(txtMsg.length()-15,txtMsg.length()-7));
        } else {
            path = "//*[@resource-id=\"com.samsung.android.messaging:id/content_text_view\"]";
            String txtMsg = driver.findElementByXPath(path).getAttribute("text");
            getBundle().setProperty("securityCode",txtMsg.substring(txtMsg.length()-15,txtMsg.length()-7));
        }

    }

    @Then("^I read the sign up SMS message$")
    public void IReadSignUpSMS() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        String path;
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            path = "//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[3]/XCUIElementTypeOther[1]";
            String txtMsg = driver.findElementByXPath(path).getAttribute("label");
            getBundle().setProperty("securityCode",txtMsg.substring(txtMsg.length()-11,txtMsg.length()-7));

        } else {
            path = "//*[@resource-id=\"com.samsung.android.messaging:id/content_text_view\"]";
            String txtMsg = driver.findElementByXPath(path).getAttribute("text");
            getBundle().setProperty("securityCode",txtMsg.substring(txtMsg.length()-11,txtMsg.length()-7));
        }

    }
    
    @Then("^I input the pass code$")
    public void IInputPassCode() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        driver.findElementByXPath("//XCUIElementTypeTextField").sendKeys(getBundle().getString("securityCode"));
    }

    @Then("^I input the confirmation code$")
    public void IInputConfirmationCode() throws Exception {
    	try {
    		IInputSecCode(getBundle().getString("securityCode"));
    	} catch (Exception e)
    	{
    		;
    	}
    }
    
    @Then("^I enter a random email on Tide$")
    public void IEnterRandomEmailOnTide() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        double number = (Math.random()*((99999 - 100)+1))+100;
        int randomNo = Double.valueOf(number).intValue();
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            driver.findElementByXPath("//*[@value=\"Enter email address\"]").sendKeys("perfecto" + Integer.toString(randomNo) + "@gmail.com");
        } else {
            driver.findElementByXPath("//*[@resource-id=\"com.tideplatform.banking.staging:id/editView\"]|//*[@resource-id=\"com.tideplatform.banking.staging:id/editView\"]").sendKeys("perfecto" + Integer.toString(randomNo) + "@gmail.com");
        }
    }

    @Then("^I enter a semi random email$")
    public void IEnterSemiRandomEmail() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        double number = (Math.random()*((99999 - 100)+1))+100;
        int randomNo = Double.valueOf(number).intValue();
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            driver.findElementByXPath("//*[@value=\"Enter email address\"]").sendKeys("perfectodemouk+" + Integer.toString(randomNo) + "@gmail.com");
        } else {
            driver.findElementByXPath("//*[@resource-id=\"com.tideplatform.banking.staging:id/editView\"]|//*[@resource-id=\"com.tideplatform.banking.staging:id/editView\"]").sendKeys("perfecto" + Integer.toString(randomNo) + "@gmail.com");
        }
    }

    @Then("^I get QR code from Tide Web")
    public void IGetQRCodeFromTideWeb() throws IOException {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        QAFExtendedWebElement ele = driver.findElementByXPath("//div[@id='qrcode']/img");
        String src = ele.getAttribute("src");

        String extension;

        String[] strings = src.split(",");
        switch (strings[0]) {//check image's extension
            case "data:image/jpeg;base64":
                extension = "jpeg";
                break;
            case "data:image/png;base64":
                extension = "png";
                break;
            default://should write cases for more images types
                extension = "jpg";
                break;
        }

        byte[] data = Base64.getDecoder().decode(strings[1]);

        String path = "/Users/juliusmong/Desktop/TideQR." + extension;
        File file = new File(path);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            double multiply = 3;
            resize(path, path, multiply);
            addBorder(path, path, 10, 10);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String username = "juliusm@perfectomobile.com";
        String securityToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJzbFV4OFFBdjdVellIajd4YWstR0tTbE43UjFNSllDbC1TRVJiTlU1RFlFIn0.eyJqdGkiOiI2NmNiMDZkYS01ZTE5LTRkYjMtYTUzOC0xMzdjOTEzOWNlOWEiLCJleHAiOjAsIm5iZiI6MCwiaWF0IjoxNTU5OTk1NzgxLCJpc3MiOiJodHRwczovL2F1dGgucGVyZmVjdG9tb2JpbGUuY29tL2F1dGgvcmVhbG1zL2RlbW8tcGVyZmVjdG9tb2JpbGUtY29tIiwiYXVkIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJzdWIiOiJlYzRlMzMyMS03OGJiLTQ1YzQtYjg1YS03YTBmY2VlZGZjM2UiLCJ0eXAiOiJPZmZsaW5lIiwiYXpwIjoib2ZmbGluZS10b2tlbi1nZW5lcmF0b3IiLCJub25jZSI6ImNlMGQ2ZGQ4LWNiZjYtNGFjOS04YjAwLTNhZGI2OTRjMDMzOCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjhmYzg5YmZmLTgzZmEtNGVlNy1iNWZmLWM1MDY5ZWU4MzkwNyIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fX0.Cy1ytviOnthSejolpQ-aVq4sibNn1IkJZ2sHx71snQxufKpiMU3ALfiYlAOMqe3Zu3UhCwlAFdMTkXS3ENleZCBKVyLhnNzALEIrSGoZgqyeQzrlYCy9Ypzuma7AytJc-sFYuf5PcrjZyaUiGEJCrAEs3w85b6Lk7x_ww_oaGlYtloXeubohvV3gEhTT3Dk1w6SC4DFYrrdOqm1qy9Lt7TUFfWQiaoi7UniMu74mr8L6A1dEYzvnAQEc561j_WhKeyAWi6HbUOyJhbftzbMVlVx5it4CDjmcDBCdMX6-t31WB2fGe3ngnYZqvtTx5L8QXFhRIUAbLV6p2HEvkt-WNw";
        CloudUtils.uploadMedia("demo.perfectomobile.com", username, securityToken, path, "PRIVATE:Tide/TideQRCode." + extension);
    }

    public static void resize(
            String inputImagePath,
            String outputImagePath, double percent) throws IOException {
        File inputFile = new File(inputImagePath);
        FileInputStream fis = new FileInputStream(inputFile);
        BufferedImage inputImage = ImageIO.read(fis);

        int scaledWidth = (int) (inputImage.getWidth() * percent);
        int scaledHeight = (int) (inputImage.getHeight() * percent);
        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
    }

    public static void resize(
            String inputImagePath,
            String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }

    public void addBorder(
            String inputImagePath,
            String outputImagePath, int borderLeft, int borderTop) throws IOException {
        File inputFile = new File(inputImagePath);
        FileInputStream fis = new FileInputStream(inputFile);
        BufferedImage source = ImageIO.read(fis);
        int width = source.getWidth();
        int height = source.getHeight();
        int borderedImageWidth = width + (borderLeft * 2);
        int borderedImageHeight = height + (borderTop * 2);
        BufferedImage img = new BufferedImage(borderedImageWidth, borderedImageHeight, BufferedImage.TYPE_3BYTE_BGR);
        img.createGraphics();
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, borderedImageWidth, borderedImageHeight);
        g.drawImage(source, borderLeft, borderTop, width + borderLeft, height + borderTop, 0, 0, width, height, Color.YELLOW, null);
        File output = File.createTempFile("output", ".png");
        ImageIO.write(img, "png", new File(outputImagePath));
    }

}