package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Then;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.perfecto.ShadowDomUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@QAFTestStepProvider
public class VodafoneStepDefs
{
    QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
    JavascriptExecutor js = DeviceUtils.getQAFDriver();

    @Then("^I search for \"([^\"]*)\" on Vodafone portal$")
    public void ISearchVodafone(String keyword) {
        Capabilities caps = DeviceUtils.getQAFDriver().getCapabilities();
        Set<String> capNames = caps.getCapabilityNames();
        ShadowDomUtils shadowUtls = new ShadowDomUtils();

        if (DeviceUtils.getDeviceProperty(Constants.OS).contains(Constants.IOS) && capNames.contains("bundleId") && caps.getCapability("bundleId").toString().toUpperCase().contains("CHROME"))
        {
            ReportUtils.logStepStart("Initiate a Search");
            driver.waitForAllElementVisible();
            try { Thread.sleep(4000); } catch (Exception ignored) {;}
            driver.findElementByXPath("//*[@label=\"Search\"]").click();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}

            ReportUtils.logStepStart("Search for keyword");
            driver.findElementByXPath("//*[@label=\"Insert Search word\"]").sendKeys(keyword);
            driver.findElementByXPath("//*[@label=\"search\"]//*[@label=\"Search\"]").click();

            ReportUtils.logStepStart("Choose first result");
            driver.waitForAllElementVisible();

            // close tab
            driver.findElementByXPath("//*[@label=\"Show Tabs\"]").click();
            driver.findElementByXPath("//*[@label=\"Close All\"]").click();
//            driver.findElementByXPath("//*[@label=\"Create new tab.\"]").click();
        }
        else if (capNames.contains("appPackage") && caps.getCapability("appPackage").toString().toUpperCase().contains("SBROWSER"))
        {
            ReportUtils.logStepStart("Initiate a Search");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            samsungClick("Welcome", "below", "7%");

            ReportUtils.logStepStart("Search for keyword");
            CommonSteps.ISendKeys(keyword);
            Map<String, Object> EnterKeyEvent  = new HashMap<>();
            EnterKeyEvent.put("key", "66");
            driver.executeScript("mobile:key:event", EnterKeyEvent);

            ReportUtils.logStepStart("Choose first result");
            driver.waitForAllElementVisible();

            // close tab
            DeviceUtils.closeApp(caps.getCapability("appPackage").toString(),"identifier");
            DeviceUtils.startApp(caps.getCapability("appPackage").toString(),"identifier");
        }
        else
        {
            ReportUtils.logStepStart("Initiate a Search");
            driver.waitForAllElementVisible();
            try { Thread.sleep(2000); } catch (Exception ignored) {;}
            WebElement parentShadowElement = driver.findElement(By.cssSelector("#spl-nav-wrapper > vha-header:nth-child(5)"));
            Map<String, Object> params = new HashMap<>();
            params.put("parentElement", parentShadowElement);
            params.put("innerSelector", ".nav-search-open");
            shadowUtls.clickElementShadowDOM(((RemoteWebDriver)driver), params);

            ReportUtils.logStepStart("Search for keyword");
            driver.findElementByCssSelector("#cludo-search-content-form-input").sendKeys(keyword);
            jsClickIfMac("#cludo-search-content-form > button");

            ReportUtils.logStepStart("Choose first result");
            driver.waitForAllElementVisible();

        }
    }

    private void jsClickIfMac(String cssSelector)
    {
        String browserName =driver.getCapabilities().getCapability("browserName").toString();
        if (browserName.toUpperCase().contains("SAFARI")) {
            js.executeScript("arguments[0].click();", driver.findElementByCssSelector(cssSelector));
            try { Thread.sleep(3000); } catch (Exception ignored) {;}
        } else {
            driver.findElementByCssSelector(cssSelector).click();
        }
    }

    private void samsungClick(String label)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungClick(String label, String direction, String offset)
    {
        Map<String, Object> params = new HashMap<>();
        params.put("label", label);
        params.put("screen.top", "10%");
        params.put("screen.left", "0%");
        params.put("screen.width", "100%");
        params.put("screen.height", "80%");
        params.put("label.direction", direction);
        params.put("label.offset", offset);
        driver.executeScript("mobile:button-text:click", params);
        try { Thread.sleep(3000); } catch (Exception ignored) {;}
    }

    private void samsungFind(String label) {
        Map<String, Object> params = new HashMap<>();
        params.put("content", label);
        driver.executeScript("mobile:text:find", params);
    }
}
