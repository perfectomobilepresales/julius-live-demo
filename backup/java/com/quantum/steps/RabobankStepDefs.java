package com.quantum.steps;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import java.util.HashMap;
import java.util.Map;

@QAFTestStepProvider
public class RabobankStepDefs
{
    @Then("^I want to see the language selection dropdown menu$")
    public void IWantToSeeTheLanguageSelectionDropdownMenu() throws Exception
    {
        String imgPath = "";
        int threshold = 90;
        int timeout = 60;
        if (DeviceUtils.getDeviceProperty("os").contains("Android"))
        {
            imgPath = "PRIVATE:Rabobank/S7_language_selection_open.png";
        }
        else if (DeviceUtils.getDeviceProperty("os").contains("iOS"))
        {
            imgPath = "PRIVATE:Rabobank/iPhoneX_language_selection_open.png";
        }
        else if (ConfigurationManager.getBundle().getProperty("appType").equals("Web"))
        {
            imgPath = "PRIVATE:Rabobank/Chrome_language_selection_open_low_res.png";
        }

        String context = DeviceUtils.getCurrentContext();
        DeviceUtils.switchToContext("VISUAL");
        Map<String, Object> params = new HashMap<>();
        params.put("content", imgPath);
        params.put("measurement", "accurate");
        params.put("source", "primary");
        params.put("threshold", threshold);
        params.put("timeout", timeout);
        params.put("match", "bounded");
        Object result = DeviceUtils.getQAFDriver().executeScript("mobile:checkpoint:image", params);
        DeviceUtils.switchToContext(context);

        Validator.verifyThat("Image " + imgPath + " should be visible", result.toString(), Matchers.equalTo("true"));

        if (result.toString().equalsIgnoreCase("true"))
        {
            ReportUtils.logVerify("The image file " + imgPath + " was verified successfully with threshold of " + threshold + "%", true);
        }
        else
        {
            ReportUtils.logVerify("The image file " + imgPath + " verification failed with threshold of " + threshold + "%", false);
        }
    }

    @When("^I click on select language$")
    public void IClickOnSelectLanguage()
    {
        QAFExtendedWebElement element = new QAFExtendedWebElement("language.label");

        if (ConfigurationManager.getBundle().getProperty("appType") != null)
        {
            CommonStep.click("language.label");
        }

        if (DeviceUtils.getDeviceProperty("os").contains("iOS"))
        {
            JavascriptExecutor js = DeviceUtils.getQAFDriver();
            js.executeScript("arguments[0].click();", element);
        }
        else if (DeviceUtils.getDeviceProperty("os").contains("Android"))
        {
            element.click();
        }
    }

    @Given("^I open Global Farmers website$")
    public void IOpenGlobalFarmersWebsite()
    {
        if (ConfigurationManager.getBundle().getProperty("appType") == null)
        {
            PerfectoApplicationSteps.navigateToURL("http://www.globalfarmers.com");
        }
        else if (ConfigurationManager.getBundle().getProperty("appType").equals("Web"))
        {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            driver.get("http://www.globalfarmers.com");
            //driver.manage().window().maximize();
            CommonStep.click("cookies.button");
        }
    }
}
