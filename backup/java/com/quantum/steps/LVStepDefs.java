package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.CloudUtils;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

// import com.perfecto.PerfectoLabUtils;

@QAFTestStepProvider
public class LVStepDefs
{
    @Then("^I view mens selection$")
    public void IViewMensLV() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            Map<String, Object> params = new HashMap<>();
            params.put("label","HERREN");
            DeviceUtils.getQAFDriver().executeScript(Constants.MOBILEBUTTONTEXTCLICK, params);
            Thread.currentThread().wait(1000);
            Map<String, Object> params2 = new HashMap<>();
            params2.put("label","Schuhe");
            DeviceUtils.getQAFDriver().executeScript(Constants.MOBILEBUTTONTEXTCLICK, params2);
            Thread.currentThread().wait(1000);
            Map<String, Object> params3 = new HashMap<>();
            params3.put("label","Alle Schuhe");
            DeviceUtils.getQAFDriver().executeScript(Constants.MOBILEBUTTONTEXTCLICK, params3);
        } else {
            driver.findElementByXPath("//*[@text=\"MEN\"]");
            Thread.currentThread().wait(1000);
            driver.findElementByXPath("//*[@text=\"Shoes\"]");
            Thread.currentThread().wait(1000);
            driver.findElementByXPath("//*[@text=\"All Shoes\"]");
        }
    }

}