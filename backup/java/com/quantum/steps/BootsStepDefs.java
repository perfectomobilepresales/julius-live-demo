package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriver;
import com.qmetry.qaf.automation.util.Validator;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.HashMap;
import java.util.Map;

@QAFTestStepProvider
public class BootsStepDefs
{

    @Then("^I want to see the Boots logo$")
    public void IWantToSeeTheBootsLogo() throws Exception
    {
        String imgPath = "";
        int threshold = 90;
        int timeout = 60;
        if (DeviceUtils.getCurrentContext().equals("WEBVIEW")) { //ConfigurationManager.getBundle().getProperty("appType").equals("Web")
            if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
                imgPath = "PRIVATE:Boots/Boots_iPhoneX.png";
            } else if (DeviceUtils.getDeviceProperty("os").contains("Android")) {
                imgPath = "PRIVATE:Boots/Boots_s7.png";
            }
        }
        else
        {
            threshold = 50;
            imgPath = "PRIVATE:Boots/Boots_app_logo.png";
        }

        String context = DeviceUtils.getCurrentContext();
        DeviceUtils.switchToContext("VISUAL");
        Map<String, Object> params = new HashMap<>();
        params.put("content", imgPath);
        params.put("measurement", "accurate");
        params.put("source", "primary");
        params.put("threshold", threshold);
        params.put("timeout", timeout);
        params.put("match", "bounded");
        Object result = DeviceUtils.getQAFDriver().executeScript("mobile:checkpoint:image", params);
        DeviceUtils.switchToContext(context);

        Validator.verifyThat("Image " + imgPath + " should be visible", result.toString(), Matchers.equalTo("true"));

        if (result.toString().equalsIgnoreCase("true"))
        {
            ReportUtils.logVerify("The image file " + imgPath + " was verified successfully with threshold of " + threshold + "%", true);
        }
        else
        {
            ReportUtils.logVerify("The image file " + imgPath + " verification failed with threshold of " + threshold + "%", false);
        }
    }

    @When("^I type \"([^\"]*)\" in input \"(.*?)\"")
    public void ITypeInInput(String query, String input)
    {
        QAFExtendedWebElement element = new QAFExtendedWebElement(input);
        //element.sendKeys(query);

        Actions actions = new Actions(DeviceUtils.getQAFDriver());
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(query);
        actions.sendKeys(Keys.ENTER);
        actions.build().perform();
    }

    @When("^I use JavaScript to click element \"(.*?)\"")
    public void IUseJavaScriptToClickElement(String locator)
    {
//    // Take screenshot and store as a file format
//    File src= DeviceUtils.getQAFDriver().getScreenshotAs(OutputType.FILE);
//    try {
//      // now copy the  screenshot to desired location using copyFile //method
//      FileUtils.copyFile(src, new File("/Users/yiminy/Downloads/error.png"));
//    }
//
//    catch (IOException e)
//    {
//      System.out.println(e.getMessage());
//
//    }
//
//    QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
//    WebDriverWait wait = new WebDriverWait(driver, 10);
        QAFExtendedWebElement element = new QAFExtendedWebElement(locator);
//
//    WebElement ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='estores_overlay_content']//a")));

//    element.click();
        JavascriptExecutor js = DeviceUtils.getQAFDriver();
        //js.executeScript("javascript:eStoreProductOverlay('shipToOverlay');", element);
        js.executeScript("javaScript:processSelection('GB','https://www.boots.com','','GB')", element);

        DeviceUtils.getQAFDriver().manage().window().maximize();
    }

}
