package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@QAFTestStepProvider
public class JohnLewisStepDefs {

    @When("I bring up the JL menu")
    public void IBringUpJLMenu()
    {
        Map<String, Object> params2 = new HashMap<>();
        params2.put("start", "0%,50%");
        params2.put("end", "95%,50%");
        params2.put("duration", "1");
        Object result2 = DeviceUtils.getQAFDriver().executeScript("mobile:touch:swipe", params2);
    }

}
