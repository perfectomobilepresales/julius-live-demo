package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;

import java.util.HashMap;
import java.util.Map;

@QAFTestStepProvider
public class FlutterGalleryStepDefs {

    @Then("^I press on \"(.*?)\"$")
    public void IPressOn(String inField) throws Exception {
        DeviceUtils.switchToContext(Constants.NATIVEAPP);
        DeviceUtils.getQAFDriver().findElementByXPath("//*[contains(text(),inField)]").click();
    }

}