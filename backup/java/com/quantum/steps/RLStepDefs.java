package com.quantum.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.CloudUtils;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;

@QAFTestStepProvider
public class RLStepDefs
{

    @Then("^I enter a random email on RL$")
    public void IEnterRandomEmailOnRL() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        double number = (Math.random()*((99999 - 100)+1))+100;
        int randomNo = Double.valueOf(number).intValue();
        String emailId = "uat" + Integer.toString(randomNo);
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            driver.findElementByXPath("//*[@label=\"Email address\" and @name=\"email\"]").sendKeys(emailId);
            getBundle().setProperty("emailAddr",emailId + "@rlg.com");
        } else {
            driver.findElementByXPath("//*[@resource-id=\"com.royallondon.android.uat:id/activity_enter_email_email_inner\"]").sendKeys(emailId);
            getBundle().setProperty("emailAddr",emailId + "@rlg.com");
        }
    }

    @Then("^I click firm on \"(.*?)\"$")
    public void IClickFirm(String inName) {

        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

        try {
            DeviceUtils.switchToContext("NATIVE_APP");
            driver.findElementByXPath("//*[@resource-id=\"com.royallondon.android.uat:id/activity_biometric_opt_in_cta_skip\"]").click();
            Thread.currentThread().sleep(1200);
        }
        catch (InterruptedException e)
        {
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }
        catch (Exception e)
        {
            ;
        }
    }
}