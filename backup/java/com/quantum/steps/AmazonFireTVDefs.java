package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.HashMap;
import java.util.Map;

@QAFTestStepProvider

public class AmazonFireTVDefs {

    private QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

    @Then("^I press UP")
    public void IPressUP() {
        try {
            Map<String, Object> params1 = new HashMap<>();
            params1.put("keySequence", "UP");
            Object result1 = driver.executeScript("mobile:presskey", params1);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press DOWN")
    public void IPressDOWN() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "DOWN");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press LEFT")
    public void IPressLEFT() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "LEFT");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press RIGHT")
    public void IPressRIGHT() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "RIGHT");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press ENTER")
    public void IPressENTER() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "ENTER");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press HOME")
    public void IPressHOME() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "HOME");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press BACK")
    public void IPressBACK() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "BACK");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press MENU")
    public void IPressMENU() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "MENU");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press PLAYorPAUSE")
    public void IPressPLAY() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "PLAY");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press FORWARD")
    public void IPressFORWARD() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "FORWARD");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

    @Then("^I press BACKWARD")
    public void IPressBACKWARD() {
        try {
            Map<String, Object> params2 = new HashMap<>();
            params2.put("keySequence", "BACKWARD");
            Object result2 = driver.executeScript("mobile:presskey", params2);
            Thread.sleep(2000);
        } catch (Exception ignored) {;}
    }

}
