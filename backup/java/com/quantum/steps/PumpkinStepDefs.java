package com.quantum.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

import com.perfecto.PerfectoLabUtils;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.CloudUtils;
import cucumber.api.java.en.Then;

// import com.perfecto.PerfectoLabUtils;

import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Graphics2D;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

@QAFTestStepProvider
public class PumpkinStepDefs
{

    private static final String[] numNames = {
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine"
    };

    @Then("^I enter PIN \"(.*?)\"$")
    public void IEnterPIN(String inCode) throws Exception {
//        DeviceUtils.switchToContext("NATIVE_APP");
        int codeLength = inCode.length();
        for (int i = 0; i < codeLength; i++) {
            CommonStep.click("xpath=//*[@resource-id=\"com.pumpkin.pumpkin.debug:id/" + numNames[Integer.parseInt(inCode.substring(i, i + 1))] + "\"]");
        }
    }

}