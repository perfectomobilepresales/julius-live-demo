package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@QAFTestStepProvider

public class HLStepDefs {

    private boolean bIsIOS = DeviceUtils.getDeviceProperty("os").contains("iOS");

    @When("^I test HL browser")
    public void ITestHLBrowser() {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        try {
            driver.get("http://www.hl.co.uk");
        } catch (Exception e) {
            ;
        }
    }

    @Given("^I start HL mobile app")
    public void IStartHLApp() {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        String appID = "uk.co.hl.ios.enterprise";
        if (!bIsIOS) appID = "com.mubaloo.android.hargreaveslansdown.qa";
        DeviceUtils.closeApp(appID, "identifier", true);
        DeviceUtils.startApp(appID, "identifier");
        DeviceUtils.switchToContext("NATIVE_APP");
    }

    @Then("^I go to \"(.*?)\"$")
    public void IGoTo(String inName) {

        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

        try {
            if (inName.contains("My accounts")) {
                if (bIsIOS) {
                    driver.findElementByXPath("//*[@label=\"My accounts\"]").click();
                } else {
                    driver.findElementByXPath("//*[@resource-id=\"com.mubaloo.android.hargreaveslansdown.qa:id/bottom_nav_portfolio\"]//*[@resource-id=\"com.mubaloo.android.hargreaveslansdown.qa:id/icon\"]").click();
                }
                Thread.currentThread().sleep(1200);
            } else {
                if (bIsIOS) {
                    driver.findElementByXPath("//*[@value=\"" + inName + "\"]").click();
                } else {
                    driver.findElementByXPath("//*[@text=\"" + inName + "\"]").click();
                }
                Thread.currentThread().sleep(1200);
            }
        } catch (InterruptedException e) {
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            ;
        }
    }

    @Given("^I enter test login \"(.*?)\" and dob \"(.*?)\"$")
    public void ILogin2HLApp(String username, String dob) {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

        try {
            if (bIsIOS) {
                driver.findElementByXPath("//*[@name=\"Username\"]").sendKeys(username);
                driver.findElementByXPath("//*[@name=\"Date of Birth\"]").sendKeys(dob);
                driver.findElementByXPath("//*[@name=\"Save details\"]").click();
                driver.findElementByXPath("//*[@label=\"Hargreaves Lansdown\"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]/XCUIElementTypeStaticText[1]").click();
            } else {
                driver.findElementByXPath("//*[@text=\"Username\"]").sendKeys(username);
                driver.findElementByXPath("//*[@text=\"Date of birth (ddmmyy)\"]").sendKeys(dob);
                driver.findElementByXPath("//*[@text=\"OFF\"]").click();
                driver.findElementByXPath("//*[@text=\"Continue\"]").click();
            }
            Thread.currentThread().sleep(1200);
        } catch (InterruptedException e) {
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            ;
        }
    }


}
