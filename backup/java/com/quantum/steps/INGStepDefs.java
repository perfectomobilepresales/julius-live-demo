package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.JavascriptExecutor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

@QAFTestStepProvider

public class INGStepDefs {

    @Then("^I validate ING logins:$")
    public void IValidateINGLogins(List<Map<Object,Object>> dataList) {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        for (int i=1; i<dataList.size(); i++)
        {
            Map<Object,Object> dataMap = dataList.get(i);
            if (i<2) new QAFExtendedWebElement("ing.accountNo").sendKeys(dataMap.get("accountNo").toString());
            new QAFExtendedWebElement("ing.pin").sendKeys(dataMap.get("pin").toString());
            new QAFExtendedWebElement("ing.signIn").click();
            new QAFExtendedWebElement("ing.retry").click();
        }
    }

}
