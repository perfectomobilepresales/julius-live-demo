package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.JavascriptExecutor;

@QAFTestStepProvider

public class EdenredStepDefs {

    @Then("^I accept Edenred cookies$")
    public void IAcceptEdenredCookies()
    {
        try {
            QAFExtendedWebDriver driver= DeviceUtils.getQAFDriver();
            driver.findElementByCssSelector("div.ub-emb-iframe-wrapper:nth-child(1) > button:nth-child(1)\n").click();
        } catch (Exception e) {
            ;
        }
    }

    @When("^I click thru on Edenred$")
    public void IClickThruEdenred()
    {
        try {
            QAFExtendedWebDriver driver= DeviceUtils.getQAFDriver();
            driver.findElementByCssSelector(".tumK").click();
            Thread.currentThread().sleep(1000);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("window.scrollBy(0,1000)");
            driver.findElementByCssSelector("article.kampanya:nth-child(6) > div:nth-child(2) > a:nth-child(1)").click();
            Thread.currentThread().sleep(1000);
        } catch (Exception e) {
            ;
        }
    }

    @Then("^I find \"([^\"]*)\" on Edenred")
    public void IFindOnEdenred(String searchKey) throws Throwable
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            QAFExtendedWebElement searchResultElement = driver.findElementByXPath("//*[contains(text(),searchKey)]");
            searchResultElement.verifyPresent();
        } catch (Exception e) {
            ;
        }
    }
}
