package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Then;

import java.util.HashMap;
import java.util.Map;

@QAFTestStepProvider
public class WattsStepDefs
{
    @Then("^I login to Watts by XPath$")
    public void ITestWattsXPath() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        driver.findElementByXPath("(//a[@id=\"createAccount\"])[1]").click();
        Thread.currentThread().sleep(2000);
        driver.findElementByXPath("(//input[@id=\"email\"])[1]").sendKeys("test123@test.com");
        driver.findElementByXPath("(//input[@id=\"newPassword\"])[1]").sendKeys("test123456");
        driver.findElementByXPath("(//input[@id=\"reenterPassword\"])[1]").sendKeys("test123456");
        driver.findElementByXPath("(//input[@id=\"givenName\"])[1]").sendKeys("Perfecto");
        driver.findElementByXPath("(//input[@id=\"surname\"])[1]").sendKeys("Demo");
        driver.findElementByXPath("//*[@id=\"extension_Company\"]").sendKeys("Perfecto");
        driver.findElementByXPath("//*[@id=\"jobTitle\"]").sendKeys("SE");
        driver.findElementByXPath("//*[@id=\"streetAddress\"]").sendKeys("1 Sunset Boulevard");
        driver.findElementByXPath("//*[@id=\"city\"]").sendKeys("Sin City");
        driver.findElementByXPath("//*[@id=\"state\"]").sendKeys("Sunny State");
        driver.findElementByXPath("//*[@id=\"postalCode\"]").sendKeys("12345");
        driver.findElementByXPath("//*[@id=\"country\"]").sendKeys("United States");
        DeviceUtils.swipe("50%,50%", "50%,10%");
        driver.findElementByXPath("//*[@id=\"extension_AgreedToTermsAndConditions_1\"]").click();
        driver.findElementByXPath("//*[@id=\"continue\"]").click();
    }

    @Then("^I login to Watts by CSS$")
    public void ITestWattsCSS() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        driver.findElementByCssSelector("#createAccount").click();
        Thread.currentThread().sleep(2000);
        driver.findElementByCssSelector("#email").sendKeys("test123@test.com");
        driver.findElementByCssSelector("#newPassword").sendKeys("test123456");
        driver.findElementByCssSelector("#reenterPassword").sendKeys("test123456");
        driver.findElementByCssSelector("#givenName").sendKeys("Perfecto");
        driver.findElementByCssSelector("#surname").sendKeys("Demo");
        driver.findElementByCssSelector("#extension_Company").sendKeys("Perfecto");
        driver.findElementByCssSelector("#jobTitle").sendKeys("SE");
        driver.findElementByCssSelector("#streetAddress").sendKeys("1 Sunset Boulevard");
        driver.findElementByCssSelector("#city").sendKeys("Sin City");
        driver.findElementByCssSelector("#state").sendKeys("Sunny State");
        driver.findElementByCssSelector("#postalCode").sendKeys("12345");
        driver.findElementByCssSelector("#country").sendKeys("United States");
        DeviceUtils.swipe("50%,50%", "50%,10%");
        driver.findElementByCssSelector("#extension_AgreedToTermsAndConditions_1").click();
        driver.findElementByCssSelector("#continue").click();
    }
}
