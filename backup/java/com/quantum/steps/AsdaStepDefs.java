package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

@QAFTestStepProvider

public class AsdaStepDefs {

    private boolean bIsIOS = DeviceUtils.getDeviceProperty("os").contains("iOS");

    @Then("^I set the slider")
    public void ISetSlider() {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

        try {
            DeviceUtils.switchToContext("NATIVE_APP");
            if (bIsIOS) {
                driver.findElementByXPath("//*[@value=\"0 %\"]").click();
            } else {
                driver.findElementByXPath("//*[@class=\"android.widget.SeekBar\"]").click();
            }
            Thread.currentThread().sleep(1200);
        }
        catch (InterruptedException e)
        {
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }
        catch (Exception e)
        {
            ;
        }
    }

    @Then("^I login to Asda")
    public void ILogin2Asda()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

        try {
            DeviceUtils.switchToContext("NATIVE_APP");
            if (bIsIOS) {
                Thread.currentThread().sleep(3000);
                driver.findElementByXPath("//*[@label=\"Sign in\"]").click();
                Thread.currentThread().sleep(2000);
                driver.findElementByXPath("//XCUIElementTypeOther[@label=\"SIGN_IN_INPUT_EMAIL_ADDRESS\"]").sendKeys("perfectodemouk@gmail.com");
                driver.findElementByXPath("//XCUIElementTypeOther[@label=\"SIGN_IN_INPUT_PASSWORD\"]").sendKeys("Perfecto2019!");
                driver.findElementByXPath("//AppiumAUT/XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[4]").click();
            } else {
                Thread.currentThread().sleep(3000);
                driver.findElementByXPath("//*[@text=\"Sign in\"]").click();
                Thread.currentThread().sleep(2000);
                driver.findElementByXPath("//*[@content-desc=\"SIGN_IN_INPUT_EMAIL_ADDRESS\"]").click();
                driver.findElementByXPath("//*[@content-desc=\"SIGN_IN_INPUT_EMAIL_ADDRESS\"]").sendKeys("perfectodemouk@gmail.com");
                driver.findElementByXPath("//*[@content-desc=\"SIGN_IN_INPUT_PASSWORD\"]").click();
                driver.findElementByXPath("//*[@content-desc=\"SIGN_IN_INPUT_PASSWORD\"]").sendKeys("Perfecto2019!");
                driver.findElementByXPath("//*[@text=\"Sign in\"]").click();
            }
        }
        catch (InterruptedException e)
        {
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }
        catch (Exception e)
        {
            ;
        }
    }


    @When("^I scan barcode \"(.*?)\"$")
    public void IScanBarcode(String imageFile) {

        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

        try {
            Map<String, Object> params6 = new HashMap<>();
            params6.put("repositoryFile", "GROUP:Julius/Asda/" + imageFile + ".png");
            if (bIsIOS)
                params6.put("name", "Scan & Go");
            else
                params6.put("name", "Scan And Go");
            Object result6 = driver.executeScript("mobile:image.injection:start", params6);
            if (imageFile.contains("ASP1008799")) {
                if (bIsIOS)
                {
                    IHit("START_SHOPPING");
                    PerfectoApplicationSteps.waitToAppear(3, "//*[@label=\"OPEN_CHECKOUT\"]");
                } else {
                    IHit("Start shopping");
                    PerfectoApplicationSteps.waitToAppear(3, "//*[@text=\"Checkout\"]");
                }
                ICreateNewCart();
            } else if (imageFile.contains("Checkout")) {
                DeviceUtils.switchToContext("NATIVE_APP");
                if (bIsIOS) {
                    driver.findElementByXPath("//*[@label=\"MAIN_CONTENT_CONTAINER\"]/XCUIElementTypeOther[1]").click();
                    PerfectoApplicationSteps.waitToAppear(3, "//*[@label=\"Yes\"]");
                    Thread.currentThread().sleep(3000);
                } else {
                    driver.findElementByXPath("//*[@text=\"Tap screen to scan\"]").click();
                    PerfectoApplicationSteps.waitToAppear(3, "//*[@text=\"Yes\"]");
                    Thread.currentThread().sleep(3000);
                }
            } else {
                if (bIsIOS)
                {
                    PerfectoApplicationSteps.waitToAppear(3, "//*[@label=\"MAIN_CONTENT_CONTAINER\"]/XCUIElementTypeOther[1]");
                    driver.findElementByXPath("//*[@label=\"MAIN_CONTENT_CONTAINER\"]/XCUIElementTypeOther[1]").click();
                } else {
                    PerfectoApplicationSteps.waitToAppear(3, "//*[@text=\"Tap screen to scan\"]");
                    driver.findElementByXPath("//*[@text=\"Tap screen to scan\"]").click();
                }
            }
            Map<String, Object> params7 = new HashMap<>();
            Object result7 = driver.executeScript("mobile:image.injection:stop", params7);
        }
        catch (Exception e)
        {
            ;
        }
    }


    @Then("^I write comment \"(.*?)\"$")
    public void IWriteComment(String txtComment)
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

            DeviceUtils.switchToContext("NATIVE_APP");
            QAFExtendedWebElement element;
            if (bIsIOS)
                element = driver.findElementByXPath("//XCUIElementTypeTextView");
            else
                element = driver.findElementByXPath("//*[@text=\"Enter comments here…\"]");
            element.click();
            driver.getKeyboard().sendKeys(txtComment);
            if (!bIsIOS)
                DeviceUtils.hideKeyboard();
        } catch (Exception e) {
            ;
        }
    }

    @Then("^I create new cart$")
    public void ICreateNewCart()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        try {
            DeviceUtils.switchToContext("NATIVE_APP");
            if (bIsIOS) {
                driver.findElementByXPath("//*[@label=\"Create New Cart\"]").click();
                Thread.currentThread().sleep(1200);
                PerfectoApplicationSteps.waitToAppear(3, "//*[@label=\"MAIN_CONTENT_CONTAINER\"]/XCUIElementTypeOther[1]");
                driver.findElementByXPath("//*[@label=\"MAIN_CONTENT_CONTAINER\"]/XCUIElementTypeOther[1]").click();
                Thread.currentThread().sleep(3000);
            } else {
                driver.findElementByXPath("//*[@text=\"Create New Cart\"]").click();
                Thread.currentThread().sleep(1200);
                PerfectoApplicationSteps.waitToAppear(3, "//*[@text=\"Tap screen to scan\"]");
                driver.findElementByXPath("//*[@text=\"Tap screen to scan\"]").click();
                Thread.currentThread().sleep(3000);
            }
        }
        catch (InterruptedException e)
        {
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }
        catch (Exception e)
        {
            ;
        }
    }

    @Then("^I hit \"(.*?)\"$")
    public void IHit(String inName) {

        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();

        try {
            DeviceUtils.switchToContext("NATIVE_APP");
            if (bIsIOS) {
                driver.findElementByXPath("//*[@label=\"" + inName + "\"]").click();
            } else {
                driver.findElementByXPath("//*[@text=\"" + inName + "\"]").click();
            }
            Thread.currentThread().sleep(1200);
        }
        catch (InterruptedException e)
        {
            // Restore interrupted state...
            Thread.currentThread().interrupt();
        }
        catch (Exception e)
        {
            ;
        }
    }

    @Then("^I complete QC check$")
    public void ICompleteQCCheck()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params7 = new HashMap<>();
        params7.put("label", "Barcode Scanned");
        params7.put("interval", "1");
        params7.put("timeout", "10");
        params7.put("ignorecase", "nocase");
        Object result7 = driver.executeScript("mobile:button-text:click", params7);
    }

    @Then("^I finish shopping$")
    public void IFinishShopping()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params7 = new HashMap<>();
        params7.put("label", "Finished");
        params7.put("interval", "1");
        params7.put("timeout", "10");
        params7.put("ignorecase", "nocase");
        Object result7 = driver.executeScript("mobile:button-text:click", params7);
    }

    @Then("^I scan received barcode \"(.*?)\"$")
    public void IScanReceivingBarcode(String scannedCode)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        Map<String, Object> params1 = new HashMap<>();
        params1.put("command","am broadcast -a com.walmart.scanner.SCANNED --es \"com.motorolasolutions.emdk.datawedge.data_string\" \"" + scannedCode + "\" --es \"com.motorolasolutions.emdk.datawedge.label_type\" \"Code128\"");
        String result1 = (String) driver.executeScript("mobile:handset:shell", params1);
    }

    @Then("^I scan the received barcodes:")
    public void IScanReceivedBarcodesFromList(List<String> scannedCodes)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        for (String scannedCode : scannedCodes) {
            Map<String, Object> params1 = new HashMap<>();
            params1.put("command", "am broadcast -a com.walmart.scanner.SCANNED --es \"com.motorolasolutions.emdk.datawedge.data_string\" \"" + scannedCode + "\" --es \"com.motorolasolutions.emdk.datawedge.label_type\" \"Code128\"");
            String result1 = (String) driver.executeScript("mobile:handset:shell", params1);
        }
    }

    @Then("^I get barcodes from received labels")
    public void IGetBarcodesFromLabels()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        for (int i=1; i<=4; i++)
        {
            getBundle().setProperty("label"+i, driver.findElementByXPath("//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[" + i + "]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[1]").getText());
        }
    }

    @Then("^I scan barcodes from received labels")
    public void IScanReceivedBarcodes()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        for (int i=1; i<=4; i++)
        {
            String scannedCode = getBundle().getString("label"+i);
            Map<String, Object> params1 = new HashMap<>();
            params1.put("command", "am broadcast -a com.walmart.scanner.SCANNED --es \"com.motorolasolutions.emdk.datawedge.data_string\" \"" + scannedCode + "\" --es \"com.motorolasolutions.emdk.datawedge.label_type\" \"Code128\"");
            String result1 = (String) driver.executeScript("mobile:handset:shell", params1);
        }
    }

    @Then("^I verify reverse logical type")
    public void IVerifyReverseLogicalType()
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        driver.findElementByXPath("//*[@content-desc=\"Reverse Logistics Containers\"]").getText().equals("No");
    }
}
