package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

@QAFTestStepProvider
public class CSGStepDefs
{
    @When("^I type \"([^\"]*)\" in element \"(.*?)\"$")
    public void ITypeInElement(String query, String input)
    {
        QAFExtendedWebElement element = new QAFExtendedWebElement(input);
        element.sendKeys(query);
    }

    @When("^I hover and then click on Sign Out$")
    public void IHoverAndThenClickOnSignOut()
    {
        Actions a = new Actions(DeviceUtils.getQAFDriver());
        QAFExtendedWebElement element = DeviceUtils.getQAFDriver().findElementByCssSelector(".tt-dropdown-title.tt-dynamic-bold");
        a.moveToElement(element).build().perform();

        QAFExtendedWebElement element1 = DeviceUtils.getQAFDriver().findElementByCssSelector(".action.ng-scope");
        element1.click();
    }

    @Then("^the video is playing$")
    public void TheVideoIsPlaying() throws InterruptedException, IOException
    {
        Thread.sleep(1000);
        File start = DeviceUtils.getQAFDriver().getScreenshotAs(OutputType.FILE);
        File end = DeviceUtils.getQAFDriver().getScreenshotAs(OutputType.FILE);

        byte[] f1 = Files.readAllBytes(start.toPath());
        byte[] f2 = Files.readAllBytes(end.toPath());

        if (!Arrays.equals(f1, f2))
        {
            ReportUtils.logVerify("Video playback successful as both images are different", true);
        }
        else
        {
            ReportUtils.logVerify("Video playback unsuccessful as both images are the same", false);
        }
    }

}
