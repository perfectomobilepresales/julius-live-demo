package com.quantum.steps;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.DriverUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

@QAFTestStepProvider
public class BarclaysStepDefs {

    @Then("^I want to see the closest branch$")
    public void IWantToSeeTheBarclaysLogo() throws Exception {
        DeviceUtils.switchToContext("NATIVE_APP");
        List<WebElement> elements1 = DeviceUtils.getQAFDriver().findElementsByXPath("//*[@label=\"Barclays Branch, 55 Broadmead Bristol BS1 3EA\"]");
        int elemSize = elements1.size();

        if (elemSize > 0) {
            ReportUtils.logVerify("The closest branch was verified successfully", true);
        } else {
            ReportUtils.logVerify("The closest branch search failed ", false);
        }
    }

    @Then("^I enter Barclays passcode$")
    public void IEnterBarclaysPassCode() throws Exception {
        DeviceUtils.switchToContext("NATIVE_APP");
        List<String> buttons = new ArrayList<String>();
        buttons.add("xpath=//*[@label=\"1\"]");
        buttons.add("xpath=//*[@label=\"5\"]");
        buttons.add("xpath=//*[@label=\"9\"]");
        buttons.add("xpath=//*[@label=\"7\"]");
        buttons.add("xpath=//*[@label=\"3\"]");
        for(int i=0; i<5; i++)
        {
            CommonStep.click(buttons.get(i));
        }
    }

}
