package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriver;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.DriverUtils;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;


@QAFTestStepProvider

public class BTStepsDefs {

    @Then("^I accept BT cookies$")
    public void IAcceptBTCookies()
    {
        try {
            QAFExtendedWebDriver driver= DeviceUtils.getQAFDriver();
            driver.findElementByCssSelector("button.cookie-ui__btn___2YvnJ:nth-child(2)").click();
        } catch (Exception e) {
            ;
        }
    }

    @When("^I login to BT Broadband$")
    public void ILoginToBTBroadband()
    {
        try {
            QAFExtendedWebDriver driver= DeviceUtils.getQAFDriver();
            driver.findElementByCssSelector(".main-content > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1) > div:nth-child(1) > div:nth-child(1) > p:nth-child(1) > a:nth-child(3) > span:nth-child(1)").click();
            Thread.currentThread().sleep(1000);
            driver.findElementByCssSelector("input.logintextbox:nth-child(10)").clear();
            driver.findElementByCssSelector("input.logintextbox:nth-child(10)").sendKeys("jccmong@gmail.com");
            driver.findElementByCssSelector("input.logintextbox:nth-child(17)").clear();
            driver.findElementByCssSelector("input.logintextbox:nth-child(17)").sendKeys("FEkfUU6uyFec2g");
            driver.findElementByCssSelector(".loginbtngoBT").click();
            Thread.currentThread().sleep(1000);
        } catch (Exception e) {
            ;
        }
    }

    @Then("^I find \"([^\"]*)\" on BT Broadband")
    public void IFindOnBTBroadband(String searchKey) throws Throwable
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            QAFExtendedWebElement searchResultElement = driver.findElementByXPath("//*[contains(text(),searchKey)]");
            searchResultElement.verifyPresent();
        } catch (Exception e) {
            ;
        }
    }

    @Then("^I run BT headless test$")
    public void IRunBTHeadless()
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            driver.findElementByCssSelector("button.cookie-ui__btn___2YvnJ:nth-child(2)").click();
            driver.findElementByCssSelector(".main-content > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1) > div:nth-child(1) > div:nth-child(1) > p:nth-child(1) > a:nth-child(3) > span:nth-child(1)").click();
            driver.findElementByCssSelector("input.logintextbox:nth-child(10)").clear();
            driver.findElementByCssSelector("input.logintextbox:nth-child(10)").sendKeys("jccmong@gmail.com");
            driver.findElementByCssSelector("input.logintextbox:nth-child(17)").clear();
            driver.findElementByCssSelector("input.logintextbox:nth-child(17)").sendKeys("FEkfUU6uyFec2g");
            driver.findElementByCssSelector(".loginbtngoBT").click();
            QAFExtendedWebElement searchResultElement = driver.findElementByXPath("//*[contains(text(),searchKey)]");
            searchResultElement.verifyPresent();
        } catch (Exception e) {
            ;
        }
    }

}
