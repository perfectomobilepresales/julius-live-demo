package com.quantum.steps;

import com.google.common.collect.ImmutableMap;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.DriverCommand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

@QAFTestStepProvider

public class AristocratStepDefs {

    @Then("^I set up the Unity test config \"(.*?)\" and port \"(.*?)\"$")
    public void ISetUpUnityTest(String server, String port)
    {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        try {

            int heightAdjust = 10;
            String editBox = "//hierarchy/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.EditText[1]\n"; // "//*[@class=\"android.widget.EditText\"]"

            String resScreen = DeviceUtils.getDeviceProperty("resolution");
            int resolution = Integer.parseInt(resScreen.substring(0,resScreen.indexOf("*")));
            if (resolution>1440) heightAdjust = 7;

            // Go to LoneStar Debug
            Map<String, Object> params = new HashMap<>();
            params.put("label", "LoneStar Debug");
            params.put("screen.top", "40%");
            params.put("screen.left", "80%");
            params.put("screen.width", "20%");
            params.put("screen.height", "30%");
            driver.executeScript("mobile:button-text:click", params);

            Thread.sleep(1000);

            // Set server
            Map<String, Object> params2 = new HashMap<>();
            params2.put("label", "Attach JS debugger");
            params2.put("label.direction", "above");
            params2.put("label.offset", Integer.toString(heightAdjust) + "%");
            params2.put("screen.top", "15%");
            params2.put("screen.left", "40%");
            params2.put("screen.width", "20%");
            params2.put("screen.height", "10%");
            driver.executeScript("mobile:button-text:click", params2);

            Thread.sleep(1000);

            if (DeviceUtils.getDeviceProperty("os").contains("Android")) {
                driver.findElementByXPath(editBox).clear();
                driver.findElementByXPath(editBox).sendKeys(server);
                driver.findElementByXPath("//*[@text=\"OK\"]").click();
            } else {
                driver.getKeyboard().sendKeys(" ");
                Map<String, Object> paramsDel = new HashMap<>();
                paramsDel.put("label", "Done");
                paramsDel.put("label.direction", "right");
                paramsDel.put("label.offset", "5%");
                paramsDel.put("screen.top", "50%");
                paramsDel.put("screen.left", "50%");
                paramsDel.put("screen.width", "50%");
                paramsDel.put("screen.height", "50%");
                driver.executeScript("mobile:button-text:click", paramsDel);
                driver.getKeyboard().sendKeys(server);
                Map<String, Object> paramsDone = new HashMap<>();
                paramsDone.put("label", "Done");
                paramsDone.put("screen.top", "50%");
                paramsDone.put("screen.left", "50%");
                paramsDone.put("screen.width", "50%");
                paramsDone.put("screen.height", "50%");
                driver.executeScript("mobile:button-text:click", paramsDone);
            }

            Thread.sleep(1000);

            // Enable test harness
            Map<String, Object> params3 = new HashMap<>();
            params3.put("label", "Enable Test Harness");
            params3.put("screen.top", "30%");
            params3.put("screen.left", "15%");
            params3.put("screen.width", "30%");
            params3.put("screen.height", "15%");
            driver.executeScript("mobile:button-text:click", params3);

            Thread.sleep(1000);

            // Set proxy
            Map<String, Object> params4 = new HashMap<>();
            params4.put("label", "Add Server");
            params4.put("label.direction", "below");
            params4.put("label.offset", Integer.toString(heightAdjust) + "%");
            params4.put("screen.top", "50%");
            params4.put("screen.left", "30%");
            params4.put("screen.width", "30%");
            params4.put("screen.height", "20%");
            driver.executeScript("mobile:button-text:click", params4);

            Thread.sleep(1000);

            if (DeviceUtils.getDeviceProperty("os").contains("Android")) {
                driver.findElementByXPath(editBox).clear();
                driver.findElementByXPath(editBox).sendKeys(server+":"+port);
                driver.findElementByXPath("//*[@text=\"OK\"]").click();
            } else {
                driver.getKeyboard().sendKeys(" ");
                Map<String, Object> paramsDel = new HashMap<>();
                paramsDel.put("label", "Done");
                paramsDel.put("label.direction", "right");
                paramsDel.put("label.offset", "5%");
                paramsDel.put("screen.top", "50%");
                paramsDel.put("screen.left", "50%");
                paramsDel.put("screen.width", "50%");
                paramsDel.put("screen.height", "50%");
                driver.executeScript("mobile:button-text:click", paramsDel);
                driver.getKeyboard().sendKeys(server+":"+port);
                Map<String, Object> paramsDone = new HashMap<>();
                paramsDone.put("label", "Done");
                paramsDone.put("screen.top", "50%");
                paramsDone.put("screen.left", "50%");
                paramsDone.put("screen.width", "50%");
                paramsDone.put("screen.height", "50%");
                driver.executeScript("mobile:button-text:click", paramsDone);
            }

            Thread.sleep(1000);

//        // Use Proxy
//        Map<String, Object> params5 = new HashMap<>();
//        params5.put("content", "Use Proxy");
//        params5.put("screen.top", "15%");
//        params5.put("screen.left", "20%");
//        params5.put("screen.width", "15%");
//        params5.put("screen.height", "15%");
//        driver.executeScript("mobile:button-text:click", params5);

            // Click Add Server
            Map<String, Object> params6 = new HashMap<>();
            params6.put("label", "Add Server");
            params6.put("screen.top", "50%");
            params6.put("screen.left", "40%");
            params6.put("screen.width", "15%");
            params6.put("screen.height", "20%");
            driver.executeScript("mobile:button-text:click", params6);

            Thread.sleep(1000);

            // Bring up Server list
            Map<String, Object> params7 = new HashMap<>();
            params7.put("label", "Current Atlas");
            params7.put("label.direction", "above");
            params7.put("label.offset", Integer.toString(heightAdjust) + "%");
            params7.put("screen.top", "10%");
            params7.put("screen.left", "0%");
            params7.put("screen.width", "20%");
            params7.put("screen.height", "20%");
            driver.executeScript("mobile:button-text:click", params7);

            Thread.sleep(1000);

            Map<String, Object> paramsScroll = new HashMap<>();
            paramsScroll.put("start", "20%,50%");
            paramsScroll.put("end", "20%,1%");
            Object res = driver.executeScript("mobile:touch:swipe", paramsScroll);

            Thread.sleep(1000);

            // Click the newly added server
            Map<String, Object> params8 = new HashMap<>();
            params8.put("label", "Recordings");
            params8.put("label.direction", "below");
            params8.put("label.offset", Integer.toString((int)Math.floor((double)heightAdjust/2)) + "%");
            params8.put("screen.top", "50%");
            params8.put("screen.left", "0%");
            params8.put("screen.width", "20%");
            params8.put("screen.height", "30%");
            driver.executeScript("mobile:button-text:click", params8);

            Thread.sleep(1000);

            // Go to Config Settings
            Map<String, Object> params9 = new HashMap<>();
            params9.put("label", "Config Settings");
            params9.put("screen.top", "0%");
            params9.put("screen.left", "80%");
            params9.put("screen.width", "20%");
            params9.put("screen.height", "25%");
            driver.executeScript("mobile:button-text:click", params9);

            Thread.sleep(1000);

            // Click Play
            Map<String, Object> params10 = new HashMap<>();
            params10.put("label", "Play");
            params10.put("screen.top", "80%");
            params10.put("screen.left", "80%");
            params10.put("screen.width", "20%");
            params10.put("screen.height", "20%");
            driver.executeScript("mobile:button-text:click", params10);
        }
        catch (Exception ex) {}
    }

}
