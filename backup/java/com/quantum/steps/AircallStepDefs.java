package com.quantum.steps;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.CloudUtils;
import cucumber.api.java.en.Then;

// import com.perfecto.PerfectoLabUtils;

import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.awt.Graphics2D;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

@QAFTestStepProvider
public class AircallStepDefs {

    @Then("^I call the other phone$")
    public void ICallTheOtherPhone() throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        driver.findElementByXPath("//*[@resource-id=\"com.aircall:id/keyboardFieldText\"]").sendKeys(getBundle().getString("phoneNumber"));
    }

    @Then("^I validate contact \"([^\"]*)\" is found")
    public void IValidateContactFound(String personName) throws Exception {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        DeviceUtils.switchToContext("NATIVE_APP");
        driver.findElementByXPath("//*[@resource-id=\"com.aircall:id/personName\"]").getText().contains(personName);
    }
}
