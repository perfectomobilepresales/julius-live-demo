package com.quantum.steps;

import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.quantum.utils.DeviceUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.JavascriptExecutor;

import java.util.concurrent.TimeUnit;

@QAFTestStepProvider

public class BAStepDefs {

    @When("^I accept BA cookies")
    public void IAcceptBACookies()
    {
        try {
            QAFExtendedWebDriver driver= DeviceUtils.getQAFDriver();
            driver.findElementByCssSelector(".exit-icon").click();
        } catch (Exception e) {
            ;
        }
    }

    @Then("^I login to BA")
    public void ILoginToBA()
    {
        try {
            QAFExtendedWebDriver driver= DeviceUtils.getQAFDriver();
            driver.findElementById("loginid").sendKeys("jccmong@gmail.com");
            driver.findElementById("password").sendKeys("Y3C-Q*n_W5D#v@3");
            driver.findElementByCssSelector(".ivsg2-height-override").click();
        } catch (Exception e) {
            ;
        }
    }

    @Then("^I find \"([^\"]*)\" on BA")
    public void IFindOnBA(String searchKey) throws Throwable
    {
        try {
            QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
            QAFExtendedWebElement searchResultElement = driver.findElementByXPath("//label[text()=searchKey or contains(text(), searchKey)]");
            searchResultElement.verifyPresent();
        } catch (Exception e) {
            ;
        }
    }
}
