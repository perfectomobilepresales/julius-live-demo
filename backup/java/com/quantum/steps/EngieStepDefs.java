package com.quantum.steps;

import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.DriverUtils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.aspectj.weaver.patterns.HasMemberTypePattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static com.qmetry.qaf.automation.core.ConfigurationManager.getBundle;

// import com.perfecto.PerfectoLabUtils;


@QAFTestStepProvider
public class EngieStepDefs {

    @Then("^I start Engie application$")
    public static void startEngieApp() {
        String appName="engieMarketPrices";
        try {
            if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
                appName="EngieMarketPrices";
            }
            DeviceUtils.startApp(appName, "name");
            if (DeviceUtils.getDeviceProperty("os").contains("iOS"))
                DeviceUtils.switchToContext("NATIVE_APP");
            else
                DeviceUtils.switchToContext("WEBVIEW");
        } catch (Exception ex) {}
    }

    @Given("^I try to close Engie application$")
    public void ITryToCloseEngieApplication()
    {
        String appName="engieMarketPrices";
        try {
            if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
                appName="EngieMarketPrices";
            }
            DeviceUtils.closeApp(appName, "name", true);
        } catch (Exception ex) {}
    }

    @Then("^I view period \"(.*?)\"$")
    public void IViewPeriod(String period) {
        QAFExtendedWebDriver driver = DeviceUtils.getQAFDriver();
        ArrayList<String> arr = new ArrayList<String>(Arrays.asList("CAL-21","CAL-22","CAL-23","Q4-20","Q1-21","Q2-21","Q3-21"));
        if (DeviceUtils.getDeviceProperty("os").contains("iOS")) {
            driver.findElementByXPath("//*[@label=\"" + period + "\" and @value=\"0\"]").click();
        } else {
            driver.findElementByXPath("/html[1]/body[1]/ion-app[1]/ion-popover[1]/div[1]/div[2]/div[1]/ng-component[1]/ion-list[1]/ion-item[" + Integer.toString (arr.indexOf(period)+1) + "]/div[1]/ion-radio[1]/button[1]/span[1]").click();
        }
    }
}
