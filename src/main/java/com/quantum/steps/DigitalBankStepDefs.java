package com.quantum.steps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.perfecto.CommonUtils;
import com.perfecto.PerfectoLabUtils;
import com.perfecto.reportium.client.ReportiumClient;
import com.perfecto.reportium.test.TestContext;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStepProvider;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.quantum.utils.AppiumUtils;
import com.quantum.utils.DeviceUtils;
import com.quantum.utils.DriverUtils;
import com.quantum.utils.ReportUtils;
import cucumber.api.java.en.Given;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@QAFTestStepProvider
public class DigitalBankStepDefs {

    @Given("^I log in to Digital Bank")
    public void loginToDigitalBank() throws InterruptedException {

        QAFExtendedWebDriver driver = DriverUtils.getDriver();

        if (ConfigurationManager.getBundle().getProperty("appType").equals("Web"))
        {

            ReportUtils.logStepStart("Log In");
            driver.get("http://dbankdemo.com/bank/login?error");
            driver.findElement(By.id("username")).click();
            driver.findElement(By.id("username")).clear();
            driver.findElement(By.id("username")).sendKeys("jsmith@demo.io");
            driver.findElement(By.cssSelector("form")).click();
            driver.findElement(By.id("password")).click();
            driver.findElement(By.id("password")).clear();
            driver.findElement(By.id("password")).sendKeys("Demo123!");
            driver.findElement(By.id("submit")).click();
//            driver.findElement(By.cssSelector("div.page-header.float-left")).click();
            Thread.sleep(3000);

            ReportUtils.logStepStart("ATM Search");
            driver.findElement(By.cssSelector("i.fa.fa-search")).click();
            driver.findElement(By.id("zipcode")).click();
            driver.findElement(By.id("zipcode")).clear();
            driver.findElement(By.id("zipcode")).sendKeys("94414");
            driver.findElement(By.id("zipcode")).sendKeys(Keys.ENTER);
            driver.findElement(By.cssSelector("div.card-body")).click();
            Thread.sleep(2000);

            ReportUtils.logStepStart("Log Out");
            driver.findElement(By.cssSelector("img.user-avatar.rounded-circle")).click();
            driver.findElement(By.linkText("Logout")).click();
            driver.findElement(By.cssSelector("div.sufee-alert.alert.with-close.alert-success.alert-dismissible.fade.show")).click();

        } else {
//        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//        WebDriverWait wait = new WebDriverWait(driver, 30);
//        StopWatch stopwatch = new StopWatch();
//        Map<String, Object> params = new HashMap<>();
//        List<String> timers = new ArrayList<>();
//
//        ReportiumClient reportiumClient = ReportUtils.getReportClient();
//        reportiumClient.testStart("DigitalBank1", new TestContext("tag2", "tag3"));
//
//        reportiumClient.stepStart("Login");
//        WebElement server = driver.findElement(By.name("MainServer"));
//        server.clear();
//        server.sendKeys("75.185.184.71:80");
//
//        WebElement signIn = driver.findElement(By.name("MainSignIn"));
//        stopwatch.start();
//        signIn.click();
//        reportiumClient.stepStart("Get Checking");
//        WebElement accountChecking = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.name("AccountChecking"))));
//        stopwatch.stop();
//        String time = Long.toString(stopwatch.getTime());
//        timers.add(time);
//        stopwatch.reset();
//
//        params.put("name", "LoginTimer");
//        params.put("result", time);
//        driver.executeScript("mobile:status:timer", params);
//        params.clear();
//
//        accountChecking.click();
//
//        reportiumClient.stepStart("Return From Checking");
//        driver.findElement(By.xpath("(//*[@name='DetailReturn'])[last()]")).click();
//
//        reportiumClient.stepStart("Goto ATM Screen");
//        WebElement atmButton = driver.findElement(By.xpath("(//*[@name='AccountATM'])[last()]"));
//        stopwatch.start();
//        atmButton.click();
//
//        reportiumClient.stepStart("Fill In ZIPcode");
//        WebElement zipEntry = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("(//*[@name='ATMZip'])[last()]"))));
//        stopwatch.stop();
//        time = Long.toString(stopwatch.getTime());
//        timers.add(time);
//        stopwatch.reset();
//
//        params.put("name", "ATMTimer");
//        params.put("result", time);
//        driver.executeScript("mobile:status:timer", params);
//        params.clear();
//
//        zipEntry.sendKeys("11749");
//
//        reportiumClient.stepStart("Submit ZIPcode");
////		driver.findElement(By.xpath("(//*[@name='ATMSubmit'])[last()]")).click();
//
//        reportiumClient.stepStart("Exit ATM Screen");
//        driver.findElement(By.xpath("(//*[@name='ATMReturn'])[last()]")).click();
//
//        reportiumClient.stepStart("Exit to Main");
//        driver.findElement(By.xpath("(//*[@name='AccountExit'])[last()]")).click();
//
//        reportiumClient.stepStart("Exit App");
//        driver.findElement(By.xpath("(//*[@name='MainExit'])[last()]")).click();

            StopWatch stopwatch = new StopWatch();
            List<String> timers = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            ReportUtils.logStepStart("Login");
            CommonUtils.clear("dbank.input.server");
            CommonUtils.sendKeys("75.185.184.71:80", "dbank.input.server");
            CommonUtils.waitForEnabled("dbank.bttn.signIn");

            stopwatch.start();
            CommonUtils.click("dbank.bttn.signIn");
            ReportUtils.logStepStart("Get Checking");
            CommonUtils.waitForVisible("dbank.bttn.checking");
            CommonUtils.waitForEnabled("dbank.bttn.checking");
            stopwatch.stop();
            String time = Long.toString(stopwatch.getTime());
            timers.add(time);
            stopwatch.reset();

            params.put("name", "LoginTimer");
            params.put("result", time);
            driver.executeScript("mobile:status:timer", params);
            params.clear();
//        CommonUtils.click("dbank.bttn.checking");
//
//        ReportUtils.logStepStart("Return From Checking");
//        CommonUtils.click("dbank.bttn.checkingReturn");

            ReportUtils.logStepStart("Goto ATM Screen");
            stopwatch.start();
            CommonUtils.click("dbank.bttn.atm");
            CommonUtils.waitForVisible("dbank.input.atmZip");
            CommonUtils.waitForEnabled("dbank.input.atmZip");
            stopwatch.stop();
            time = Long.toString(stopwatch.getTime());
            timers.add(time);
            stopwatch.reset();

            params.put("name", "ATMTimer");
            params.put("result", time);
            driver.executeScript("mobile:status:timer", params);
            params.clear();
            CommonUtils.sendKeys("11749", "dbank.input.atmZip");

            ReportUtils.logStepStart("Submit ZIPcode");
            CommonUtils.click("dbank.bttn.atmSubmit");

            ReportUtils.logStepStart("Exit ATM Screen");
            CommonUtils.click("dbank.bttn.atmReturn");

            ReportUtils.logStepStart("Exit to Main");
            CommonUtils.click("dbank.bttn.accountExit");

            ReportUtils.logStepStart("Exit App");
            CommonUtils.click("dbank.bttn.exit");

            File csvOutputFile = new File("./" + DriverUtils.getDriver().getCapabilities().getCapability("deviceName") + "_test.csv");
            try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
                pw.println(String.join(",", timers));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Given("^I validate ATM information")
    public void validateATMInfo() throws InterruptedException {
        ReportUtils.logAssert("Successful ATM valiation", true);
    }
}